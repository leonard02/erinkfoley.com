<?php include("includes/config.inc.php");
	
	define("T","tbl_aboutus",true);
	define("TS","tbl_shows",true);
	define("TSD","tbl_show_dates",true);

	$page_id = 5;
	
	//About details are fetched
	$sql = "SELECT * FROM `".T."`";
	$res = $db->get($sql);
	$row = $db->fetch_array($res);
	
	//Shows are fetched
	$sql_shows = "SELECT * FROM `".TS."` WHERE `show_type`='1' AND `display`='Yes' ORDER BY `sequence`";
	$res_shows = $db->get($sql_shows);
	$num_shows = $db->num_rows($res_shows);

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<?php include("js.css.inc.php");?>
</head>
<body>
<div class="social_media"><?php include("socialmedia.inc.php");?></div>
<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td align="center" valign="top">
<table width="1126" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td height="33" align="center" valign="top">
<!--<div class="social_media"><img src="images/social_media.jpg" alt="" width="25" height="161" usemap="#Map" border="0" />
  <map name="Map" id="Map">
    <area shape="circle" coords="14,73,9" href="#" />
    <area shape="circle" coords="14,99,11" href="#" />
    <area shape="circle" coords="13,123,10" href="#" />
    <area shape="circle" coords="15,151,10" href="#" />
  </map>
</div>-->   
    </td>
      </tr>
  <tr>
    <td align="center" valign="top"><?php include("header.inc.php");?></td>
  </tr>
  <tr>
    <td align="center" valign="top"><table width="100%" border="0" align="center" cellpadding="0" cellspacing="0" class="about_area">
      <tr>
        <td width="28%" align="left" valign="top"><table width="319" border="0" align="left" cellpadding="0" cellspacing="0">
         <tr>
            <td height="35" colspan="3" align="left" valign="middle" class="style4">COMING SOON</td>
         </tr>
          <tr>
            <td width="98" align="left" valign="top">&nbsp;</td>
            <td width="93" align="left" valign="top">&nbsp;</td>
            <td width="128" align="left" valign="top">&nbsp;</td>
          </tr>
		  <?php if($num_shows>0){
		  while($row_shows=$db->fetch_array($res_shows)){
		  		
				$show_image="uploads/shows/".$row_shows['image_path'];
				
				if($row_shows['link']!=""){
					if($row_shows['location']!="Name")
						$location = "<a href='".$row_shows['link']."' target='_blank' style='color:#333333;text-decoration:none;'>".$f->getValue($row_shows['location'])."</a>";
					else
						$location = "";
					if($row_shows['city_state']!="City, State")
						$city_state = "<a href='".$row_shows['link']."' target='_blank' style='color:#333333;text-decoration:none;'>".$f->getValue($row_shows['city_state'])."</a>";
					else
						$city_state = "";
				}else{
					if($row_shows['location']!="Name")
						$location = $f->getValue($row_shows['location']);
					else
						$location = "";
					if($row_shows['city_state']!="City, State")
						$city_state = $f->getValue($row_shows['city_state']);
					else
						$city_state = "";
				}
				
				$headline = $f->getValue($row_shows['headline']);
		  ?>
          <tr>
            <td colspan="2" align="left" valign="top"><table width="191" border="0" cellspacing="0" cellpadding="0">
				 <?php if($row_shows['summarized_dates']=="Y"){?>
                  <tr>
                    <td width="98" align="left" valign="top"><?php echo date("D, n.j",strtotime($row_shows['from_date']));?>
                      -<br /><?php echo date("D, n.j",strtotime($row_shows['to_date']));?></td>
                    <td width="93" align="left" valign="top"><?php echo strtoupper($row_shows['summarize_time']);?></td>
                  </tr>
				  <?php }else if($row_shows['list_dates']=="Y"){
				  	//Show dates are fetched
					$sql_show_dates = "SELECT * FROM `".TSD."` WHERE `show_id`='".$row_shows['show_id']."' ORDER BY `show_date`";
					$res_show_dates = $db->get($sql_show_dates);
					while($row_show_dates = $db->fetch_array($res_show_dates)){
				  ?>
                  <tr>
                    <td width="98" align="left" valign="top"><?php echo date("D, n.j",strtotime($row_show_dates['show_date']));?></td>
                    <td width="93" align="left" valign="top"><?php echo strtoupper($row_show_dates['from_time']);?> <?php if($row_show_dates['to_time']!="") echo ",<br />".strtoupper($row_show_dates['to_time']);?></td>
                  </tr>
                  <tr>
                    <td align="left" valign="top">&nbsp;</td>
                    <td align="left" valign="top">&nbsp;</td>
                  </tr>
				  <?php }
				  }
				  ?>
                </table></td>
            <td width="128" align="left" valign="top">
              <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                  <td><?php echo $headline;?></td>
                </tr>
				<tr>
                  <td><?php echo $location;?></td>
                </tr>
                <tr>
                  <td><?php echo $city_state;?></td>
                </tr>
              </table></td>
          </tr>
		  <tr>
            <td align="left" valign="top">&nbsp;</td>
            <td align="left" valign="top">&nbsp;</td>
            <td align="left" valign="top">&nbsp;</td>
          </tr>
		  <?php 
		  	}
		  }?>
        </table></td>
        <td width="9.5%" align="center" valign="middle"><img src="images/about-div.jpg" width="13" height="320" alt="" /></td>
        <td width="62.5%" align="left" valign="top"><table width="630" border="0" align="left" cellpadding="0" cellspacing="0">
          <tr>
            <td height="35" align="left" valign="top">&nbsp;</td>
          </tr>
          <tr>
            <td align="left" valign="top">&nbsp;</td>
          </tr>
          <tr>
            <td align="left" valign="top"><?php echo $f->getValue($row['content']);?></td>
          </tr>
          <tr>
            <td align="left" valign="top">&nbsp;</td>
          </tr>
        </table></td>
      </tr>
    </table></td>
  </tr>

  <tr>
    <td align="center" valign="top">&nbsp;</td>
  </tr>
  <tr>
    <td align="center" valign="top">&nbsp;</td>
  </tr>
  <tr>
    <td align="center" valign="top">&nbsp;</td>
  </tr>
  <tr>
    <td align="center" valign="top"><img src="images/shows/devide-line.jpg" width="1046" height="2" alt="" /></td>
  </tr>
  <tr>
    <td align="center" valign="top">&nbsp;</td>
  </tr>
  <tr>
    <td align="center" valign="top">&nbsp;</td>
  </tr>
  <tr>
    <td align="center" valign="top"><?php include("footer.inc.php");?></td>
  </tr>
  <tr>
    <td align="center" valign="top">&nbsp;</td>
  </tr>
  <tr>
    <td align="center" valign="top">&nbsp;</td>
  </tr>
  <tr>
    <td align="center" valign="top">&nbsp;</td>
  </tr>
    </table></td>
  </tr>
</table>
    </td>
  </tr>
</table>
</body>
</html>
