<?php
	require_once("../includes/config.inc.php");
	$f->redirectBase = WEBSITE_URL;
	$f->isLogin('_admin','index.php');
	
	$page_id = 6;
	
	define("T","tbl_aboutus",true);
	$msg = $_GET['msg'];
	
	if(isset($_POST['btnSave']) && empty($_POST['btnSave'])==false){
		$update_array=array("content" => $f->post('content'));
		
		$db->update(T,$update_array,"id",'1');
		$msg = "Record successfully updated!";
	}
	//About details are fetched
	$sql = "SELECT * FROM `".T."`";
	$res = $db->get($sql);
	$row = $db->fetch_array($res);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<?php include("js.css.inc.php");?>
<script src="ckeditor/ckeditor.js" type="text/javascript"></script>
</head>

<body>
<!--main-->
<div id="main">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td align="center" valign="top">
	<table width="1131" border="0" align="center" cellpadding="0" cellspacing="0">
      <tr>
        <td align="center" valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0">
		  <tr>
		  	<td colspan="2">
				<?php include("header.inc.php");?>				
			</td>
		  </tr>          
		  <tr>
            <td height="22" colspan="2" align="left" valign="top" bgcolor="#444444"><table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <th width="30" scope="row">&nbsp;</th>
                <td width="1101" class="style3">ABOUT</td>
              </tr>
            </table></td>
          </tr>
          <tr>
            <td height="22" colspan="2" align="left" valign="top" bgcolor="#bcbcbc">&nbsp;</td>
          </tr>
        </table></td>
      </tr>
    </table>
	</td>
  </tr>
  <tr>
    <td align="left" valign="top" class="contaner">&nbsp;</td>
  </tr>
    <?php if($msg!=""){
  ?>
  <tr>
    <td align="center" valign="top" class="contaner" height="30"><?php echo urldecode($msg);?></td>
  </tr>
  <?php
  }?>

  </table>
<div class="contaner">
<form name="frm" id="frm" action="<?php echo CP;?>" method="post">
<table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#d9d6d6">
<tr>
    <td width="16%" align="center" valign="top">&nbsp;</td>
    <td width="84%" align="left" valign="top">&nbsp;</td>
  </tr>
  <tr>
    <td width="16%" align="left" valign="top" style="padding-left:30px;">ABOUT TEXT</td>
    <td width="84%" align="left" valign="top">
	<textarea name="content" cols="80" rows="10" id="content"><?php echo $f->getValue($row['content']);?></textarea>
	<script type="text/javascript">
		CKEDITOR_BASEPATH = '<?php echo WEBSITE_URL;?>/ckeditor/';
		CKEDITOR.replace('content', {toolbar : 'Full',width : '767px',height : '388px',
		filebrowserBrowseUrl : '<?php echo WEBSITE_URL;?>/ckeditor/filemanager/browser/default/browser.html?Connector=<?php echo WEBSITE_URL;?>/ckeditor/filemanager/connectors/php/connector.php',
		filebrowserImageBrowseUrl : '<?php echo WEBSITE_URL;?>/ckeditor/filemanager/browser/default/browser.html?Type=Image&Connector=<?php echo WEBSITE_URL;?>/ckeditor/filemanager/connectors/php/connector.php',
		filebrowserFlashBrowseUrl : '<?php echo WEBSITE_URL;?>/ckeditor/filemanager/browser/default/browser.html?Type=Flash&Connector=<?php echo WEBSITE_URL;?>/ckeditor/filemanager/connectors/php/connector.php',
		filebrowserUploadUrl : '<?php echo WEBSITE_URL;?>/ckeditor/filemanager/connectors/php/upload.php?Type=File',
		filebrowserImageUploadUrl : '<?php echo WEBSITE_URL;?>/ckeditor/filemanager/connectors/php/upload.php?Type=Image',
		filebrowserFlashUploadUrl : '<?php echo WEBSITE_URL;?>/ckeditor/filemanager/connectors/php/upload.php?Type=Flash'});
	</script>	
	</td>
  </tr>
  <tr>
    <td align="center" valign="top">&nbsp;</td>
    <td align="left" valign="top">&nbsp;</td>
  </tr>
</table>
<table width="100%"  border="0" cellpadding="0" cellspacing="0">
  <tr>
    <td  width="14%" scope="col">&nbsp;</td>
    <td  width="4%" scope="col">&nbsp;</td>
    <td  width="82%" scope="col">&nbsp;</td>
  </tr>
  <tr>
    <td scope="col">&nbsp;</td>
    <td scope="col">&nbsp;</td>
    <td scope="col"><input name="btnSave" id="btnSave" type="submit" value=" SAVE " class="input2" /></td>
  </tr>
  <tr>
    <td scope="col">&nbsp;</td>
    <td scope="col">&nbsp;</td>
    <td scope="col">&nbsp;</td>
  </tr>
</table>
</form>
</div>
</div>