<?php
	require_once("../includes/config.inc.php");
	$f->redirectBase = WEBSITE_URL;
	$f->isLogin('_admin','index.php');
	
	$msg = '&nbsp;';
	if(isset($_POST['btnChangePassword'])) {
		$old_pwd = mysql_real_escape_string($f->makePassword($_POST['txtOldPassword']));
		$new_pwd = mysql_real_escape_string($f->makePassword($_POST['txtNewPassword']));
		$sql = "SELECT * FROM `tbl_admin` WHERE `username`='".$_SESSION['_admin']."' AND `password`='".$old_pwd."'";
		$res = $db->get($sql,__FILE__,__LINE__);
		if($db->num_rows($res) > 0) {
			$sql = "UPDATE `tbl_admin` SET `password`='".$new_pwd."' WHERE `username`='".$_SESSION['_admin']."'";
			$db->get($sql,__FILE__,__LINE__);
			$msg = $f->getHtmlMessage('Password has been successfully changed');
		} else {
			$msg = $f->getHtmlError('Old Password does not matching');
		}
	}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<?php include("js.css.inc.php");?>
<script type="text/javascript">
$(document).ready(function() {
	$('#frmChangePassword').validate();
	$("#txtOldPassword").rules("add", {
		minlength: 6,
		messages: {
			minlength: "Minimum input 6 characters"
		}
	});
	$("#txtNewPassword").rules("add", {
		minlength: 6,
		messages: {
			minlength: "Minimum input 6 characters"
		}
	});
	$("#txtConfPassword").rules("add", {
		minlength: 6,
		equalTo: "#txtNewPassword",
		messages: {
			minlength: "Minimum input 6 characters",
			equalTo: "Should be same as above"
		}
	});	
});
</script>
</head>
<body>
<!--main-->
<div id="main">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
    <td align="center" valign="top"><table width="1131" border="0" align="center" cellpadding="0" cellspacing="0">
      <tr>
        <td align="center" valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0">
		 <tr>
		  	<td colspan="2">
				<?php include("header.inc.php");?>				
			</td>
		  </tr>            
		  <tr>
			<td height="22" colspan="2" align="left" valign="top" bgcolor="#444444">&nbsp;</td>
		  </tr>
		  <tr>
			<td height="22" colspan="2" align="left" valign="top" bgcolor="#bcbcbc">&nbsp;</td>
			</tr>
        </table></td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td align="left" valign="top" class="contaner">&nbsp;</td>
  </tr>
  </table>
 
<div class="contaner">
  
  <table width="100%" border="0" cellspacing="0" cellpadding="0" >
   <tr>
    <td height="84" align="center"><?php echo $msg;?></td>
  </tr>
  <tr>
    <td align="center"><form action="<?php echo CP;?>" method="post" name="frmChangePassword" id="frmChangePassword">
     <input name="sq" type="hidden" id="sq" value="<?php echo $counter;?>" />
        <input name="type" type="hidden" id="type" value="Sequence" />
		<table width="500" border="0" align="center" cellpadding="0" cellspacing="1">
			<tr>
				<td width="157" class="label-right">Old Password:</td>
				<td width="340"><input name="txtOldPassword" type="password" id="txtOldPassword" class="required input1" /></td>
			</tr>
            <tr>
            	<td height="3" colspan="2"></td>
            </tr>
			<tr>
				<td class="label-right">New Password:</td>
				<td><input name="txtNewPassword" type="password" id="txtNewPassword" class="required input1" /></td>
			</tr>
             <tr>
            	<td height="3" colspan="2"></td>
            </tr>
			<tr>
				<td class="label-right">Confirm New Password:</td>
				<td><input name="txtConfPassword" type="password" id="txtConfPassword" class="required input1" /></td>
			</tr>
			<tr>
				<td height="10"></td>
			</tr>
			
			<tr>
				<td>&nbsp;</td>
				<td><input name="btnChangePassword" type="submit" class="button" id="btnChangePassword" value=" Change Password " /></td>
			</tr>
			<tr>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
			</tr>
		</table>
    	</form></td>
  </tr>
  </table>
  <div class="clear"></div>
</div>   
<div class="clear"></div>
</div>
<!--main-->
<?php require_once("tb.php");?>
</body>
</html>
