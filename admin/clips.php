<?php
	require_once("../includes/config.inc.php");
	$f->redirectBase = WEBSITE_URL;
	$f->isLogin('_admin','index.php');
	
	$page_id = 2;
	
	define("TC","tbl_clips",true);

	$index = $_GET['index'];
	$msg = $_GET['msg'];
	
	if(isset($_GET['action']) && $_GET['action']=="Delete"){
		$Id = $_GET['Id'];
		
		//image path is fetched
		$sql_show = "SELECT * FROM ".TC." WHERE `clip_id`='".$Id."'";
		$res_show = $db->get($sql_show);
		$row_show = $db->fetch_array($res_show);
		@unlink("../uploads/videos/image/".$row_show['image_path']);
		
		if($row_show['file_path']!="")
			@unlink("../uploads/videos/".$row_show['file_path']);
		
		if($row_show['custom_file']!="")
			@unlink("../uploads/videos/".$row_show['custom_file']);
			
		$sql = "DELETE FROM `".TC."` WHERE `clip_id`='".$Id."'";
		$db->get($sql);
		
		$f->Redirect(CP."?msg=".urlencode("Record successfully deleted!"));
	}

	
	if($index == 'Sequence' && $_GET['action']=='SQ'){
		$sql = "SELECT * FROM `".TC."` ORDER BY `sequence` ASC";
		$res = $db->get($sql);
		while($row = $db->fetch_array($res)) {
			$sq = $_POST['sq'.$row['clip_id']];
			if($sq != $row['sequence']) {
				$sql = "SELECT * FROM ".TC." WHERE `clip_id`=".$row['clip_id'];
				$res_faq = $db->get($sql,__FILE__,__LINE__);
				$faq = $db->fetch_array($res_faq);
				$sql = "UPDATE `".TC."` SET `sequence`=".$faq['sequence']." WHERE `sequence`=".$sq." ";
				$db->get($sql,__FILE__,__LINE__);
				$sql = "UPDATE `".TC."` SET `sequence`='".$sq."' WHERE `clip_id`=".$row['clip_id']." ";
				$db->get($sql,__FILE__,__LINE__);
			}
		}
		$f->Redirect(CP."?index=List&msg=".urlencode("Sequence has been successfully updated"));

	}

	
	
	
	//Clip details are fetched
	$sql_clips = "SELECT * FROM `".TC."` ORDER BY `sequence`";
	$res_clips = $db->get($sql_clips);
	$records = $db->num_rows($res_clips);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<?php include("js.css.inc.php");?>
<script type="text/javascript">
$(document).ready(function(){
		$('.delete').click(function()

		{

			var href = $(this).attr('href');

			var title = $(this).attr('rel');

			var text = '<div id="a" align="center"><strong>Are you sure you want to delete the Clip?</div><br><div id="b" align="center"><strong>'+title+'</strong><div>';

			jConfirm(text, 'Confirmation', function(r){

				if(r == true){

					window.location.href = href;

				}

			});

			return false;

		});
		

});
function __doPostSQ() {
    var str = "";
    flag = false;
    var sq = document.getElementById('sq').value;
    sq = sq.split(",");
    //alert(sq.length);
    for(var j=0;j<sq.length;j++) {
        str = document.getElementById('sq' + sq[j]).value;
        if(str == 0 || str == "") {
            flag = true;
        }
    }
    if(flag == true) {
        alert("Sequence can not be blank and should be greater than zero");
    } else {
        document.frmSchedule.submit();
    }
}
</script>
</head>
<body>
<!--main-->
<div id="main">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td align="center" valign="top"><table width="1131" border="0" align="center" cellpadding="0" cellspacing="0">
      <tr>
        <td align="center" valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0">
		  <tr>
		  	<td colspan="2">
				<?php include("header.inc.php");?>				
			</td>
		  </tr>          
		  <tr>
            <td height="22" colspan="2" align="left" valign="top" bgcolor="#444444"><table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <th width="30" scope="row">&nbsp;</th>
                <td width="1101" class="style3">CLIPS</td>
              </tr>
            </table></td>
          </tr>
          <tr>
            <td height="22" colspan="2" align="left" valign="top" bgcolor="#bcbcbc"><table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <th width="30" scope="row">&nbsp;</th>
                <td class="style4"><a href="clipsdetail.php?index=Add">ADD NEW CLIP</a></td>
                </tr>
            </table></td>
          </tr>
        </table></td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td align="left" valign="top" class="contaner">&nbsp;</td>
  </tr>
  <?php if($msg!=""){
  ?>
  <tr>
    <td align="center" valign="top" class="contaner" height="30"><?php echo urldecode($msg);?></td>
  </tr>
  <?php
  }?>
  </table>

<div class="contaner">
<form name="frmSchedule" id="frmSchedule" action="<?php echo CP.'?index=Sequence&action=SQ';?>" method="post">
<?php if($records>0){?>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <?php 
  $cnt = 0;
  while($row_clips = $db->fetch_array($res_clips)){
  		if($cnt == 0){
			$bgcolor = "#d9d6d6";
			$cnt++;
		}
		else if($cnt == 1){
			$bgcolor = "#e6e6e6";
			$cnt--;
		}
  ?>
  <tr>
    <td width="155" height="25" align="center" valign="middle" bgcolor="<?php echo $bgcolor;?>"><?php echo $f->getValue($row_clips['name']);?></td>
    <td width="1" align="left" valign="top" scope="col"></td>
    <td width="104" align="center" valign="middle" bgcolor="<?php echo $bgcolor;?>"><a href="clipsdetail.php?index=Edit&Id=<?php echo $row_clips['clip_id'];?>"><img src="images/pencil.png" width="14" height="14" alt="" /></a></td>
    <td width="104" align="center" valign="middle" bgcolor="<?php echo $bgcolor;?>"><a href="<?php echo CP;?>?action=Delete&Id=<?php echo $row_clips['clip_id'];?>" rel="<?php echo $f->getValue($row_clips['name']);?>" class="delete"><img src="images/delite.png" width="14" height="13" alt="" /></a></td>
    <td width="103" align="center" valign="middle" bgcolor="<?php echo $bgcolor;?>"><input type="text" size="1" class="input7" value="<?php echo $row_clips['sequence'];?>" id="sq<?php echo $row_clips['clip_id'];?>" name="sq<?php echo $row_clips['clip_id'];?>" style="text-align: center;" /></td>
    <td width="768" align="center" valign="middle" bgcolor="<?php echo $bgcolor;?>">&nbsp;</td>
  </tr>
  <?php }?>
  </table>
  <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
   <tr >
    <td width="23%" height="25" align="left" valign="middle">&nbsp;</td>
    <td width="77%" align="left" valign="middle"><input name="btnSave" type="submit" value="SAVE SORT" class="input2" /></td>
    </tr>
</table>

 <?php }else{?>
 <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td align="center" valign="top">
		<table width="100%" border="0" cellspacing="0" cellpadding="0">
		  <tr>
			<th height="25" align="center" bgcolor="#d9d6d6" scope="col">No Results Found</th>
			</tr>
		</table>    
    </td>
    </tr>
 </table>
 <table width="100%" border="0" cellpadding="0" cellspacing="0">
  <tr>
    <td width="400" scope="col">&nbsp;</td>
    <td width="731" scope="col">&nbsp;</td>
  </tr>
</table>
 <?php }?>
</form>
<div class="clear"></div>
</div>  
 
<div class="clear"></div>
</div>
<!--main-->
</body>
</html>
