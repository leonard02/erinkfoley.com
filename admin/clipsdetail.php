<?php
	require_once("../includes/config.inc.php");
	include('../includes/youtube.class.php');
	
	$f->redirectBase = WEBSITE_URL;
	$f->isLogin('_admin','index.php');
	
	$page_id = 2;
	
	define("TC","tbl_clips",true);

	$index = $_GET['index'];
	$msg = $_GET['msg'];
	
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<?php include("js.css.inc.php");?>
<script type="text/javascript">
$(document).ready(function(){
	$("#frmClips").validate({

			errorPlacement: function(error, element){

				element.parent().parent().children().children(".errorContainer").append(error);

			}

	});
	$(".videoclass").click(function(){
		if($(this).val()=="file"){
			$('#upload_videoTrId').show();
			$('#utubeTrId').hide();
			$('#otherTrId').hide();
			$('#customfileTrId').hide();
			$("#upload_video").rules("add", {
				<?php if($index=="Add"){?>
				required: true,
				<?php }?>
				accept: 'flv|mp4|m4v|f4v|mov',
				messages: {
					accept: "Accept only flv, mp4, m4v, f4v , mov file"
				}
			});
			$("#utube").rules("remove");
			$("#embed_code").rules("remove");
			$("#custom_file").rules("remove");
		}else if($(this).val()=="utube"){
			$('#upload_videoTrId').hide();
			$('#otherTrId').hide();
			$('#customfileTrId').hide();
			$("#upload_video").rules("remove");
			$("#embed_code").rules("remove");
			$("#custom_file").rules("remove");
			$('#utubeTrId').show();
			$("#utube").rules("add", {
				required: true
			});
		}else if($(this).val()=="embed_code"){
			$('#upload_videoTrId').hide();
			$("#upload_video").rules("remove");
			$("#utube").rules("remove");
			$('#utubeTrId').hide();
			$('#customfileTrId').hide();
			$('#otherTrId').show();
			$("#embed_code").rules("add", {
				required: true
			});
		}else if($(this).val()=="custom_file"){
			$('#upload_videoTrId').hide();
			$("#upload_video").rules("remove");
			$("#utube").rules("remove");
			$("#embed_code").rules("remove");
			$('#utubeTrId').hide();
			$('#customfileTrId').show();
			$('#otherTrId').hide();
			$("#custom_file").rules("add", {
				required: true
			});
		}
	});
	$("#image_path").rules("add",{
		<?php if($index=="Add"){
		?>
		required: true,
		<?php
		}?>
		accept: 'jpg|jpeg|png|gif|JPG|JPEG|PNG|GIF',
		messages: {
			accept: "Accept only jpg, png, gif files"
		}
	});
});
	
	
</script>
<link rel="stylesheet" type="text/css" href="../flowplayer/skin/minimalist.css">
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>
<script type="text/javascript" src="../flowplayer/flowplayer.min.js"></script>
<style type="text/css">
.flowplayer { width: 600px; height: 360px; }
</style>
</head>
<body>
<!--main-->
<div id="main">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td align="center" valign="top"><table width="1131" border="0" align="center" cellpadding="0" cellspacing="0">
      <tr>
        <td align="center" valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0">
		<tr>
		  	<td colspan="2">
				<?php include("header.inc.php");?>				
			</td>
		 </tr>          
		 <tr>
            <td height="22" colspan="2" align="left" valign="top" bgcolor="#444444"><table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <th width="30" scope="row">&nbsp;</th>
                <td width="1101" class="style3">CLIPS</td>
              </tr>
            </table></td>
          </tr>
          <tr>
            <td height="22" colspan="2" align="left" valign="top" bgcolor="#bcbcbc"><table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <th width="30" scope="row">&nbsp;</th>
                <td width="1101" class="style4"><a href="clips.php">BACK TO LIST</a></td>
              </tr>
            </table></td>
          </tr>
        </table></td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td align="left" valign="top" class="contaner">&nbsp;</td>
  </tr>
  </table>

<div class="contaner">
<?php if($index == "Add"){
	if(isset($_POST['btnSave']) && empty($_POST['btnSave'])==false){
		
		$video_type = $f->post('radvideo');
		
		if($video_type == "embed_code")
			$embed_code = $f->post('embed_code');
		else
			$embed_code = "";
		
		if($video_type == "custom_file")
			$custom_file = $f->post('custom_file');
		else
			$custom_file = "";
			
		if($video_type == "file"){
			
			if($_FILES['upload_video']['name']!=""){
				require_once('../includes/file.upload.inc.php');

				$objFileUpload = new FileUpload();
										
				$objFileUpload->UploadContent = $_FILES['upload_video'];

				$objFileUpload->UploadFolder = "../uploads/videos";

				$image_return = $objFileUpload->Upload();
				
				$file_path = $image_return['server_name'];
			}
		}
		
		if($_FILES['image_path']['name']!=""){
				require_once('../includes/file.upload.inc.php');

				$objFileUpload = new FileUpload();
										
				$objFileUpload->UploadContent = $_FILES['image_path'];

				$objFileUpload->UploadFolder = "../uploads/videos/image";

				$image_return = $objFileUpload->Upload();
				
				$image_path = $image_return['server_name'];
		}
		
		$sequence_id = $m->getMaxSequence(TC);
		
		$insert_sql_array = array("name" => $f->post('name'),
								  "video_type" => $video_type,
								  "utube" => $f->post('utube'),
								  "file_path" => $file_path,
								  "embed_code" => $embed_code,
								  "custom_file" => $custom_file,
								  "image_path" => $image_path,
								  "sequence" => $sequence_id,
								  "create_date" => 'CURDATE()',
								  "create_time" => 'CURTIME()');
								  
		$db->insert(TC,$insert_sql_array);
		$f->Redirect("clips.php?msg=".urlencode("Record successfully added!"));
		
		
	}
?>
<form name="frmClips" id="frmClips" action="<?php echo CP."?".QS;?>" method="post" enctype="multipart/form-data">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr >
    <td width="147" height="25" align="center" valign="middle" class="line1" >NAME</td>
    <td width="2" align="left" valign="middle" scope="col"></td>
    <td width="30" align="left" valign="middle" class="line1" >&nbsp;</td>
    <td width="952" align="left" valign="middle" class="line1" ><input name="name" id="name" type="text" value="" class="input6 required" />&nbsp;<span class="errorContainer"></span></td>
  </tr>
  <tr >
    <td height="10" width="147" align="center" valign="middle" class="line2" ></td>
    <td width="2" align="left" valign="middle" ></td>
    <td width="30" align="left" valign="middle" class="line2"></td>
    <td width="952" align="left" valign="middle" class="line2"></td>
  </tr>
  <tr >
    <td width="147" align="center" valign="top" class="line2" scope="col">VIDEO</td>
    <td width="2" align="left" valign="middle" scope="col"></td>
    <td width="30" align="left" valign="middle" class="line5" scope="col" >&nbsp;</td>
    <td width="952" align="left" valign="middle" class="line2"><input type="radio" name="radvideo" id="radvideo1" value="utube" class="videoclass required"  />Youtube&nbsp;<input type="radio" name="radvideo" id="radvideo1" value="file" class="videoclass required" />Video File&nbsp;<input type="radio" name="radvideo" id="radvideo3" value="embed_code" class="videoclass required"  />Other Embed Code&nbsp;<input type="radio" name="radvideo" id="radvideo4" value="custom_file" class="videoclass required"  />Custom File&nbsp;<span class="errorContainer"></span></td>
  </tr>
  <tr >
    <td width="147" align="center" valign="top" class="line2" scope="col">&nbsp;</td>
    <td width="2" align="left" valign="middle" scope="col"></td>
    <td width="30" align="left" valign="middle" class="line5" scope="col" >&nbsp;</td>
    <td width="952" align="left" valign="middle" class="line2">&nbsp;</td>
  </tr>
  <tr id="upload_videoTrId" style="display:none">
    <td align="center" valign="top" class="line2" scope="col">&nbsp;</td>
    <td align="left" valign="middle" scope="col"></td>
    <td align="left" valign="middle" class="line5" scope="col">&nbsp;</td>
    <td align="left" valign="middle" class="line2"><table width="544" border="0" cellpadding="0" cellspacing="0">
      <tr>
        <td width="91" scope="col">Upload Video</td>
        <td width="453" scope="col"><input name="upload_video" id="upload_video" type="file" />&nbsp;<span class="errorContainer"></span></td>
      </tr>
    </table></td>
  </tr>
  <tr id="utubeTrId" style="display:none">
    <td align="center" valign="top" class="line2" scope="col">&nbsp;</td>
    <td align="left" valign="middle" scope="col"></td>
    <td align="left" valign="middle" class="line5" scope="col">&nbsp;</td>
    <td align="left" valign="middle" class="line2"><table width="544" border="0" cellpadding="0" cellspacing="0">
      <tr>
        <td width="91" scope="col">Youtube</td>
        <td width="453" scope="col"><textarea name="utube" id="utube" rows="10" cols="50"></textarea>
        &nbsp;<span class="errorContainer"></span></td>
      </tr>
    </table></td>
  </tr>
  <tr id="otherTrId" style="display:none">
    <td align="center" valign="top" class="line2" scope="col">&nbsp;</td>
    <td align="left" valign="middle" scope="col"></td>
    <td align="left" valign="middle" class="line5" scope="col">&nbsp;</td>
    <td align="left" valign="middle" class="line2"><table width="544" border="0" cellpadding="0" cellspacing="0">
      <tr>
        <td width="91" scope="col">Embed Code</td>
        <td width="453" scope="col"><textarea name="embed_code" id="embed_code" rows="10" cols="50"></textarea>
        &nbsp;<span class="errorContainer"></span></td>
      </tr>
    </table></td>
  </tr>
  <tr id="customfileTrId" style="display:none">
    <td align="center" valign="top" class="line2" scope="col">&nbsp;</td>
    <td align="left" valign="middle" scope="col"></td>
    <td align="left" valign="middle" class="line5" scope="col">&nbsp;</td>
    <td align="left" valign="middle" class="line2"><table width="544" border="0" cellpadding="0" cellspacing="0">
      <tr>
        <td width="91" scope="col">File Name</td>
        <td width="453" scope="col"><input name="custom_file" id="custom_file" type="text" value="" class="input6"/>&nbsp;<span class="errorContainer"></span></td>
      </tr>
    </table></td>
  </tr>
  <tr >
    <td align="center" valign="top" class="line2" scope="col">&nbsp;</td>
    <td align="left" valign="middle" scope="col"></td>
    <td align="left" valign="middle" class="line5" scope="col" >&nbsp;</td>
    <td align="left" valign="middle" class="line2">&nbsp;</td>
  </tr>
  <tr >
    <td align="center" valign="top" class="line1" >&nbsp;</td>
    <td align="left" valign="middle" ></td>
    <td align="left" valign="middle" class="line1" >&nbsp;</td>
    <td align="left" valign="middle" class="line1">&nbsp;</td>
  </tr>
  <tr >
    <td align="center" valign="top" class="line1" >IMAGE</td>
    <td align="left" valign="middle" ></td>
    <td align="left" valign="middle" class="line1" >&nbsp;</td>
    <td align="left" valign="middle" class="line1"><table width="544" border="0" cellpadding="0" cellspacing="0">
      <tr>
        <td width="91" scope="col">Upload Image</td>
        <td width="453" scope="col"><input name="image_path" id="image_path" type="file" /></td>
      </tr>
      <tr>
        <td colspan="2" scope="col">Image should be sized to 202 X 105 Exactly, 72 dpi</td>
        </tr>
    </table></td>
  </tr>
  <tr >
    <td align="center" valign="top" class="line1" >&nbsp;</td>
    <td align="left" valign="middle" ></td>
    <td align="left" valign="middle" class="line1" >&nbsp;</td>
    <td align="left" valign="middle" class="line1">&nbsp;</td>
  </tr>
</table>
<table width="100%"  border="0" cellpadding="0" cellspacing="0">
  <tr>
    <td width="16%" scope="col">&nbsp;</td>
    <td  width="84%" scope="col">&nbsp;</td>
  </tr>
  <tr>
    <td scope="col">&nbsp;</td>
    <td scope="col"><input name="btnSave" type="submit" value="  SAVE  " class="input2" /></td>
  </tr>
  <tr>
    <td scope="col">&nbsp;</td>
    <td scope="col">&nbsp;</td>
  </tr>
</table>
</form>
<?php }else if($index == "Edit"){

	$Id = $_GET['Id'];
	
	$sql_clips = "SELECT * FROM `".TC."` WHERE `clip_id`='".$Id."'";
	$res_clips = $db->get($sql_clips);
	$row_clips = $db->fetch_array($res_clips);
	
	$image = "../uploads/videos/image/".$row_clips['image_path'];
	
	if($row_clips['file_path']!="" || $row_clips['custom_file']!=""){
		if($row_clips['file_path']!=""){
			$file = "../uploads/videos/".$row_clips['file_path'];
			$ext = explode('.',$row_clips['file_path']);
		}
		else{
			$file = "../uploads/videos/".$row_clips['custom_file'];
			$ext = explode('.',$row_clips['custom_file']);
			//print_r($ext);
		}
		
																					
		switch(end($ext)) {
			case "mp4":
				$video_type = "video/mp4";
				break;
			case "mov":
				$video_type = "video/mp4";
				break;
			case "wmv":
				$video_type = "video/x-ms-wmv";
				break;
			case "avi":
				$video_type = "video/x-msvideo";
				break;
			case "flv":
				$video_type = "video/x-flv";
				break;
			case "m4v":
				$video_type = "video/m4v";
				break;			
		}
		//echo $video_type;
		//echo $file;
		//$data_key = '$289122895653393';
		if($_SERVER['SERVER_NAME'] == "localhost") { $data_key = '$289122895653393';}else{ $data_key = '$468743015507892';}
	}

	if(isset($_POST['btnSave']) && empty($_POST['btnSave'])==false){
		
		$video_type = $f->post('radvideo');
		if($video_type == "file"){
			
			if($_FILES['upload_video']['name']!=""){
				require_once('../includes/file.upload.inc.php');

				$objFileUpload = new FileUpload();
				
				$objFileUpload->UploadMode = 'Edit';

				$objFileUpload->OldFileName = $row_clips['file_path'];
										
				$objFileUpload->UploadContent = $_FILES['upload_video'];

				$objFileUpload->UploadFolder = "../uploads/videos";

				$image_return = $objFileUpload->Upload();
				
				$file_path = $image_return['server_name'];
			}else
				$file_path = $row_clips['file_path'];
		}else{
			if($file!="")
				unlink($file);
		}
		
		if($_FILES['image_path']['name']!=""){
				require_once('../includes/file.upload.inc.php');

				$objFileUpload = new FileUpload();
				
				$objFileUpload->UploadMode = 'Edit';

				$objFileUpload->OldFileName = $row_clips['image_path'];
										
				$objFileUpload->UploadContent = $_FILES['image_path'];

				$objFileUpload->UploadFolder = "../uploads/videos/image";

				$image_return = $objFileUpload->Upload();
				
				$image_path = $image_return['server_name'];
		}else
			$image_path = $row_clips['image_path'];
		
		if($video_type == "embed_code")
			$embed_code = $f->post('embed_code');
		else
			$embed_code = "";

		if($video_type == "custom_file")
			$custom_file = $f->post('custom_file');
		else
			$custom_file = "";

		
		$update_sql_array = array("name" => $f->post('name'),
								  "video_type" => $video_type,
								  "utube" => $f->post('utube'),
								  "embed_code" => $embed_code,
								  "custom_file" => $custom_file,
								  "file_path" => $file_path,
								  "image_path" => $image_path);
								  
		$db->update(TC,$update_sql_array,"clip_id",$Id);
		$f->Redirect("clipsdetail.php?index=Edit&Id=".$Id."&msg=".urlencode("Record successfully updated!"));
		
		
	}
?>
<form name="frmClips" id="frmClips" action="<?php echo CP."?".QS;?>" method="post" enctype="multipart/form-data">
<?php if($msg!=""){
?>
<table width="100%"  border="0" cellpadding="0" cellspacing="0">
  <tr>
    <td colspan="3" scope="col" height="30" align="center"><?php echo urldecode($msg);?></td>
    </tr>

</table>
<?php
}?>

<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr >
    <td width="147" height="25" align="center" valign="middle" class="line1" >NAME</td>
    <td width="2" align="left" valign="middle" scope="col"></td>
    <td width="30" align="left" valign="middle" class="line1" >&nbsp;</td>
    <td width="952" align="left" valign="middle" class="line1" ><input name="name" id="name" type="text" value="<?php echo $f->getValue($row_clips['name']);?>" class="input6 required" />&nbsp;<span class="errorContainer"></span></td>
  </tr>
  <tr >
    <td height="10" width="147" align="center" valign="middle" class="line2" ></td>
    <td width="2" align="left" valign="middle" ></td>
    <td width="30" align="left" valign="middle" class="line2"></td>
    <td width="952" align="left" valign="middle" class="line2"></td>
  </tr>
  <tr >
    <td width="147" align="center" valign="top" class="line2" scope="col">VIDEO</td>
    <td width="2" align="left" valign="middle" scope="col"></td>
    <td width="30" align="left" valign="middle" class="line5" scope="col" >&nbsp;</td>
    <td width="952" align="left" valign="middle" class="line2"><input type="radio" name="radvideo" id="radvideo1" value="utube" class="videoclass required" <?php if($row_clips['video_type']=="utube") echo "checked='checked'";?> />Youtube&nbsp;<input type="radio" name="radvideo" id="radvideo1" value="file" class="videoclass required" <?php if($row_clips['video_type']=="file") echo "checked='checked'";?>  />Video File&nbsp;<input type="radio" name="radvideo" id="radvideo3" value="embed_code" class="videoclass required" <?php if($row_clips['video_type']=="embed_code") echo "checked='checked'";?> />Other Embed Code&nbsp;<input type="radio" name="radvideo" id="radvideo4" value="custom_file" class="videoclass required" <?php if($row_clips['video_type']=="custom_file") echo "checked='checked'";?>   />Custom File&nbsp;<span class="errorContainer"></span></td>
  </tr>
  <tr >
    <td width="147" align="center" valign="top" class="line2" scope="col">&nbsp;</td>
    <td width="2" align="left" valign="middle" scope="col"></td>
    <td width="30" align="left" valign="middle" class="line5" scope="col" >&nbsp;</td>
    <td width="952" align="left" valign="middle" class="line2">&nbsp;</td>
  </tr>
  <tr id="upload_videoTrId" <?php if($row_clips['video_type']=="utube" || $row_clips['video_type']=="embed_code" || $row_clips['video_type']=="custom_file") echo 'style="display:none"';?>>
    <td align="center" valign="top" class="line2" scope="col">&nbsp;</td>
    <td align="left" valign="middle" scope="col"></td>
    <td align="left" valign="middle" class="line5" scope="col">&nbsp;</td>
    <td align="left" valign="middle" class="line2"><table width="544" border="0" cellpadding="0" cellspacing="0">
      <tr>
        <td width="91" scope="col">Upload Video</td>
        <td width="453" scope="col"><input name="upload_video" id="upload_video" type="file" />&nbsp;<span class="errorContainer"></span></td>
      </tr>
      <tr>
        <td colspan="2" scope="col">
			<div class="flowplayer" data-swf="../flowplayer/flowplayer.swf" data-key="<?php echo $data_key;?>" data-ratio="0.6000">
				<video>
					<source type="<?php echo $video_type?>" src="<?php echo $file;?>">
				</video>
			 </div></td>
        </tr>
    </table></td>
  </tr>
  <tr id="utubeTrId" <?php if($row_clips['video_type']=="file" || $row_clips['video_type']=="embed_code" || $row_clips['video_type']=="custom_file") echo 'style="display:none"';?>>
    <td align="center" valign="top" class="line2" scope="col">&nbsp;</td>
    <td align="left" valign="middle" scope="col"></td>
    <td align="left" valign="middle" class="line5" scope="col">&nbsp;</td>
    <td align="left" valign="middle" class="line2"><table width="544" border="0" cellpadding="0" cellspacing="0">
      <tr>
        <td width="91" scope="col">Youtube</td>
        <td width="453" scope="col"><textarea name="utube" id="utube" rows="10" cols="50"><?php echo $f->getValue($row_clips['utube']);?></textarea>
        &nbsp;<span class="errorContainer"></span></td>
      </tr>
	  <tr>
        <td colspan="2" scope="col"><?php 
			$script = $row_clips['utube'];
			$youtube = new YoutubeParser;
			$youtube->set('source',$script);
			$youtube->set('unique',true);
			$youtube->width='600';
			$youtube->height='360';
			$video = $youtube->getall();
			echo $video[0]['embed'];
		?></td>
      </tr>

    </table></td>
  </tr>
  <tr id="otherTrId" <?php if($row_clips['video_type']=="utube" || $row_clips['video_type']=="file" || $row_clips['video_type']=="custom_file") echo 'style="display:none"';?>>
    <td align="center" valign="top" class="line2" scope="col">&nbsp;</td>
    <td align="left" valign="middle" scope="col"></td>
    <td align="left" valign="middle" class="line5" scope="col">&nbsp;</td>
    <td align="left" valign="middle" class="line2"><table width="544" border="0" cellpadding="0" cellspacing="0">
      <tr>
        <td width="91" scope="col">Embed Code</td>
        <td width="453" scope="col"><textarea name="embed_code" id="embed_code" rows="10" cols="50"><?php echo htmlentities($f->getValue($row_clips['embed_code']));?></textarea>
        &nbsp;<span class="errorContainer"></span></td>
      </tr>
	  	  <tr>
        <td colspan="2" scope="col"><?php 
			echo $f->getValue($row_clips['embed_code']);
		?></td>
      </tr>

    </table></td>
  </tr>
  <tr id="customfileTrId" <?php if($row_clips['video_type']=="utube" || $row_clips['video_type']=="file" || $row_clips['video_type']=="embed_code") echo 'style="display:none"';?>>
    <td align="center" valign="top" class="line2" scope="col">&nbsp;</td>
    <td align="left" valign="middle" scope="col"></td>
    <td align="left" valign="middle" class="line5" scope="col">&nbsp;</td>
    <td align="left" valign="middle" class="line2"><table width="544" border="0" cellpadding="0" cellspacing="0">
      <tr>
        <td width="91" scope="col">File Name</td>
        <td width="453" scope="col"><input name="custom_file" id="custom_file" type="text" value="<?php echo $row_clips['custom_file'];?>" class="input6"/>&nbsp;<span class="errorContainer"></span></td>
      </tr>
	  <tr>
        <td colspan="2" scope="col">
			<div class="flowplayer" data-swf="../flowplayer/flowplayer.swf" data-key="<?php echo $data_key;?>" data-ratio="0.6000">
				<video>
					<source type="<?php echo $video_type?>" src="<?php echo $file;?>">
				</video>
			 </div>
			 </td>
        </tr>
    </table></td>
  </tr>
  <tr >
    <td align="center" valign="top" class="line2" scope="col">&nbsp;</td>
    <td align="left" valign="middle" scope="col"></td>
    <td align="left" valign="middle" class="line5" scope="col" >&nbsp;</td>
    <td align="left" valign="middle" class="line2">&nbsp;</td>
  </tr>
  <tr >
    <td align="center" valign="top" class="line1" >&nbsp;</td>
    <td align="left" valign="middle" ></td>
    <td align="left" valign="middle" class="line1" >&nbsp;</td>
    <td align="left" valign="middle" class="line1">&nbsp;</td>
  </tr>
  <tr >
    <td align="center" valign="top" class="line1" >IMAGE</td>
    <td align="left" valign="middle" ></td>
    <td align="left" valign="middle" class="line1" >&nbsp;</td>
    <td align="left" valign="middle" class="line1"><table width="544" border="0" cellpadding="0" cellspacing="0">
      <tr>
        <td colspan="2" scope="col"><img src="<?php echo $image;?>" width="202" height="105" alt="" /></td>
        </tr>
      <tr>
        <td width="91" scope="col">Upload Image</td>
        <td width="453" scope="col"><input name="image_path" id="image_path" type="file" /></td>
      </tr>
      <tr>
        <td colspan="2" scope="col">Image should be sized to 202 X 105 Exactly, 72 dpi</td>
        </tr>
    </table></td>
  </tr>
  <tr >
    <td align="center" valign="top" class="line1" >&nbsp;</td>
    <td align="left" valign="middle" ></td>
    <td align="left" valign="middle" class="line1" >&nbsp;</td>
    <td align="left" valign="middle" class="line1">&nbsp;</td>
  </tr>
</table>
<table width="100%"  border="0" cellpadding="0" cellspacing="0">
  <tr>
    <td width="16%" scope="col">&nbsp;</td>
    <td  width="84%" scope="col">&nbsp;</td>
  </tr>
  <tr>
    <td scope="col">&nbsp;</td>
    <td scope="col"><input name="btnSave" type="submit" value="  SAVE  " class="input2" /></td>
  </tr>
  <tr>
    <td scope="col">&nbsp;</td>
    <td scope="col">&nbsp;</td>
  </tr>
</table>
</form>
<?php } ?>
<div class="clear"></div>
</div>  
<div class="clear"></div>
</div>
<!--main-->
</body>
</html>
