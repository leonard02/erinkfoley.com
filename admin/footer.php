<?php
	require_once("../includes/config.inc.php");
	$f->redirectBase = WEBSITE_URL;
	$f->isLogin('_admin','index.php');
	
	$page_id = 5;
	
	define("TF","tbl_footer",true);
	$msg = $_GET['msg'];
	$index = $_GET['index'];
	
	//All the footers are fetched
	$sql_footer = "SELECT * FROM `".TF."` ORDER BY `footer_id` DESC";
	$res_footer = $db->get($sql_footer);
	$records = $db->num_rows($res_footer);
	
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<?php include("js.css.inc.php");?>
</head>
<body>
<!--main-->
<div id="main">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td align="center" valign="top"><table width="1131" border="0" align="center" cellpadding="0" cellspacing="0">
      <tr>
        <td align="center" valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0">
		  <tr>
		  	<td colspan="2">
				<?php include("header.inc.php");?>				
			</td>
		  </tr>          
		  <tr>
            <td height="22" colspan="2" align="left" valign="top" bgcolor="#444444"><table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <th width="30" scope="row">&nbsp;</th>
                <td width="1101" class="style3">FOOTER</td>
              </tr>
            </table></td>
          </tr>
          <tr>
            <td height="22" colspan="2" align="left" valign="top" bgcolor="#bcbcbc"><!--<table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <th width="30" scope="row">&nbsp;</th>
                <td class="style4"><a href="footerdetail.php?index=Add">ADD NEW FOOTER</a></td>
                </tr>
            </table>--></td>
          </tr>
        </table></td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td align="left" valign="top" class="contaner">&nbsp;</td>
  </tr>
    <?php if($msg!=""){
  ?>
  <tr>
    <td align="center" valign="top" class="contaner" height="30"><?php echo urldecode($msg);?></td>
  </tr>
  <?php
  }?>

  </table>
  
<div class="contaner">
<?php if($db->num_rows($res_footer)>0){
	$i=1;
	$cnt = 1;
	while($row_footer = $db->fetch_array($res_footer)){
		$img_path = "../uploads/footers/".$row_footer['image_path'];
		
		if($i==1)
			$bgcolor = "#d9d6d6";
		else{
			$bgcolor = "#e6e6e6";
			$i=0;
		}
		
		if($row_footer['image_link']!=""){
				$img_link = '<a href="'.$row_footer['image_link'].'" target="_blank" style="color:#000;text-decoration:none;"><img src="'.$img_path.'" width="132" height="135" alt="" /></a>';
				$name = '<a href="'.$row_footer['image_link'].'" target="_blank" style="color:#000;text-decoration:none;">'.$f->getValue($row_footer['name']).'</a>';
				if($row_footer['subheadline']!="")
					$subheadline = '<a href="'.$row_footer['image_link'].'" target="_blank" style="color:#000;text-decoration:none;">'.$f->getValue($row_footer['subheadline']).'</a>';
				else
					$subheadline = "";
			}else{
				$img_link = '<img src="'.$img_path.'" width="132" height="135" alt="" />';
				$name = $f->getValue($row_footer['name']);
				if($row_footer['subheadline']!="")
					$subheadline = $f->getValue($row_footer['subheadline']);
				else
					$subheadline = "";
			}
				
			if($row_footer['text_link']!=""){
				$footer_text = '<a href="'.$row_footer['text_link'].'" target="_blank" style="color:#000;text-decoration:none;">'.$f->getValue($row_footer['footer_text']).'</a>';
			}else
				$footer_text = $f->getValue($row_footer['footer_text']);
			
			if($row_footer['additional_text_link']!=""){
				$additional_text = '<a href="'.$row_footer['additional_text_link'].'" target="_blank" style="color:#000;text-decoration:none;">'.$f->getValue($row_footer['additional_text']).'</a>';
			}else
				$additional_text = $f->getValue($row_footer['additional_text']);
?>
	
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <th class="padding" width="15%"  align="center" bgcolor="<?php echo $bgcolor;?>" scope="col">
      <?php echo $img_link;?></th>
    <th class="" width="0%" scope="col">&nbsp;</th>
    <td width="20%" valign="top" bgcolor="<?php echo $bgcolor;?>"  scope="col" >
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
    <th align="center" scope="col" ><p>&nbsp;</p></th>
  </tr>     
  <tr>
    <th align="center" scope="col"><h2><?php echo $name;?></h2></th>
  </tr>
  <?php if($subheadline!=""){?>
  <tr>
    <td align="center"><h3><?php echo $subheadline;?></h3></td>
  </tr>
  <?php }?>
  <tr>
    <td align="center"><table class="tunes" width="70%" border="0" align="center" cellpadding="0" cellspacing="0">
		   <tr>
			<td align="center" ><h3><?php echo $footer_text;?></h3></td>
		  </tr>
		  <tr>
			<th scope="col"><?php echo $additional_text;?></th>
		  </tr>
		</table>
	</td>
  </tr>
</table>

    </td>
    <th width="0%" scope="col">&nbsp;</th>
    <th width="65%" align="center" bgcolor="<?php echo $bgcolor;?>"  scope="col"> 
    
    <table width="78%" border="0" cellspacing="0" cellpadding="0" >
      <tr>
        <th width="60%" align="right" valign="middle" scope="col"><a href="footerdetail.php?index=Edit&Id=<?php echo $row_footer['footer_id'];?>"><img src="images/pencil.png" width="14" height="14" alt="" /></a></th>
        
      </tr>
    </table></th>
  </tr>
  <?php if($cnt == $records){
  ?>
   <tr>
    <th class="padding"  align="center">&nbsp;</th>
    <th class="" scope="col">&nbsp;</th>
    <td valign="top" >&nbsp;</td>
    <th scope="col">&nbsp;</th>
    <th align="center">&nbsp;</th>
  </tr>
  <?php
  }?>
</table>
<?php 
		$i++;
		$cnt++;
	}
}?>
<div class="clear"></div>
</div>  
  
<div class="clear"></div>
</div>
<!--main-->
</body>
</html>
