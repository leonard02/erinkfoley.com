<?php
	require_once("../includes/config.inc.php");
	$f->redirectBase = WEBSITE_URL;
	$f->isLogin('_admin','index.php');
	
	$page_id = 5;
	
	define("TF","tbl_footer",true);
	$msg = $_GET['msg'];
	$index = $_GET['index'];
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<?php include("js.css.inc.php");?>
<script type="text/javascript">
$(document).ready(function(){
	$('#frmFooter').validate();
	$("#image_path").rules("add", {
		<?php if($index=="Add"){
		?>
		required: true,
		<?php
		}?>
		accept: 'jpg|jpeg|png|gif|JPG|JPEG|PNG|GIF',
		messages: {
			accept: "Accept only jpg, png, gif files"
		}
	});
	
	$("#image_link").rules("add",{
		url:true
	});
	$("#text_link").rules("add",{
		url:true
	});
	$("#additional_text_link").rules("add",{
		url:true
	});
	
	$('#image_link').focus(function(){
		if($(this).val()=="http://www.itunes.com/exampletest")
			$(this).val("");
	});
	$('#text_link').focus(function(){
		if($(this).val()=="http://www.itunes.com/exampletest")
			$(this).val("");
	});
	$('#additional_text_link').focus(function(){
		if($(this).val()=="http://www.itunes.com/exampletest")
			$(this).val("");
	});
	
});
</script>

</head>
<body>
<!--main-->
<div id="main">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td align="center" valign="top"><table width="1131" border="0" align="center" cellpadding="0" cellspacing="0">
      <tr>
        <td align="center" valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
		  	<td colspan="2">
				<?php include("header.inc.php");?>				
			</td>
		  </tr>
          <tr>
            <td height="22" colspan="2" align="left" valign="top" bgcolor="#444444"><table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <th width="30" scope="row">&nbsp;</th>
                <td width="1101" class="style3">FOOTER</td>
              </tr>
            </table></td>
          </tr>
          <tr>
            <td height="22" colspan="2" align="left" valign="top" bgcolor="#bcbcbc"><table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <th width="30" scope="row">&nbsp;</th>
                <td class="style4"><a href="footer.php">BACK TO LIST</a></td>
                </tr>
            </table></td>
          </tr>
        </table></td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td align="left" valign="top" class="contaner">&nbsp;</td>
  </tr>
  </table>

<div class="contaner">
<?php if($index == "Add"){
	
	if(isset($_POST['btnSave']) && empty($_POST['btnSave'])==false){
	
		if($_FILES['image_path']['name']!=""){
				require_once('../includes/file.upload.inc.php');

				$objFileUpload = new FileUpload();
										
				$objFileUpload->UploadContent = $_FILES['image_path'];

				$objFileUpload->UploadFolder = "../uploads/footers";

				$image_return = $objFileUpload->Upload();
				
				$image_path = $image_return['server_name'];
			}
		
		//Record is inserted into the table
		$insert_sql_array = array("name" => $f->post('name'),
									"subheadline" => $f->post('subheadline'),
									"image_path" => $image_path,
									"image_link" => $f->post('image_link'),
									"footer_text" => $f->post('footer_text'),
									"additional_text" => $f->post('additional_text'),
									"text_link" => $f->post('text_link'),
									"additional_text_link" => $f->post('additional_text_link'));
		$db->insert(TF,$insert_sql_array);
		$f->Redirect("footer.php?msg=".urlencode("Record successfully added!"));

	}
?>
<form name="frmFooter" id="frmFooter" action="<?php echo CP."?".QS;?>" method="post" enctype="multipart/form-data">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr >
    <td width="14%" align="center" class="line1" scope="col"><p class="list">NAME</p></td>
    <td width="0%" scope="col">&nbsp;</td>
    <td class="line1" width="86%" scope="col"><p ><input name="name" type="text" value="" id="name" class="input6 required" /></p></td>
  </tr>

</table>

<table width="100%"  border="0" cellpadding="0" cellspacing="0" >
  <tr>
    <td width="14%" bgcolor="#e6e6e6" scope="col" valign="top" align="center">IMAGE</td>
    <td  width="0%" scope="col">&nbsp;</td>
    <td  width="7%" align="left" bgcolor="#e6e6e6" scope="col">Upload Image</td>
    <td width="79%" scope="col" bgcolor="#e6e6e6" class="padding" ><input name="image_path" id="image_path" type="file" /></td>
  </tr>
</table>
<table width="100%"  border="0" cellpadding="0" cellspacing="0">
  <tr>
    <td  width="14%" scope="col" bgcolor="#e6e6e6">&nbsp;</td>
    <td  width="0%" scope="col">&nbsp;</td>
    <td  width="86%" align="left" bgcolor="#e6e6e6" scope="col">Size: exactly 135 x 135, 72 dpi</td>
  </tr>
</table>
 <table width="100%"  border="0" cellpadding="0" cellspacing="0">
  <tr>
    <td  width="14%" scope="col" bgcolor="#e6e6e6">&nbsp;</td>
    <td  width="0%" scope="col">&nbsp;</td>
    <td  width="3%" align="left" bgcolor="#e6e6e6" scope="col">Link</td>
    <td width="83%" bgcolor="#e6e6e6" scope="col"><input name="image_link" id="image_link" type="text" value="http://www.itunes.com/exampletest" class="input16" ></td>
  </tr>
</table>

<table width="100%"  border="0" cellpadding="0" cellspacing="0" >
  <tr>
    <td align="center" bgcolor="#d9d6d6" scope="col">Sub-headline</td>
    <td scope="col">&nbsp;</td>
    <td align="center" bgcolor="#d9d6d6" scope="col"><input name="subheadline" id="subheadline" type="text" value="" class="input16" ></td>
    <td scope="col" bgcolor="#d9d6d6">&nbsp;</td>
    <td scope="col" bgcolor="#d9d6d6" class="padding" >&nbsp;</td>
  </tr>
  <tr>
    <td width="14%" align="center" bgcolor="#d9d6d6" scope="col">Text</td>
    <td  width="0%" scope="col">&nbsp;</td>
    <td  width="27%" align="center" bgcolor="#d9d6d6" scope="col"><input name="footer_text" id="footer_text" type="text" value="" class="input16 required" ></td>
    
    <td  width="4%" scope="col" bgcolor="#d9d6d6">Link</td>
    <td width="55%" scope="col" bgcolor="#d9d6d6" class="padding" ><input name="text_link" id="text_link" type="text" value="http://www.itunes.com/exampletest" class="input16" ></td>
  </tr>
</table>

<table width="100%"  border="0" cellpadding="0" cellspacing="0" >
  <tr>
    <td width="14%" align="center" bgcolor="#e6e6e6" scope="col">TEXT (optional)</td>
    <td  width="0%" scope="col">&nbsp;</td>
    <td  width="27%" align="center" bgcolor="#e6e6e6" scope="col"><input name="additional_text" id="additional_text" type="text" value="" class="input16" ></td>
    
    <td  width="4%" scope="col" bgcolor="#e6e6e6">Link</td>
    <td width="55%" scope="col" bgcolor="#e6e6e6" class="padding" ><input name="additional_text_link" id="additional_text_link" type="text" value="http://www.itunes.com/exampletest" class="input16" ></td>
  </tr>
</table>

<table class="savesort" width="100%"  border="0" cellpadding="0" cellspacing="0">
  <tr>
    <td  width="14%" scope="col">&nbsp;</td>
    <td  width="1%" scope="col">&nbsp;</td>
    <td  width="85%" scope="col"><input name="btnSave" id="btnSave" type="submit" value=" SAVE " class="input17" /></td>
  </tr>
</table>
</form>
<?php }else if($index == "Edit"){
	
	$Id = $_GET['Id'];
	
	//Footer details are fetched
	$sql_footer = "SELECT * FROM `".TF."` WHERE `footer_id`='".$Id."'";
	$res_footer = $db->get($sql_footer);
	$row_footer = $db->fetch_array($res_footer);
	
	$image = "../uploads/footers/".$row_footer['image_path'];

	
	if(isset($_POST['btnSave']) && empty($_POST['btnSave'])==false){
	
		if($_FILES['image_path']['name']!=""){
				require_once('../includes/file.upload.inc.php');

				$objFileUpload = new FileUpload();
				
				$objFileUpload->UploadMode = 'Edit';

				$objFileUpload->OldFileName = $row_footer['image_path'];

				$objFileUpload->UploadContent = $_FILES['image_path'];

				$objFileUpload->UploadFolder = "../uploads/footers";

				$image_return = $objFileUpload->Upload();
				
				$image_path = $image_return['server_name'];
			}else
				$image_path = $row_footer['image_path'];
		
		//Record is inserted into the table
		$update_array = array("name" => $f->post('name'),
									"subheadline" => $f->post('subheadline'),
									"image_path" => $image_path,
									"image_link" => $f->post('image_link'),
									"footer_text" => $f->post('footer_text'),
									"additional_text" => $f->post('additional_text'),
									"text_link" => $f->post('text_link'),
									"additional_text_link" => $f->post('additional_text_link'));
		$db->update(TF,$update_array,"footer_id",$Id);
		$f->Redirect(CP."?index=Edit&Id=".$Id."&msg=".urlencode("Record successfully added!"));

	}
?>
<form name="frmFooter" id="frmFooter" action="<?php echo CP."?".QS;?>" method="post" enctype="multipart/form-data">
<?php if($msg!=""){
?>
<table width="100%"  border="0" cellpadding="0" cellspacing="0">
  <tr>
    <td colspan="3" scope="col" height="30" align="center"><?php echo $msg;?></td>
    </tr>

</table>
<?php
}?>

<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr >
    <td width="14%" align="center" class="line1" scope="col"><p class="list">NAME</p></td>
    <td width="0%" scope="col">&nbsp;</td>
    <td class="line1" width="86%" scope="col"><p ><input name="name" type="text" value="<?php echo $f->getValue($row_footer['name']);?>" id="name" class="input6 required" /></p></td>
  </tr>

</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr >
    <td width="14%" align="center" valign="top" bgcolor="#e6e6e6" scope="col" >IMAGE</td>
    <td width="0%" scope="col">&nbsp;</td>
    <td class="line16" width="15%" scope="col"><img src="resize.php?imgfile=<?php echo $image;?>&max_height=135&max_width=132" alt="" /></td>
    
    <td class="line16" width="71%" valign="top" bgcolor="#e6e6e6" scope="col" >&nbsp;</td>
  </tr>
</table>

<table width="100%"  border="0" cellpadding="0" cellspacing="0" >
  <tr>
    <td width="14%" bgcolor="#e6e6e6" scope="col" valign="top" align="center"></td>
    <td  width="0%" scope="col">&nbsp;</td>
    <td  width="7%" align="left" bgcolor="#e6e6e6" scope="col">Upload Image</td>
    <td width="79%" scope="col" bgcolor="#e6e6e6" class="padding" ><input name="image_path" id="image_path" type="file" /></td>
  </tr>
</table>
<table width="100%"  border="0" cellpadding="0" cellspacing="0">
  <tr>
    <td  width="14%" scope="col" bgcolor="#e6e6e6">&nbsp;</td>
    <td  width="0%" scope="col">&nbsp;</td>
    <td  width="86%" align="left" bgcolor="#e6e6e6" scope="col">Size: exactly 135 x 135, 72 dpi</td>
  </tr>
</table>
 <table width="100%"  border="0" cellpadding="0" cellspacing="0">
  <tr>
    <td  width="14%" scope="col" bgcolor="#e6e6e6">&nbsp;</td>
    <td  width="0%" scope="col">&nbsp;</td>
    <td  width="3%" align="left" bgcolor="#e6e6e6" scope="col">Link</td>
    <td width="83%" bgcolor="#e6e6e6" scope="col"><input name="image_link" id="image_link" type="text" value="<?php echo $f->getValue($row_footer['image_link']);?>" class="input16" ></td>
  </tr>
</table>

<table width="100%"  border="0" cellpadding="0" cellspacing="0" >
 <tr>
    <td align="center" bgcolor="#d9d6d6" scope="col">Sub-headline</td>
    <td scope="col">&nbsp;</td>
    <td align="center" bgcolor="#d9d6d6" scope="col"><input name="subheadline" id="subheadline" type="text" value="<?php echo $f->getValue($row_footer['subheadline']);?>" class="input16" ></td>
    <td scope="col" bgcolor="#d9d6d6">&nbsp;</td>
    <td scope="col" bgcolor="#d9d6d6" class="padding" >&nbsp;</td>
  </tr>

  <tr>
    <td width="14%" align="center" bgcolor="#d9d6d6" scope="col">Text</td>
    <td  width="0%" scope="col">&nbsp;</td>
    <td  width="27%" align="center" bgcolor="#d9d6d6" scope="col"><input name="footer_text" id="footer_text" type="text" value="<?php echo $f->getValue($row_footer['footer_text']);?>" class="input16 required" ></td>
    
    <td  width="4%" scope="col" bgcolor="#d9d6d6">Link</td>
    <td width="55%" scope="col" bgcolor="#d9d6d6" class="padding" ><input name="text_link" id="text_link" type="text" value="<?php echo $f->getValue($row_footer['text_link']);?>" class="input16" ></td>
  </tr>
</table>

<table width="100%"  border="0" cellpadding="0" cellspacing="0" >
  <tr>
    <td width="14%" align="center" bgcolor="#e6e6e6" scope="col">TEXT (optional)</td>
    <td  width="0%" scope="col">&nbsp;</td>
    <td  width="27%" align="center" bgcolor="#e6e6e6" scope="col"><input name="additional_text" id="additional_text" type="text" value="<?php echo $f->getValue($row_footer['additional_text']);?>" class="input16" ></td>
    
    <td  width="4%" scope="col" bgcolor="#e6e6e6">Link</td>
    <td width="55%" scope="col" bgcolor="#e6e6e6" class="padding" ><input name="additional_text_link" id="additional_text_link" type="text" value="<?php echo $f->getValue($row_footer['additional_text_link']);?>" class="input16" ></td>
  </tr>
</table>

<table class="savesort" width="100%"  border="0" cellpadding="0" cellspacing="0">
  <tr>
    <td  width="14%" scope="col">&nbsp;</td>
    <td  width="1%" scope="col">&nbsp;</td>
    <td  width="85%" scope="col"><input name="btnSave" id="btnSave" type="submit" value=" SAVE " class="input17" /></td>
  </tr>
</table>
</form>
<?php }?>
<div class="clear"></div>
</div>  

<div class="clear"></div>
</div>
<!--main-->
</body>
</html>
