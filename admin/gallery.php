<?php
	require_once("../includes/config.inc.php");
	$f->redirectBase = WEBSITE_URL;
	$f->isLogin('_admin','index.php');
	
	$page_id = 4;
	
	define("TG","tbl_gallery",true);

	$index = $_GET['index'];
	$msg = $_GET['msg'];
	
	
	if(isset($_GET['action']) && $_GET['action']=="delete"){
		$Id = $_GET['Id'];
		
		//image path is fetched
		$sql_gal = "SELECT * FROM ".TG." WHERE `gallery_id`='".$Id."'";
		$res_gal = $db->get($sql_gal);
		$row_gal = $db->fetch_array($res_gal);
		unlink("../uploads/gallery/".$row_gal['main_image']);
		unlink("../uploads/gallery/thumbnail/".$row_gal['thumbnail_image']);
		
		$sql = "DELETE FROM `".TG."` WHERE `gallery_id`='".$Id."'";
		$db->get($sql);
		
		$f->Redirect(CP."?msg=".urlencode("Record successfully deleted!"));
	}

	if($index == 'Sequence' && $_GET['action']=='SQ'){
		$sql = "SELECT * FROM `".TG."` ORDER BY `sequence` ASC";
		$res = $db->get($sql);
		while($row = $db->fetch_array($res)) {
			$sq = $_POST['sq'.$row['gallery_id']];
			if($sq != $row['sequence']) {
				$sql = "SELECT * FROM ".TG." WHERE `gallery_id`=".$row['gallery_id'];
				$res_faq = $db->get($sql,__FILE__,__LINE__);
				$faq = $db->fetch_array($res_faq);
				$sql = "UPDATE `".TG."` SET `sequence`=".$faq['sequence']." WHERE `sequence`=".$sq." ";
				$db->get($sql,__FILE__,__LINE__);
				$sql = "UPDATE `".TG."` SET `sequence`='".$sq."' WHERE `gallery_id`=".$row['gallery_id']." ";
				$db->get($sql,__FILE__,__LINE__);
			}
		}
		$f->Redirect(CP."?index=List&msg=".urlencode("Sequence has been successfully updated"));

	}

	
	
	//All the Gallery photos are fetched
	$sql = "SELECT * FROM `".TG."` ORDER BY `sequence`";
	$res = $db->get($sql);
	$records = $db->num_rows($res);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<?php include("js.css.inc.php");?>
<script type="text/javascript">
$(document).ready(function(){
		$('.delete').click(function()

		{

			var href = $(this).attr('href');

			var text = '<div id="a" align="center"><strong>Are you sure you want to delete the Gallery Photo?</strong></div>';

			jConfirm(text, 'Confirmation', function(r){

				if(r == true){

					window.location.href = href;

				}

			});

			return false;

		});
		

});
function __doPostSQ() {
    var str = "";
    flag = false;
    var sq = document.getElementById('sq').value;
    sq = sq.split(",");
    //alert(sq.length);
    for(var j=0;j<sq.length;j++) {
        str = document.getElementById('sq' + sq[j]).value;
        if(str == 0 || str == "") {
            flag = true;
        }
    }
    if(flag == true) {
        alert("Sequence can not be blank and should be greater than zero");
    } else {
        document.frmSchedule.submit();
    }
}
</script>
</head>
<body>
<!--main-->
<div id="main">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td align="center" valign="top"><table width="1131" border="0" align="center" cellpadding="0" cellspacing="0">
      <tr>
        <td align="center" valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0">
		  <tr>
		  	<td colspan="2">
				<?php include("header.inc.php");?>				
			</td>
		  </tr>          
		  <tr>
            <td height="22" colspan="2" align="left" valign="top" bgcolor="#444444"><table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <th width="30" scope="row">&nbsp;</th>
                <td width="1101" class="style3">GALLERY</td>
              </tr>
            </table></td>
          </tr>
          <tr>
            <td height="22" colspan="2" align="left" valign="top" bgcolor="#bcbcbc"><table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
              <tr>
                <th width="17" align="left" valign="middle" scope="row">&nbsp;</th>
                <td width="1114" align="left" valign="middle"><a href="gallerydetail.php?index=Add">ADD IMAGE</a></td>
              </tr>
            </table></td>
          </tr>
        </table></td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td align="left" valign="top" class="contaner">&nbsp;</td>
  </tr>
  <?php if($msg!=""){
  ?>
  <tr>
    <td align="center" valign="top" class="contaner" height="30"><?php echo urldecode($msg);?></td>
  </tr>
  <?php
  }?>
</table>
  
<div class="contaner">
<form name="frmSchedule" id="frmSchedule" action="<?php echo CP.'?index=Sequence&action=SQ';?>" method="post">
  <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0" class="gal_area">
    <tr>
      <td  width="17" height="40" align="center" valign="middle" bgcolor="#d9d6d6">&nbsp;</td>
    <td  width="153" align="left" valign="middle" bgcolor="#d9d6d6" >IMAGE</td>
    <td width="678" align="center" valign="middle" bgcolor="#d9d6d6" >&nbsp;</td>
    <td width="6" align="center" valign="middle" class="" >&nbsp;</td>
    <td width="136" align="center" valign="middle" bgcolor="#d9d6d6" >APPLY SORT</td>
    <td width="6" align="center" valign="middle" >&nbsp;</td>
    <td width="135" align="center" valign="middle" bgcolor="#d9d6d6">ACTIONS</td>
  </tr>
  <?php if($records>0){
  	$cnt = 0;
  	while($row = $db->fetch_array($res)){
		$thumbnail = "../uploads/gallery/thumbnail/".$row['thumbnail_image'];
		
		if($cnt == 0){
			$bgcolor = "#e6e6e6";
			$cnt++;
		}
		else if($cnt == 1){
			$bgcolor = "#d9d6d6";
			$cnt--;
		}
  ?>
    <tr>
      <td align="center" valign="middle" bgcolor="<?php echo $bgcolor;?>" >&nbsp;</td>
      <td align="left" valign="middle" bgcolor="<?php echo $bgcolor;?>" ><img src="<?php echo $thumbnail;?>" height="55" width="55" alt="" /></td>
      <td align="center" valign="middle" bgcolor="<?php echo $bgcolor;?>" >&nbsp;</td>
      <td align="center" valign="middle" class="" >&nbsp;</td>
      <td align="center" valign="middle" bgcolor="<?php echo $bgcolor;?>"><input type="text" size="1" class="input13" value="<?php echo $row['sequence'];?>" id="sq<?php echo $row['gallery_id'];?>" name="sq<?php echo $row['gallery_id'];?>" style="text-align: center;" /></td>
      <td align="center" valign="middle" ></td>
      <td align="center" valign="middle" bgcolor="<?php echo $bgcolor;?>"><table width="60%" border="0" align="center" cellpadding="0" cellspacing="0" >
        <tr>
          <th width="60%" align="center" valign="middle" scope="col"><a href="gallerydetail.php?index=Edit&Id=<?php echo $row['gallery_id'];?>"><img src="images/pencil.png" width="14" height="14" alt="" /></a></th>
          <th width="40%" align="center" valign="middle" scope="col"><a href="<?php echo CP;?>?index=List&action=delete&Id=<?php echo $row['gallery_id'];?>" class="delete"><img src="images/delite.png" width="14" height="13" alt="" /></a></th>
        </tr>
      </table></td>
    </tr>
	<?php 
			$counter.= $row['gallery_id'].",";
		}
		$counter = substr($counter,0,strlen($counter)-1);
		?>
		<input name="sq" type="hidden" id="sq" value="<?php echo $counter;?>" />
        <input name="type" type="hidden" id="type" value="Sequence" />
<?php 

	}else{
	?>
	  <tr>
		<th height="25" align="center" bgcolor="#e6e6e6" colspan="7">No Records Found</th>
	  </tr>
	 <?php 	
	 }
	?>
    <tr>
      <td align="center" valign="middle" >&nbsp;</td>
      <td align="left" valign="middle" >&nbsp;</td>
      <td align="center" valign="middle" >&nbsp;</td>
      <td align="center" valign="middle" class="" >&nbsp;</td>
      <td align="center" valign="middle">&nbsp;</td>
      <td align="center" valign="middle">&nbsp;</td>
      <td align="center" valign="middle">&nbsp;</td>
    </tr>
    <tr>
      <td align="center" valign="middle" >&nbsp;</td>
      <td align="left" valign="middle" >&nbsp;</td>
      <td align="center" valign="middle" >&nbsp;</td>
      <td align="center" valign="middle" class="" >&nbsp;</td>
      <td align="center" valign="middle"><input type="button" value="SAVE SORT" class="input2" onclick="javascript:__doPostSQ();" name="btnChangeSQ"  id="btnChangeSQ" <?php if($records == 0) {?> disabled="disabled"<?php }?> /></td>
      <td align="center" valign="middle">&nbsp;</td>
      <td align="center" valign="middle">&nbsp;</td>
    </tr>
  </table>
 </form> 
  <div class="clear"></div>
</div>  
  
<div class="clear"></div>
</div>
<!--main-->
</body>
</html>
