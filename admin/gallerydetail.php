<?php
	require_once("../includes/config.inc.php");
	$f->redirectBase = WEBSITE_URL;
	$f->isLogin('_admin','index.php');
	
	$page_id = 4;
	
	define("TG","tbl_gallery",true);

	$index = $_GET['index'];
	$msg = $_GET['msg'];
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<?php include("js.css.inc.php");?>
<script type="text/javascript">
$(document).ready(function(){
	$('#frmGallery').validate();
	$("#main_image").rules("add", {
		<?php if($index=="Add"){
		?>
		required: true,
		<?php
		}?>
		accept: 'jpg|jpeg|png|gif|JPG|JPEG|PNG|GIF',
		messages: {
			accept: "Accept only jpg, png, gif files"
		}
	});
	$("#thumbnail_image").rules("add", {
		<?php if($index=="Add"){
		?>
		required: true,
		<?php
		}?>
		accept: 'jpg|jpeg|png|gif|JPG|JPEG|PNG|GIF',
		messages: {
			accept: "Accept only jpg, png, gif files"
		}
	});
});
</script>
</head>
<body>
<!--main-->
<div id="main">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td align="center" valign="top"><table width="1131" border="0" align="center" cellpadding="0" cellspacing="0">
      <tr>
        <td align="center" valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0">
		 <tr>
		  	<td colspan="2">
				<?php include("header.inc.php");?>				
			</td>
		  </tr>            
		  <tr>
            <td height="22" colspan="2" align="left" valign="top" bgcolor="#444444"><table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <th width="30" scope="row">&nbsp;</th>
                <td width="1101" class="style3"><?php echo strtoupper($index);?> GALLERY</td>
              </tr>
            </table></td>
          </tr>
          <tr>
            <td height="22" colspan="2" align="left" valign="top" bgcolor="#bcbcbc"><table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <th width="30" scope="row">&nbsp;</th>
                <td width="1101" class="style4"><a href="gallery.php">BACK TO LIST</a></td>
              </tr>
            </table></td>
          </tr>
        </table></td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td align="left" valign="top" class="contaner">&nbsp;</td>
  </tr>
  </table>

<div class="contaner">
<?php if($index == "Add"){
	if(isset($_POST['btnSave']) && empty($_POST['btnSave'])==false){
	
		if($_FILES['main_image']['name']!=""){
			require_once('../includes/file.upload.inc.php');

			$objFileUpload = new FileUpload();
									
			$objFileUpload->UploadContent = $_FILES['main_image'];

			$objFileUpload->UploadFolder = "../uploads/gallery";

			$image_return = $objFileUpload->Upload();
			
			$main_image = $image_return['server_name'];
		}
		
		if($_FILES['thumbnail_image']['name']!=""){
			require_once('../includes/file.upload.inc.php');

			$objFileUpload = new FileUpload();
									
			$objFileUpload->UploadContent = $_FILES['thumbnail_image'];

			$objFileUpload->UploadFolder = "../uploads/gallery/thumbnail";

			$image_return = $objFileUpload->Upload();
			
			$thumbnail_image = $image_return['server_name'];
		}
		
		$sequence_id = $m->getMaxSequence(TG);
		
		$insert_sql_array = array("main_image" => $main_image,
								  "thumbnail_image" => $thumbnail_image,
								  "sequence" => $sequence_id);
		$db->insert(TG,$insert_sql_array);
		
		
		$f->Redirect("gallery.php?msg=".urlencode("Gallery details successfully added!"));


	}

?>
<form name="frmGallery" id="frmGallery" action="<?php echo CP."?".QS;?>" method="post" enctype="multipart/form-data">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr >
    <td height="10" width="147" align="center" valign="middle" bgcolor="#d9d6d6" ></td>
    <td width="2" align="left" valign="middle" ></td>
    <td width="30" align="left" valign="middle"  bgcolor="#d9d6d6"></td>
    <td width="952" align="left" valign="middle"  bgcolor="#d9d6d6"></td>
  </tr>
  <tr >
    <td width="147" align="center" valign="top"  bgcolor="#d9d6d6">MAIN</td>
    <td width="2" align="left" valign="middle" ></td>
    <td width="30" align="left" valign="top"  bgcolor="#d9d6d6">&nbsp;</td>
    <td width="952" align="left" valign="middle"  bgcolor="#d9d6d6"><table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
      <tr>
          <th width="27%" align="left" valign="top" scope="row"><input type="file" name="main_image" id="main_image" /></th>
          <td width="73%" align="left" valign="top">Size should be exactly 512 in height and any width up to 1100, 72 dpi</td>
        </tr>
    </table></td>
  </tr>
  <tr >
    <td width="147" align="center" valign="top" bgcolor="#d9d6d6">&nbsp;</td>
    <td width="2" align="left" valign="middle" scope="col"></td>
    <td width="30" align="left" valign="middle"  bgcolor="#d9d6d6">&nbsp;</td>
    <td width="952" align="left" valign="middle"  bgcolor="#d9d6d6">&nbsp;</td>
  </tr>
  
  <tr >
    <td align="center" valign="top" bgcolor="#e6e6e6">&nbsp;</td>
    <td align="left" valign="middle" ></td>
    <td align="left" valign="middle"  bgcolor="#e6e6e6">&nbsp;</td>
    <td align="left" valign="middle"  bgcolor="#e6e6e6">&nbsp;</td>
  </tr>
  <tr >
    <td align="center" valign="top" bgcolor="#e6e6e6">THUMBNAIL</td>
    <td align="left" valign="middle" ></td>
    <td align="left" valign="middle" bgcolor="#e6e6e6">&nbsp;</td>
    <td align="left" valign="middle" bgcolor="#e6e6e6"><table width="100%" border="0" align="left" cellpadding="0" cellspacing="0">
      <tr>
          <td width="27%" align="left" valign="top"><input type="file" name="thumbnail_image" id="thumbnail_image" /></td>
          <td width="73%" align="left" valign="top">Size: exactly 55 X 55, 72 dpi</td>
        </tr>
    </table></td>
  </tr>
  <tr >
    <td align="center" valign="top" bgcolor="#e6e6e6">&nbsp;</td>
    <td align="left" valign="middle" ></td>
    <td align="left" valign="middle" bgcolor="#e6e6e6">&nbsp;</td>
    <td align="left" valign="middle" bgcolor="#e6e6e6">&nbsp;</td>
  </tr>
  
  <tr >
    <td align="center" valign="top"  bgcolor="#e6e6e6">&nbsp;</td>
    <td align="left" valign="middle"></td>
    <td align="left" valign="middle" bgcolor="#e6e6e6">&nbsp;</td>
    <td align="left" valign="middle" bgcolor="#e6e6e6">&nbsp;</td>
  </tr>
</table>
<table width="100%"  border="0" cellpadding="0" cellspacing="0">
  <tr>
    <td width="16%" scope="col">&nbsp;</td>
    <td  width="84%" scope="col">&nbsp;</td>
  </tr>
  <tr>
    <td scope="col">&nbsp;</td>
    <td scope="col"><input name="btnSave" id="btnSave" type="submit" value="SAVE" class="input2" /></td>
  </tr>
  <tr>
    <td scope="col">&nbsp;</td>
    <td scope="col">&nbsp;</td>
  </tr>
</table>
</form>
<?php }else if($index == "Edit"){

	$Id = $_GET['Id'];
	
	//Gallery details are fetched
	$sql_gal = "SELECT * FROM `".TG."` WHERE `gallery_id`='".$Id."'";
	$res_gal = $db->get($sql_gal);
	$row_gal = $db->fetch_array($res_gal);
	
	$main_image = "../uploads/gallery/".$row_gal['main_image'];
	$thumbnail_image = "../uploads/gallery/thumbnail/".$row_gal['thumbnail_image'];
	
	if(isset($_POST['btnSave']) && empty($_POST['btnSave'])==false){
	
		if($_FILES['main_image']['name']!=""){
			require_once('../includes/file.upload.inc.php');

			$objFileUpload = new FileUpload();
			
			$objFileUpload->UploadMode = 'Edit';

			$objFileUpload->OldFileName = $row_gal['main_image'];
									
			$objFileUpload->UploadContent = $_FILES['main_image'];

			$objFileUpload->UploadFolder = "../uploads/gallery";

			$image_return = $objFileUpload->Upload();
			
			$main_image = $image_return['server_name'];
		}else
			$main_image = $row_gal['main_image'];
		
		if($_FILES['thumbnail_image']['name']!=""){
			require_once('../includes/file.upload.inc.php');

			$objFileUpload = new FileUpload();
			
			$objFileUpload->UploadMode = 'Edit';

			$objFileUpload->OldFileName = $row_gal['thumbnail_image'];
																		
			$objFileUpload->UploadContent = $_FILES['thumbnail_image'];

			$objFileUpload->UploadFolder = "../uploads/gallery/thumbnail";

			$image_return = $objFileUpload->Upload();
			
			$thumbnail_image = $image_return['server_name'];
		}else
			$thumbnail_image = $row_gal['thumbnail_image'];
		
		$update_sql_array = array("main_image" => $main_image,
								  "thumbnail_image" => $thumbnail_image);
		$db->update(TG,$update_sql_array,"gallery_id",$Id);
		
		
		$f->Redirect(CP."?index=Edit&Id=".$Id."&msg=".urlencode("Gallery details successfully updated!"));


	}

?>
<form name="frmGallery" id="frmGallery" action="<?php echo CP."?".QS;?>" method="post" enctype="multipart/form-data">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr >
    <td height="10" width="147" align="center" valign="middle" bgcolor="#d9d6d6" ></td>
    <td width="2" align="left" valign="middle" ></td>
    <td width="30" align="left" valign="middle"  bgcolor="#d9d6d6"></td>
    <td width="952" align="left" valign="middle"  bgcolor="#d9d6d6"></td>
  </tr>
  <tr >
    <td width="147" align="center" valign="top"  bgcolor="#d9d6d6">MAIN</td>
    <td width="2" align="left" valign="middle" ></td>
    <td width="30" align="left" valign="top"  bgcolor="#d9d6d6">&nbsp;</td>
    <td width="952" align="left" valign="middle"  bgcolor="#d9d6d6"><table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
      <tr>
          <th width="63%" align="left" valign="top" scope="row"><img src="resize.php?imgfile=<?php echo $main_image;?>&max_height=511&max_width=413" alt="" /></th>
          <td width="37%" align="left" valign="top">Size should be exactly 512 in height and any width up to 1100, 72 dpi</td>
        </tr>
    </table></td>
  </tr>
  <tr >
    <td width="147" align="center" valign="top" bgcolor="#d9d6d6">&nbsp;</td>
    <td width="2" align="left" valign="middle" scope="col"></td>
    <td width="30" align="left" valign="middle"  bgcolor="#d9d6d6">&nbsp;</td>
    <td width="952" align="left" valign="middle"  bgcolor="#d9d6d6">&nbsp;</td>
  </tr>
  <tr >
    <td align="center" valign="top" bgcolor="#d9d6d6">&nbsp;</td>
    <td align="left" valign="middle" ></td>
    <td align="left" valign="middle" bgcolor="#d9d6d6">&nbsp;</td>
    <td align="left" valign="middle"  bgcolor="#d9d6d6"><table width="544" border="0" cellpadding="0" cellspacing="0">
      <tr>
        <td width="91" scope="col">Upload Image</td>
        <td width="453" scope="col"><input type="file" name="main_image" id="main_image" /></td>
      </tr>
    </table></td>
  </tr>
  <tr >
    <td align="center" valign="top"  bgcolor="#d9d6d6">&nbsp;</td>
    <td align="left" valign="middle" ></td>
    <td align="left" valign="middle"  bgcolor="#d9d6d6">&nbsp;</td>
    <td align="left" valign="middle"  bgcolor="#d9d6d6">&nbsp;</td>
  </tr>
  <tr >
    <td align="center" valign="top" bgcolor="#e6e6e6">&nbsp;</td>
    <td align="left" valign="middle" ></td>
    <td align="left" valign="middle"  bgcolor="#e6e6e6">&nbsp;</td>
    <td align="left" valign="middle"  bgcolor="#e6e6e6">&nbsp;</td>
  </tr>
  <tr >
    <td align="center" valign="top" bgcolor="#e6e6e6">THUMBNAIL</td>
    <td align="left" valign="middle" ></td>
    <td align="left" valign="middle" bgcolor="#e6e6e6">&nbsp;</td>
    <td align="left" valign="middle" bgcolor="#e6e6e6"><table width="100%" border="0" align="left" cellpadding="0" cellspacing="0">
      <tr>
          <td width="24%" align="left" valign="top"><img src="<?php echo $thumbnail_image;?>" width="55" height="55" alt="" /></td>
          <td width="76%" align="left" valign="top">Size: exactly 55 X 55, 72 dpi</td>
        </tr>
    </table></td>
  </tr>
  <tr >
    <td align="center" valign="top" bgcolor="#e6e6e6">&nbsp;</td>
    <td align="left" valign="middle" ></td>
    <td align="left" valign="middle" bgcolor="#e6e6e6">&nbsp;</td>
    <td align="left" valign="middle" bgcolor="#e6e6e6">&nbsp;</td>
  </tr>
  <tr >
    <td align="center" valign="top" bgcolor="#e6e6e6">&nbsp;</td>
    <td align="left" valign="middle" ></td>
    <td align="left" valign="middle" bgcolor="#e6e6e6">&nbsp;</td>
    <td align="left" valign="middle" bgcolor="#e6e6e6"><table width="544" border="0" cellpadding="0" cellspacing="0">
      <tr>
        <td width="91" scope="col">Upload Image</td>
        <td width="453" scope="col"><input type="file" name="thumbnail_image" id="thumbnail_image" /></td>
      </tr>
    </table></td>
  </tr>
  <tr >
    <td align="center" valign="top"  bgcolor="#e6e6e6">&nbsp;</td>
    <td align="left" valign="middle"></td>
    <td align="left" valign="middle" bgcolor="#e6e6e6">&nbsp;</td>
    <td align="left" valign="middle" bgcolor="#e6e6e6">&nbsp;</td>
  </tr>
</table>
<table width="100%"  border="0" cellpadding="0" cellspacing="0">
  <tr>
    <td width="16%" scope="col">&nbsp;</td>
    <td  width="84%" scope="col">&nbsp;</td>
  </tr>
  <tr>
    <td scope="col">&nbsp;</td>
    <td scope="col"><input name="btnSave" id="btnSave" type="submit" value="SAVE" class="input2" /></td>
  </tr>
  <tr>
    <td scope="col">&nbsp;</td>
    <td scope="col">&nbsp;</td>
  </tr>
</table>
</form>
<?php }?>
<div class="clear"></div>
</div>  
<div class="clear"></div>
</div>
<!--main-->
</body>
</html>
