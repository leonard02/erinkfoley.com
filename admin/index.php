<?php
	require_once("../includes/config.inc.php");
	
	if(isset($_SESSION['_admin']) && empty($_SESSION['_admin'])==false)
		$f->Redirect("getStarted.php");
		
	$msg = '&nbsp;';
	define("T","`tbl_admin`",true);
	if(!empty($_GET['action']) && $_GET['action']=="1"):
		$msg = $f->getHtmlMessage('You have been successfully logged out');
	endif;
	if(isset($_POST['btnLogin'])) {
		$username = $f->setValue($_POST['username']);
		$password = $f->makePassword($f->setValue($_POST['password']));
		
		$sql="SELECT * FROM ".T." WHERE `username`='".$username."' AND `password`='".$password."'";
		$res = $db->get($sql,__FILE__,__LINE__);
		
		if($db->num_rows($res) > 0):
		$row_admin = $db->fetch_array($res);
		$_SESSION['u_id']=$row_admin['admin_id'];
		$_SESSION['u_type']=$row_admin['user_type'];
		$_SESSION['_admin'] = $username;
			if(empty($_POST['redirect'])==false) {
				$gotoURL = base64_decode($_POST['redirect']);
			} else {
				$gotoURL = "getStarted.php";
			}
			$f->Redirect($gotoURL);
			exit();
		else:
			$msg = $f->getHtmlError('Invalid Username or bad Password');
		endif;
	}
	
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<?php include("js.css.inc.php");?>
<script type="text/javascript">
$(document).ready(function() {
	$('#frmLogin').validate();	
});
</script>
</head>
<body>
<!--main-->
<div id="main">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
    <td align="center" valign="top"><table width="1131" border="0" align="center" cellpadding="0" cellspacing="0">
      <tr>
        <td align="center" valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0">
		  <tr>
		  	<td colspan="2">
				<?php include("header.inc.php");?>				
			</td>
		  </tr>
		  <tr>
			<td height="22" colspan="2" align="left" valign="top" bgcolor="#444444">&nbsp;</td>
		  </tr>
		  <tr>
			<td height="22" colspan="2" align="left" valign="top" bgcolor="#bcbcbc">&nbsp;</td>
			</tr>
        </table></td>
      </tr>
    </table></td>
  </tr>  
 <tr>
    <td align="left" valign="top" class="contaner">&nbsp;</td>
  </tr>
  </table>
 
<div class="contaner">
<form action="<?php echo CP;?>" method="post" name="frmLogin" id="frmLogin">
  <table width="100%" border="0" cellspacing="0" cellpadding="0" >
  <?php
 if($msg!='&nbsp;')
 {
 ?>
  <tr>
    <td height="74" align="center"><?php echo $msg;?></td>
  </tr>
  <?php
  }
  ?>
  <tr>
    <td><h1>Welcome to Erin Foley<br/>Content Management System</h1>
    </td>
  </tr>
  <tr>
    <td align="center" valign="top"><table width="355" border="0" align="center" cellpadding="0" cellspacing="0">
      <tr>
        <td width="99" align="left" valign="middle">USER NAME:</td>
        <td colspan="2" align="right" valign="middle"><input name="username" type="text" id="username" class="required input1" value="<?php echo $_POST['username'];?>" /></td>
      </tr>
      <tr>
        <td width="99" align="left" valign="middle">PASSWORD:</td>
        <td colspan="2" align="right" valign="middle"><input name="password" type="password" id="password" class="required input1" size="40" /></td>
      </tr>
      <tr>
        <td align="left" valign="middle">&nbsp;</td>
        <td width="100" align="left" valign="middle">
			<input name="btnLogin" type="submit" value="LOGIN" class="input2" />
		</td>
        <td width="165" align="right" valign="middle" class="style1"><a href="forgot_password.php">( FORGOT PASSWORD? )</a></td>
        <td width="1"></td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td align="center" valign="top">&nbsp;</td>
  </tr>
  <tr>
    <td align="center" valign="top">&nbsp;</td>
  </tr>
  </table>
 </form>
  <div class="clear"></div>
</div>   
<div class="clear"></div>
</div>
<!--main-->
</body>
</html>
