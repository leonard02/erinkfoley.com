<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>:: Erin Foley CMS ::</title>
<meta http-equiv="Pragma" content="no-cache" />
<meta http-equiv="Expires" content="0" />
<!-- For Search Engine Robots -->
<meta name="robots" content="noindex, nofollow, noarchive" />
<link href="style.css" rel="stylesheet" type="text/css" />
<link href="../js/alert/jquery.alerts.css" rel="stylesheet" type="text/css" />

<?php
	// JavaScript include library
	$js_include = array(
		"../js/jquery-1.7.2.js",
		"../js/function.js",
		"../js/form.validation.js",
		"../js/jquery.validate.js",
		"../js/alert/jquery.alerts.js"
	);
	
	foreach($js_include as $js)
	{
		echo "<script type=\"text/javascript\" src=\"".$js."\"></script>\n";
	}
?>
<script type="text/javascript">
$(document).ready(function(){
		$('.logout').click(function()

		{

			var href = $(this).attr('href');

			var title = $(this).attr('rel');

			var text = '<div id="a" align="center"><strong>Are you sure you want to Logout?</div><div>';

			jConfirm(text, 'Confirmation', function(r){

				if(r == true){

					window.location.href = href;

				}

			});

			return false;

		});
		

});
</script>
