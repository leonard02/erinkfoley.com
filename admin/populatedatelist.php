<?php
	require_once("../includes/config.inc.php");
	$f->redirectBase = WEBSITE_URL;
	$f->isLogin('_admin','index.php');
	
	$from_date = $dt->dd_mm_yyyy2yyyy_mm_dd2($_POST['from_date'],"-")." 00:00:00";
	$to_date = $dt->dd_mm_yyyy2yyyy_mm_dd2($_POST['to_date'],"-")." 00:00:00";	
	$populatedates = $_POST['populatedates'];
	
	//Date difference is find out
	$date_diff = $dt->dateDiff($to_date,$from_date);
?>
<link rel="stylesheet" href="http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css" />
<script src="http://code.jquery.com/jquery-1.9.1.js"></script>
<script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
<script src="../js/jquery-ui-timepicker-addon.js" type="text/javascript"></script>
<script type="text/javascript">

var $j = jQuery.noConflict();

$j(function(){
	$j("#from_date").datepicker({
            dateFormat: 'mm-dd-yy',
            changeMonth: true,
            changeYear: true,
            showOn: 'button',
            buttonImage: 'images/calender.png'
        });

	$j("#to_date").datepicker({
            dateFormat: 'mm-dd-yy',
            changeMonth: true,
            changeYear: true,
            showOn: 'button',
            buttonImage: 'images/calender.png'
        });
	$j('.showdate').datepicker({
		dateFormat: 'mm-dd-yy'
	});
	$j( ".showtime" ).timepicker({
		timeFormat: "hh:mm tt"
	});

});
</script>
<table width="97%" border="0" cellspacing="0" cellpadding="0">
	<?php 
	if($populatedates=="Yes"){
	for($i=0;$i<=$date_diff;$i++){
		$to_date=date("m-d-Y",strtotime($dt->dayadd($from_date,$i)));
	?>
	  <tr>
		<th align="left" valign="middle">Day <?php echo $i+1;?></th>
		<th align="left" valign="middle"><input name="show_date[]" id="show_date<?php echo $i;?>" type="text" value="<?php echo $to_date;?>" class="input18 showdate" /></th>
		<th align="left" valign="middle">Time 1</th>
		<th align="left" valign="middle" scope="col"><input name="from_time[]" id="from_time<?php echo $i;?>" type="text" value="" class="input19 showtime" /></th>
		<th align="left" valign="middle">Time 2</th>
		<th align="left" valign="middle"><input name="to_time[]" id="to_time<?php echo $i;?>" type="text" value="" class="input19 showtime" /></th>
	  </tr>
	 <?php }
	 }else{
	 	for($i=1;$i<=7;$i++){
		?>
		  <tr>
			<th align="left" valign="middle">Day <?php echo $i;?></th>
			<th align="left" valign="middle"><input name="show_date[]" id="show_date<?php echo $i;?>" type="text" value="" class="input18 showdate" /></th>
			<th align="left" valign="middle">Time 1</th>
			<th align="left" valign="middle" scope="col"><input name="from_time[]" id="from_time<?php echo $i;?>" type="text" value="" class="input19 showtime" /></th>
			<th align="left" valign="middle">Time 2</th>
			<th align="left" valign="middle"><input name="to_time[]" id="to_time<?php echo $i;?>" type="text" value="" class="input19 showtime" /></th>
		  </tr>
		 <?php }
	 }
	 ?>
</table>