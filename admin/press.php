<?php
	require_once("../includes/config.inc.php");
	$f->redirectBase = WEBSITE_URL;
	$f->isLogin('_admin','index.php');
	
	$page_id = 3;
	
	define("TP","tbl_press",true);
	define("TPAI","tbl_press_addl_images",true);

	$index = $_GET['index'];
	$msg = $_GET['msg'];
	
	//Press details are fetched
	$sql_press = "SELECT * FROM `".TP."` ORDER BY sequence";
	$res_press = $db->get($sql_press);
	$num_press = $db->num_rows($res_press);
	
	if(isset($_GET['action']) && $_GET['action']=="delete"){
		$Id = $_GET['Id'];
		
		//image path is fetched
		$sql_press = "SELECT * FROM ".TP." WHERE `press_id`='".$Id."'";
		$res_press = $db->get($sql_press);
		$row_press = $db->fetch_array($res_press);
		unlink("../uploads/press/".$row_press['press_image']);
		
		$sql = "DELETE FROM `".TP."` WHERE `press_id`='".$Id."'";
		$db->get($sql);
		
		//Press images are fetched
		$sql_press = "SELECT * FROM ".TPAI." WHERE `press_id`='".$Id."'";
		$res_press = $db->get($sql_press);
		while($row_press = $db->fetch_array($res_press)){
			unlink("../uploads/press/".$row_press['image_path']);
		}
		
		$sql = "DELETE FROM `".TPAI."` WHERE `press_id`='".$Id."'";
		$db->get($sql);
		
		$f->Redirect(CP."?msg=".urlencode("Record successfully deleted!"));
	}

	if($index == 'Sequence' && $_GET['action']=='SQ'){
		$sql = "SELECT * FROM `".TP."` ORDER BY `sequence` ASC";
		$res = $db->get($sql);
		while($row = $db->fetch_array($res)) {
			$sq = $_POST['sq'.$row['press_id']];
			if($sq != $row['sequence']) {
				$sql = "SELECT * FROM ".TP." WHERE `press_id`=".$row['press_id'];
				$res_faq = $db->get($sql,__FILE__,__LINE__);
				$faq = $db->fetch_array($res_faq);
				$sql = "UPDATE `".TP."` SET `sequence`=".$faq['sequence']." WHERE `sequence`=".$sq." ";
				$db->get($sql,__FILE__,__LINE__);
				$sql = "UPDATE `".TP."` SET `sequence`='".$sq."' WHERE `press_id`=".$row['press_id']." ";
				$db->get($sql,__FILE__,__LINE__);
			}
		}
		$f->Redirect(CP."?index=List&msg=".urlencode("Sequence has been successfully updated"));

	}

	
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<?php include("js.css.inc.php");?>
<script type="text/javascript">
$(document).ready(function(){
		$('.delete').click(function()

		{

			var href = $(this).attr('href');

			var title = $(this).attr('rel');

			var text = '<div id="a" align="center"><strong>Are you sure you want to delete the Press?</div><br><div id="b" align="center"><strong>'+title+'</strong><div>';

			jConfirm(text, 'Confirmation', function(r){

				if(r == true){

					window.location.href = href;

				}

			});

			return false;

		});
		

});
function __doPostSQ() {
    var str = "";
    flag = false;
    var sq = document.getElementById('sq').value;
    sq = sq.split(",");
    //alert(sq.length);
    for(var j=0;j<sq.length;j++) {
        str = document.getElementById('sq' + sq[j]).value;
        if(str == 0 || str == "") {
            flag = true;
        }
    }
    if(flag == true) {
        alert("Sequence can not be blank and should be greater than zero");
    } else {
        document.frmSchedule.submit();
    }
}
</script>
</head>
<body>
<!--main-->
<div id="main">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td align="center" valign="top"><table width="1131" border="0" align="center" cellpadding="0" cellspacing="0">
      <tr>
        <td align="center" valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0">
		 <tr>
		  	<td colspan="2">
				<?php include("header.inc.php");?>				
			</td>
		  </tr>            
          <tr>
            <td height="22" colspan="2" align="left" valign="top" bgcolor="#444444"><table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <th width="30" scope="row">&nbsp;</th>
                <td width="1101" class="style3">PRESS</td>
              </tr>
            </table></td>
          </tr>
          <tr>
            <td height="22" colspan="2" align="left" valign="top" bgcolor="#bcbcbc"><table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <th width="30" scope="row">&nbsp;</th>
                <td width="1101" class="style4" style="background:none; padding-left:0px;"><a href="pressdetail.php?index=Add">ADD NEW PRESS</a></td>
              </tr>
            </table></td>
          </tr>
        </table></td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td align="left" valign="top" class="contaner">&nbsp;</td>
  </tr>
    <?php if($msg!=""){
  ?>
  <tr>
    <td align="center" valign="top" class="contaner" height="30"><?php echo urldecode($msg);?></td>
  </tr>
  <?php
  }?>

  </table>
<div class="contaner">
<form name="frmSchedule" id="frmSchedule" action="<?php echo CP.'?index=Sequence&action=SQ';?>" method="post">
<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td align="center" valign="top">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<?php if($num_press>0){
	
	$cnt = 0;
	while($row_press = $db->fetch_array($res_press)){
			if($cnt == 0){
				$bgcolor = "#d9d6d6";
				$cnt++;
			}
			else if($cnt == 1){
				$bgcolor = "#e6e6e6";
				$cnt--;
			}

?>
  <tr>
    <th width="30" height="25" align="center" bgcolor="<?php echo $bgcolor;?>" scope="col">&nbsp;</th>
    <th width="132" align="left" valign="middle" bgcolor="<?php echo $bgcolor;?>" scope="col"><?php echo $f->getValue($row_press['headline']);?></th>
    <th width="1" align="center"></th>
    <th width="71" align="right" valign="middle" bgcolor="<?php echo $bgcolor;?>" scope="col"><a href="pressdetail.php?index=Edit&Id=<?php echo $row_press['press_id'];?>"><img src="images/pencil.png" width="14" height="14" alt="" /></a></th>
    <th width="91" align="center" valign="middle" bgcolor="<?php echo $bgcolor;?>" scope="col"><a href="<?php echo CP;?>?index=List&action=delete&Id=<?php echo $row_press['press_id'];?>" class="delete" rel="<?php echo $f->getValue($row_press['headline']);?>"><img src="images/delite.png" width="14" height="13" alt="" /></a></th>
    <th width="101" bgcolor="<?php echo $bgcolor;?>"><input type="text" size="1" class="input7" value="<?php echo $row_press['sequence'];?>" id="sq<?php echo $row_press['press_id'];?>" name="sq<?php echo $row_press['press_id'];?>" style="text-align: center;" /></th>
    <th width="705" bgcolor="<?php echo $bgcolor;?>" scope="col">&nbsp;</th>
  </tr>
<?php 
		$counter.= $row_press['press_id'].",";

	}
	$counter = substr($counter,0,strlen($counter)-1);
?>
		<input name="sq" type="hidden" id="sq" value="<?php echo $counter;?>" />
        <input name="type" type="hidden" id="type" value="Sequence" />
<?php 
}else{
?>
  <tr>
    <th height="25" align="center" bgcolor="#e6e6e6" colspan="7">No Records Found</th>
  </tr>
 <?php }?>
</table>    
    </td>
    </tr>
 </table>
<table width="100%" border="0" cellpadding="0" cellspacing="0">
  <tr>
    <td width="325" scope="col">&nbsp;</td>
    <td width="806" scope="col">&nbsp;</td>
  </tr>
  <tr>
    <td scope="col">&nbsp;</td>
    <td scope="col"><input type="button" value="SAVE SORT" class="input2" onclick="javascript:__doPostSQ();" name="btnChangeSQ"  id="btnChangeSQ" <?php if($num_press == 0) {?> disabled="disabled"<?php }?> /></td>
  </tr>
  <tr>
    <td scope="col">&nbsp;</td>
    <td scope="col">&nbsp;</td>
  </tr>
</table>
</form>
    
    
    
   
   
   
   
   
<table width="100%" border="0" cellspacing="0" cellpadding="0">

        <td>&nbsp;</td>
        </tr>
    </table>
<div class="clear"></div>
</div>  
  
  
  
<div class="clear"></div>
</div>
<!--main-->
</body>
</html>
