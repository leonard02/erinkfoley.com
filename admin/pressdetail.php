<?php
	require_once("../includes/config.inc.php");
	$f->redirectBase = WEBSITE_URL;
	$f->isLogin('_admin','index.php');
	
	$page_id = 3;
	
	define("TP","tbl_press",true);
	define("TPAI","tbl_press_addl_images",true);
	
	$msg = $_GET['msg'];

	$index = $_GET['index'];
	
	
	if($index == "Edit"){
		$Id = $_GET['Id'];
		
		if($_GET['action']=="delete"){
		
			$addl_id = $_GET['photoid'];

			//All the photos are fetched and displayed here
			$sql_photos = "SELECT * FROM `".TPAI."` WHERE `addl_id`='".$addl_id."'"; 
			$res_photos = $db->get($sql_photos);
			$row_photos = $db->fetch_array($res_photos);
			
			@unlink("../uploads/press/".$row_photos['image_path']);
			
			$db->get("DELETE FROM `".TPAI."` WHERE `addl_id`='".$addl_id."'");
			
			$msg = "Additional image successfully deleted!";
			$f->Redirect(CP."?index=Edit&Id=".$Id."&msg=".urlencode($msg));
			
		}
				
		//All the photos are fetched and displayed here
		$sql_photos = "SELECT * FROM `".TPAI."` WHERE `press_id`='".$Id."' ORDER BY `sequence`"; 
		$res_photos = $db->get($sql_photos);
		$num_records = $db->num_rows($res_photos);
	}
	
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<?php include("js.css.inc.php");?>
<script src="ckeditor/ckeditor.js" type="text/javascript"></script>
<script type="text/javascript">
$(document).ready(function(){
	$('#frmPress').validate();
	$("#press_image").rules("add", {
		<?php if($index=="Add"){
		?>
		required: true,
		<?php
		}?>
		accept: 'jpg|jpeg|png|gif|JPG|JPEG|PNG|GIF',
		messages: {
			accept: "Accept only jpg, png, gif files"
		}
	});
	
	<?php if($index == "Add"){?>
	var counter = 2;
 	<?php }else if($index == "Edit" && $num_records>0) {
	?>
	var counter = <?php echo $num_records+1;?>;
	<?php
	}else if($index == "Edit"){
	?>
	var counter = 2;
	<?php
	}?>
    $("#addtextbox").click(function() {
 
	if(counter>10){
            alert("Only 10 textboxes allow");
            return false;
	}   
 
	var newTextBoxDiv = $(document.createElement('div'))
	     .attr("id", 'TextBoxDiv' + counter);
 
	newTextBoxDiv.html('<input type="file" name="image_path' + counter + '" id="image_path' + counter + '" value="" > <a href="javascript:;" onClick="removeButton('+counter+')"><img src="images/delite.png" width="14" height="13" alt="" /></a>');
 
	newTextBoxDiv.appendTo("#TextBoxesGroup");
 
 
	counter++;
     });
	 
	$('.delete').click(function()
	{

		var href = $(this).attr('href');

		var text = '<div id="a" align="center"><strong>Are you sure you want to delete this photo?</strong><div>';

		jConfirm(text, 'Confirmation', function(r){

			if(r == true){

				window.location.href = href;

			}

		});

		return false;

	});

	 
 
});
 function removeButton(cntr){

	if(cntr==1){
	  alert("No more textbox to remove");
	  return false;
	}   
 
	$("#TextBoxDiv" + cntr).remove();
 
 }


</script>
</head>

<body>
<!--main-->
<div id="main">

<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td align="center" valign="top"><table width="1131" border="0" align="center" cellpadding="0" cellspacing="0">
      <tr>
        <td align="center" valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0">
		 <tr>
		  	<td colspan="2">
				<?php include("header.inc.php");?>				
			</td>
		  </tr>            
          <tr>
            <td height="22" colspan="2" align="left" valign="top" bgcolor="#444444"><table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <th width="30" scope="row">&nbsp;</th>
                <td width="1101" class="style3"><?php echo strtoupper($index);?> PRESS</td>
              </tr>
            </table></td>
          </tr>
          <tr>
            <td height="22" colspan="2" align="left" valign="top" bgcolor="#bcbcbc"><table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <th width="30" scope="row">&nbsp;</th>
                <td width="1101" class="style4"><a href="press.php">BACK TO LIST</a></td>
              </tr>
            </table></td>
          </tr>
        </table></td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td align="left" valign="top" class="contaner">&nbsp;</td>
  </tr>
  </table>
  
<div class="contaner">
<?php if($index == "Add"){

	if(isset($_POST['btnSave']) && empty($_POST['btnSave'])==false){
	
		if($_FILES['press_image']['name']!=""){
			require_once('../includes/file.upload.inc.php');

			$objFileUpload = new FileUpload();
									
			$objFileUpload->UploadContent = $_FILES['press_image'];

			$objFileUpload->UploadFolder = "../uploads/press";

			$image_return = $objFileUpload->Upload();
			
			$press_image = $image_return['server_name'];
		}
		
		$sequence_id = $m->getMaxSequence(TP);
		
		$insert_sql_array = array("headline" => $f->post('headline'),
								  "press_date" => $f->post('press_date'),
								  "press_details" => $f->post('press_details'),
								  "press_image" => $press_image,
								  "sequence" => $sequence_id,
								  "create_date" => 'CURDATE()',
								  "create_time" => 'CURTIME()');
		$db->insert(TP,$insert_sql_array);
		$press_id = $db->last_insert_id();
		
		
		for($i=1;$i<=10;$i++){
			if($_FILES['image_path'.$i]['name']!=""){
				require_once('../includes/file.upload.inc.php');
				
				$sequence_id = $m->getMaxSequence(TPAI,"",$press_id);
	
				$objFileUpload = new FileUpload();
										
				$objFileUpload->UploadContent = $_FILES['image_path'.$i];
	
				$objFileUpload->UploadFolder = "../uploads/press";
	
				$image_return = $objFileUpload->Upload();
				
				$image_path = $image_return['server_name'];
				
				$insert_sql_array = array("press_id" => $press_id,
								  	"image_path" => $image_path,
									"sequence" => $sequence_id);
				$db->insert(TPAI,$insert_sql_array);
			}
		}

		$f->Redirect("press.php?msg=".urlencode("Press details successfully added!"));


	}

?>
<form name="frmPress" id="frmPress" action="<?php echo CP.'?'.QS;?>" method="post" enctype="multipart/form-data">  
<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td width="30" height="10" align="left" valign="middle" bgcolor="#d9d6d6"></td>
    <td width="128" align="left" valign="middle" bgcolor="#d9d6d6"></td>
    <td width="1" align="left" valign="middle" scope="col"></td>
    <td width="20" align="left" valign="middle" bgcolor="#d9d6d6"></td>
    <td width="952" align="left" valign="middle" bgcolor="#d9d6d6"></td>	
  <tr>
    <td width="30" height="25" align="left" valign="middle" bgcolor="#d9d6d6">&nbsp;</td>
    <td width="128" align="left" valign="middle" bgcolor="#d9d6d6">Headline</td>
    <td width="1" align="left" valign="middle" scope="col"></td>
    <td width="20" align="left" valign="middle" bgcolor="#d9d6d6">&nbsp;</td>
    <td width="952" align="left" valign="middle" bgcolor="#d9d6d6"><input name="headline" id="headline" type="text" value="" class="input6 required" />
    </td>
  </tr>
  <tr>
    <td width="30" align="center" valign="middle" bgcolor="#d9d6d6">&nbsp;</td>
    <td width="128" align="left" valign="middle" bgcolor="#d9d6d6">Date</td>
    <td width="1" align="center" valign="middle" ></td>
    <td width="20" align="center" valign="middle" bgcolor="#d9d6d6">&nbsp;</td>
    <td width="952" align="left" valign="middle" bgcolor="#d9d6d6"><input name="press_date" id="press_date" type="text" value="" class="input6 required" />
    </td>
  </tr>
  <tr>
    <td height="10" align="center" valign="middle" bgcolor="#d9d6d6"></td>
    <td align="left" valign="middle" bgcolor="#d9d6d6"></td>
    <td align="center" valign="middle" scope="col"></td>
    <td align="center" valign="middle" bgcolor="#d9d6d6"></td>
    <td align="left" valign="middle" bgcolor="#d9d6d6"></td>
  </tr>
  <tr>
    <td align="center" valign="middle" class="line6" scope="col">&nbsp;</td>
    <td align="left" valign="middle" bgcolor="#d9d6d6">&nbsp;</td>
    <td align="center" valign="middle" scope="col"></td>
    <td align="center" valign="middle" bgcolor="#d9d6d6">&nbsp;</td>
    <td align="left" valign="middle" bgcolor="#d9d6d6"><span class="line6 align">
	<textarea name="press_details" cols="80" rows="10" id="press_details"></textarea>
	<script type="text/javascript">
		CKEDITOR_BASEPATH = '<?php echo WEBSITE_URL;?>/ckeditor/';
		CKEDITOR.replace('press_details', {toolbar : 'Full',width : '767px',height : '388px',
		filebrowserBrowseUrl : '<?php echo WEBSITE_URL;?>/ckeditor/filemanager/browser/default/browser.html?Connector=<?php echo WEBSITE_URL;?>/ckeditor/filemanager/connectors/php/connector.php',
		filebrowserImageBrowseUrl : '<?php echo WEBSITE_URL;?>/ckeditor/filemanager/browser/default/browser.html?Type=Image&Connector=<?php echo WEBSITE_URL;?>/ckeditor/filemanager/connectors/php/connector.php',
		filebrowserFlashBrowseUrl : '<?php echo WEBSITE_URL;?>/ckeditor/filemanager/browser/default/browser.html?Type=Flash&Connector=<?php echo WEBSITE_URL;?>/ckeditor/filemanager/connectors/php/connector.php',
		filebrowserUploadUrl : '<?php echo WEBSITE_URL;?>/ckeditor/filemanager/connectors/php/upload.php?Type=File',
		filebrowserImageUploadUrl : '<?php echo WEBSITE_URL;?>/ckeditor/filemanager/connectors/php/upload.php?Type=Image',
		filebrowserFlashUploadUrl : '<?php echo WEBSITE_URL;?>/ckeditor/filemanager/connectors/php/upload.php?Type=Flash'});
	</script>	

	</span></td>
  </tr>
  <tr>
    <td align="center" valign="middle" bgcolor="#d9d6d6">&nbsp;</td>
    <td align="left" valign="middle" bgcolor="#d9d6d6">&nbsp;</td>
    <td align="center" valign="middle" scope="col"></td>
    <td align="center" valign="middle" bgcolor="#d9d6d6">&nbsp;</td>
    <td align="left" valign="middle" bgcolor="#d9d6d6">&nbsp;</td>
  </tr>
  <tr>
    <td colspan="5" align="center" valign="middle"><table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
      <tr>
        <td width="34" align="center" valign="top" bgcolor="#d9d6d6">&nbsp;</td>
        <td width="151" align="left" valign="top" bgcolor="#d9d6d6">Main Image</td>
        <td width="1" align="center" valign="top"></td>
        <td width="18" align="center" valign="top" bgcolor="#d9d6d6">&nbsp;</td>
        <td width="579" align="left" valign="top" bgcolor="#d9d6d6"><table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td><table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
          <tr>
            <td width="16%" align="left" valign="middle" class="line6 align" scope="col">Upload Image</td>
            <td width="84%" align="left" valign="middle" bgcolor="#d9d6d6"><input type="file" name="press_image" id="press_image" value="" /></td>
          </tr>
        </table></td>
          </tr>
		  <tr>
            <td>Image should be sized to 220 x 277 exactly, 72 dpi</td>
          </tr>
        </table></td>
        <td width="517" height="30" align="left" valign="middle" bgcolor="#d9d6d6">&nbsp;</td>
      </tr>
      
      <tr>
        <td align="center" valign="top" bgcolor="#d9d6d6">&nbsp;</td>
        <td align="left" valign="top" bgcolor="#d9d6d6">&nbsp;</td>
        <td align="center" valign="top"></td>
        <td align="center" valign="top" bgcolor="#d9d6d6">&nbsp;</td>
        <td align="left" valign="top" bgcolor="#d9d6d6">&nbsp;</td>
        <td height="15" align="left" valign="top" bgcolor="#d9d6d6">&nbsp;</td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td colspan="5" align="center" valign="middle"><table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
      <tr>
        <td width="34" align="center" valign="top" bgcolor="#d9d6d6">&nbsp;</td>
        <td width="147" align="left" valign="top" bgcolor="#d9d6d6">
          <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td>Additional</td>
            </tr>
            <tr>
              <td>Images</td>
            </tr>
          </table></td>
        <td width="1" align="center" valign="top"></td>
        <td width="22" align="center" valign="top" bgcolor="#d9d6d6">&nbsp;</td>
        <td width="579" align="left" valign="top" bgcolor="#d9d6d6"><table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td><table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
          <tr>
            <td width="16%" align="left" valign="top" class="line6 align" scope="col">Upload Image</td>
            <td width="84%" align="left" valign="middle" bgcolor="#d9d6d6">
			<div id='TextBoxesGroup'>
				<div id="TextBoxDiv1">
					<input name="image_path1" id="image_path1" type="file" value=""/>
				</div>
			</div>
			</td>
          </tr>
        </table></td>
          </tr>
		            <tr>
            <td>Image height should be 512 exactly and any width up 900, 72 dpi</td>
          </tr>

        </table></td>
        <td width="517" height="30" align="left" valign="middle" bgcolor="#d9d6d6">&nbsp;</td>
      </tr>
      
      <tr>
        <td align="center" valign="top" bgcolor="#d9d6d6">&nbsp;</td>
        <td align="left" valign="top" bgcolor="#d9d6d6">&nbsp;</td>
        <td align="center" valign="top"></td>
        <td align="center" valign="top" bgcolor="#d9d6d6">&nbsp;</td>
        <td align="left" valign="top" bgcolor="#d9d6d6">&nbsp;</td>
        <td height="15" align="left" valign="top" bgcolor="#d9d6d6">&nbsp;</td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td align="center" valign="middle" bgcolor="#d9d6d6">&nbsp;</td>
    <td align="left" valign="middle" bgcolor="#d9d6d6">&nbsp;</td>
    <td align="center" valign="middle" scope="col"></td>
    <td align="center" valign="middle" bgcolor="#d9d6d6">&nbsp;</td>
    <td align="left" valign="middle" bgcolor="#d9d6d6">&nbsp;</td>
  </tr>
  <tr>
    <td align="center" valign="middle" bgcolor="#d9d6d6">&nbsp;</td>
    <td align="left" valign="middle" bgcolor="#d9d6d6">&nbsp;</td>
    <td align="center" valign="middle" scope="col"></td>
    <td align="center" valign="middle" bgcolor="#d9d6d6">&nbsp;</td>
    <td align="left" valign="middle" bgcolor="#d9d6d6"><input name="addtextbox" id="addtextbox" type="button" value="Add New Image" class="input2" /></td>
  </tr>
  <tr>
    <td align="center" valign="middle" bgcolor="#d9d6d6">&nbsp;</td>
    <td align="left" valign="middle" bgcolor="#d9d6d6">&nbsp;</td>
    <td align="center" valign="middle" scope="col"></td>
    <td align="center" valign="middle" bgcolor="#d9d6d6">&nbsp;</td>
    <td align="left" valign="middle" bgcolor="#d9d6d6">&nbsp;</td>
  </tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
    <td width="16%" scope="col">&nbsp;</td>
    <td width="84%" scope="col">&nbsp;</td>
    </tr>
  <tr>
    <td width="16%" scope="col">&nbsp;</td>
    <td scope="col"><input name="btnSave" id="btnSave" type="submit" value="SAVE" class="input2" /></td>
    </tr>
  <tr>
    <td scope="col">&nbsp;</td>
    <td scope="col">&nbsp;</td>
    </tr>
</table>

</form>
<?php }else if($index == "Edit"){

	$Id = $_GET['Id'];
	
	//Press details are fetched
	$sql_press = "SELECT * FROM ".TP." WHERE `press_id`='".$Id."'";
	$res_press = $db->get($sql_press);
	$row_press = $db->fetch_array($res_press);
	
	$press_image = "../uploads/press/".$row_press['press_image'];


	if(isset($_POST['btnSave']) && empty($_POST['btnSave'])==false){
	
		if($_FILES['press_image']['name']!=""){
			require_once('../includes/file.upload.inc.php');

			$objFileUpload = new FileUpload();
			
			$objFileUpload->UploadMode = 'Edit';

			$objFileUpload->OldFileName = $row_press['press_image'];
									
			$objFileUpload->UploadContent = $_FILES['press_image'];

			$objFileUpload->UploadFolder = "../uploads/press";

			$image_return = $objFileUpload->Upload();
			
			$press_image = $image_return['server_name'];
		}else
			$press_image = $row_press['press_image'];
		
		$update_sql_array = array("headline" => $f->post('headline'),
								  "press_date" => $f->post('press_date'),
								  "press_details" => $f->post('press_details'),
								  "press_image" => $press_image);
		$db->update(TP,$update_sql_array,"press_id",$Id);

		foreach($_POST['sequence'] as $key => $val){
			$addl_id = $key;
			
			$sql = "UPDATE `".TPAI."` SET `sequence`='".$val."' WHERE `addl_id`='".$key."'";
			$db->get($sql);
			
			//Array is populated
			$arr_sequence[]=$val;
		}

		if(count($arr_sequence)>0){
			//Array is sorted
			asort($arr_sequence);
			$max_val = end($arr_sequence);
		}
		
		$j=$max_val+1;
		for($i=1;$i<=10;$i++){
			if($_FILES['image_path'.$i]['name']!=""){
				
				require_once('../includes/file.upload.inc.php');
				
				$sequence_id = $m->getMaxSequence(TPAI,"",$Id);
	
				$objFileUpload = new FileUpload();
										
				$objFileUpload->UploadContent = $_FILES['image_path'.$i];
	
				$objFileUpload->UploadFolder = "../uploads/press";
	
				$image_return = $objFileUpload->Upload();
				
				$image_path = $image_return['server_name'];
				
				$insert_sql_array = array("press_id" => $Id,
								  	"image_path" => $image_path,
									"sequence" => $sequence_id);
				$db->insert(TPAI,$insert_sql_array);
				$j++;
			}
		}

		$f->Redirect(CP."?index=Edit&Id=".$Id."&msg=".urlencode("Press details successfully updated!"));


	}

?>
<form name="frmPress" id="frmPress" action="<?php echo CP.'?'.QS;?>" method="post" enctype="multipart/form-data">
<?php if($msg!=""){
?>
<table width="100%"  border="0" cellpadding="0" cellspacing="0">
  <tr>
    <td colspan="3" scope="col" height="30" align="center"><?php echo urldecode($msg);?></td>
    </tr>

</table>
<?php
}?>  
<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td width="30" height="10" align="left" valign="middle" bgcolor="#d9d6d6"></td>
    <td width="128" align="left" valign="middle" bgcolor="#d9d6d6"></td>
    <td width="1" align="left" valign="middle" scope="col"></td>
    <td width="20" align="left" valign="middle" bgcolor="#d9d6d6"></td>
    <td width="952" align="left" valign="middle" bgcolor="#d9d6d6"></td>	
  <tr>
    <td width="30" height="25" align="left" valign="middle" bgcolor="#d9d6d6">&nbsp;</td>
    <td width="128" align="left" valign="middle" bgcolor="#d9d6d6">Headline</td>
    <td width="1" align="left" valign="middle" scope="col"></td>
    <td width="20" align="left" valign="middle" bgcolor="#d9d6d6">&nbsp;</td>
    <td width="952" align="left" valign="middle" bgcolor="#d9d6d6"><input name="headline" id="headline" type="text" value="<?php echo $f->getValue($row_press['headline']);?>" class="input6 required" />    </td>
  </tr>
  <tr>
    <td width="30" align="center" valign="middle" bgcolor="#d9d6d6">&nbsp;</td>
    <td width="128" align="left" valign="middle" bgcolor="#d9d6d6">Date</td>
    <td width="1" align="center" valign="middle" ></td>
    <td width="20" align="center" valign="middle" bgcolor="#d9d6d6">&nbsp;</td>
    <td width="952" align="left" valign="middle" bgcolor="#d9d6d6"><input name="press_date" id="press_date" type="text" value="<?php echo $f->getValue($row_press['press_date']);?>" class="input6 required" />    </td>
  </tr>
  <tr>
    <td height="10" align="center" valign="middle" bgcolor="#d9d6d6"></td>
    <td align="left" valign="middle" bgcolor="#d9d6d6"></td>
    <td align="center" valign="middle" scope="col"></td>
    <td align="center" valign="middle" bgcolor="#d9d6d6"></td>
    <td align="left" valign="middle" bgcolor="#d9d6d6"></td>
  </tr>
  <tr>
    <td align="center" valign="middle" class="line6" scope="col">&nbsp;</td>
    <td align="left" valign="middle" bgcolor="#d9d6d6">&nbsp;</td>
    <td align="center" valign="middle" scope="col"></td>
    <td align="center" valign="middle" bgcolor="#d9d6d6">&nbsp;</td>
    <td align="left" valign="middle" bgcolor="#d9d6d6"><span class="line6 align">
	<textarea name="press_details" cols="80" rows="10" id="press_details"><?php echo $f->getValue($row_press['press_details']);?></textarea>
	<script type="text/javascript">
		CKEDITOR_BASEPATH = '<?php echo WEBSITE_URL;?>/ckeditor/';
		CKEDITOR.replace('press_details', {toolbar : 'Full',width : '767px',height : '388px',
		filebrowserBrowseUrl : '<?php echo WEBSITE_URL;?>/ckeditor/filemanager/browser/default/browser.html?Connector=<?php echo WEBSITE_URL;?>/ckeditor/filemanager/connectors/php/connector.php',
		filebrowserImageBrowseUrl : '<?php echo WEBSITE_URL;?>/ckeditor/filemanager/browser/default/browser.html?Type=Image&Connector=<?php echo WEBSITE_URL;?>/ckeditor/filemanager/connectors/php/connector.php',
		filebrowserFlashBrowseUrl : '<?php echo WEBSITE_URL;?>/ckeditor/filemanager/browser/default/browser.html?Type=Flash&Connector=<?php echo WEBSITE_URL;?>/ckeditor/filemanager/connectors/php/connector.php',
		filebrowserUploadUrl : '<?php echo WEBSITE_URL;?>/ckeditor/filemanager/connectors/php/upload.php?Type=File',
		filebrowserImageUploadUrl : '<?php echo WEBSITE_URL;?>/ckeditor/filemanager/connectors/php/upload.php?Type=Image',
		filebrowserFlashUploadUrl : '<?php echo WEBSITE_URL;?>/ckeditor/filemanager/connectors/php/upload.php?Type=Flash'});
	</script>	

	</span></td>
  </tr>
  <tr>
    <td align="center" valign="middle" bgcolor="#d9d6d6">&nbsp;</td>
    <td align="left" valign="middle" bgcolor="#d9d6d6">&nbsp;</td>
    <td align="center" valign="middle" scope="col"></td>
    <td align="center" valign="middle" bgcolor="#d9d6d6">&nbsp;</td>
    <td align="left" valign="middle" bgcolor="#d9d6d6">&nbsp;</td>
  </tr>
  <tr>
    <td colspan="5" align="center" valign="middle"><table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
      <tr>
        <td width="30" rowspan="2" align="center" valign="top" bgcolor="#d9d6d6">&nbsp;</td>
        <td width="128" rowspan="2" align="left" valign="top" bgcolor="#d9d6d6">Main Image</td>
        <td width="1" rowspan="2" align="center" valign="top"></td>
        <td width="20" rowspan="2" align="center" valign="top" bgcolor="#d9d6d6">&nbsp;</td>
        <td width="135" rowspan="2" align="left" valign="top" bgcolor="#d9d6d6"><img src="resize.php?imgfile=<?php echo $press_image;?>&max_height=146&max_width=111" alt="" /></td>
        <td width="817" height="30" align="left" valign="middle" bgcolor="#d9d6d6">Image should be sized to 220 x 277 exactly, 72 dpi</td>
      </tr>
      <tr>
        <td height="15" align="left" valign="top" bgcolor="#d9d6d6"><table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
          <tr>
            <td width="12%" align="left" valign="middle" class="line6 align" scope="col">Upload Image</td>
            <td width="88%" align="left" valign="middle" bgcolor="#d9d6d6"><input type="file" name="press_image" id="press_image" value="" /></td>
          </tr>
        </table></td>
      </tr>
      <tr>
        <td align="center" valign="top" bgcolor="#d9d6d6">&nbsp;</td>
        <td align="left" valign="top" bgcolor="#d9d6d6">&nbsp;</td>
        <td align="center" valign="top"></td>
        <td align="center" valign="top" bgcolor="#d9d6d6">&nbsp;</td>
        <td align="left" valign="top" bgcolor="#d9d6d6">&nbsp;</td>
        <td height="15" align="left" valign="top" bgcolor="#d9d6d6">&nbsp;</td>
      </tr>
    </table></td>
    </tr>
  <?php if($db->num_rows($res_photos)>0){?>
  <tr>
    <td colspan="5" align="left" valign="top"><table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
      <tr>
        <td width="30" align="center" valign="top" bgcolor="#d9d6d6">&nbsp;</td>
        <td width="128" align="left" valign="top" bgcolor="#d9d6d6">Additional Images</td>
        <td width="1" align="center" valign="top"></td>
        <td width="20" align="center" valign="top" bgcolor="#d9d6d6">&nbsp;</td>
        <td height="15" align="left" valign="top" bgcolor="#d9d6d6">
		<div id='TextBoxesGroup'>
		<table width="100%" border="0" cellspacing="0" cellpadding="0">
		<?php
				$i=1;
				while($row_photos = $db->fetch_array($res_photos)){
				
					$photo_path = "../uploads/press/".$row_photos['image_path'];
		?>
          <tr>
            <td width="16%"><img src="resize.php?imgfile=<?php echo $photo_path;?>&max_height=146&max_width=111" alt="" /></td>
            <td width="84%" align="left" valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0">
			<?php if($i==1){?>
              <tr>
                <td colspan="2" class="line6 align" scope="col">Image height should be 512 exactly and any width up 900, 72 dpi</td>
                <td class="line6 align" scope="col">&nbsp;</td>
                <td class="line6 align" scope="col">&nbsp;</td>
              </tr>
			  <?php }?>
              <tr>
                <td width="2%" class="line6 align" scope="col">&nbsp;</td>
                <td width="53%" class="line6 align" scope="col">
					<div id="TextBoxDiv<?php echo $i;?>">
						  <input name="sequence[<?php echo $row_photos['addl_id'];?>]" type="text" value="<?php echo $row_photos['sequence'];?>" class="input7" size="5" />
						  &nbsp;<a href="<?php echo CP;?>?index=Edit&action=delete&Id=<?php echo $Id;?>&photoid=<?php echo $row_photos['addl_id'];?>" class="delete"><img src="images/delite.png" width="14" height="13" alt="" /></a>					
					</div>
				</td>
                <td width="4%" class="line6 align" scope="col">&nbsp;</td>
                <td width="41%" class="line6 align" scope="col">&nbsp;</td>
              </tr>
            </table></td>
          </tr>
          <tr>
            <td>&nbsp;</td>
            <td align="left" valign="top">&nbsp;</td>
          </tr>
		<?php 
				$i++;
				}
		?>

        </table>
		</div>
		</td>
        </tr>
    </table></td>
  </tr>
  <?php }else{?>

  <tr>
    <td colspan="5" align="center" valign="middle"><table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
      <tr>
        <td width="34" align="center" valign="top" bgcolor="#d9d6d6">&nbsp;</td>
        <td width="147" align="left" valign="top" bgcolor="#d9d6d6">
          <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td>Additional</td>
            </tr>
            <tr>
              <td>Images</td>
            </tr>
          </table></td>
        <td width="1" align="center" valign="top"></td>
        <td width="22" align="center" valign="top" bgcolor="#d9d6d6">&nbsp;</td>
        <td width="579" align="left" valign="top" bgcolor="#d9d6d6"><table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td><table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
          <tr>
            <td width="16%" align="left" valign="top" class="line6 align" scope="col">Upload Image</td>
            <td width="84%" align="left" valign="middle" bgcolor="#d9d6d6">
			<div id='TextBoxesGroup'>
				<div id="TextBoxDiv1">
					<input name="image_path1" id="image_path1" type="file" value=""/>
				</div>
			</div>
			</td>
          </tr>
        </table></td>
          </tr>
		            <tr>
            <td>Image height should be 512 exactly and any width up 900, 72 dpi</td>
          </tr>

        </table></td>
        <td width="517" height="30" align="left" valign="middle" bgcolor="#d9d6d6">&nbsp;</td>
      </tr>
      
      <tr>
        <td align="center" valign="top" bgcolor="#d9d6d6">&nbsp;</td>
        <td align="left" valign="top" bgcolor="#d9d6d6">&nbsp;</td>
        <td align="center" valign="top"></td>
        <td align="center" valign="top" bgcolor="#d9d6d6">&nbsp;</td>
        <td align="left" valign="top" bgcolor="#d9d6d6">&nbsp;</td>
        <td height="15" align="left" valign="top" bgcolor="#d9d6d6">&nbsp;</td>
      </tr>
    </table></td>
  </tr>
  <?php }?>
<tr>
    <td align="center" valign="middle" bgcolor="#d9d6d6">&nbsp;</td>
    <td align="left" valign="middle" bgcolor="#d9d6d6">&nbsp;</td>
    <td align="center" valign="middle" scope="col"></td>
    <td align="center" valign="middle" bgcolor="#d9d6d6">&nbsp;</td>
    <td align="left" valign="middle" bgcolor="#d9d6d6">&nbsp;</td>
  </tr>
  <tr>
    <td align="center" valign="middle" bgcolor="#d9d6d6">&nbsp;</td>
    <td align="left" valign="middle" bgcolor="#d9d6d6">&nbsp;</td>
    <td align="center" valign="middle" scope="col"></td>
    <td align="center" valign="middle" bgcolor="#d9d6d6">&nbsp;</td>
    <td align="left" valign="middle" bgcolor="#d9d6d6"><input name="addtextbox" id="addtextbox" type="button" value="Add New Image" class="input2" /></td>
  </tr>
  <tr>
    <td align="center" valign="middle" bgcolor="#d9d6d6">&nbsp;</td>
    <td align="left" valign="middle" bgcolor="#d9d6d6">&nbsp;</td>
    <td align="center" valign="middle" scope="col"></td>
    <td align="center" valign="middle" bgcolor="#d9d6d6">&nbsp;</td>
    <td align="left" valign="middle" bgcolor="#d9d6d6">&nbsp;</td>
  </tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
    <td width="16%" scope="col">&nbsp;</td>
    <td width="84%" scope="col">&nbsp;</td>
    </tr>
  <tr>
    <td width="16%" scope="col">&nbsp;</td>
    <td scope="col"><input name="btnSave" id="btnSave" type="submit" value="SAVE" class="input2" /></td>
    </tr>
  <tr>
    <td scope="col">&nbsp;</td>
    <td scope="col">&nbsp;</td>
    </tr>
</table>

</form>
<?php }?>

<div class="clear"></div>
</div>  
  
  
  
<div class="clear"></div>
</div>
</body>
</html>
