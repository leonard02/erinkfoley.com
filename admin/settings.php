<?php
	require_once("../includes/config.inc.php");
	$f->redirectBase = WEBSITE_URL;
	$f->isLogin('_admin','index.php');
	$msg = "&nbsp;";
	if(isset($_POST['btnUpdate'])):
		$settings = $_POST['settings'];
		
		$data = array();
		foreach($settings as $field => $value)
		{
			$data[$field] = $f->setValue($value);
		}
		
		$db->update('tbl_admin',$data,"admin_id",1);
		$msg = $f->getHtmlMessage("Settings have been successfully updated");
	endif;
	
	$sql = "SELECT * FROM `tbl_admin` WHERE `admin_id`=1";
	$res = $db->get($sql,__FILE__,__LINE__);
	$row = $db->fetch_array($res);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<?php include("js.css.inc.php");?>
<script type="text/javascript">
$(document).ready(function() {
	$('#frmSettings').validate();
	$('#smtp').change(function() {
		if($(this).val() == "Yes")
		{
			$('.smtp_tr').css('display', '');
			$("#smtp_hostname").rules("add", {
				required: true,
			});
			$("#smtp_username").rules("add", {
				required: true,
				email: true
			});
			$("#smtp_password").rules("add", {
				required: true,
				minlength: 6,
				messages: {
					minlength: "Minimum input 6 characters"
				}
			});
		}
		else
		{
			$('.smtp_control').rules('remove');
			$('.smtp_control').val('');
			$('.smtp_tr').css('display', 'none');
			//$('.smtp_tr').animate({"display": "none"}, "slow");
		}	
	});
});
</script>
</head>
<body>
<!--main-->
<div id="main">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td align="center" valign="top"><table width="1131" border="0" align="center" cellpadding="0" cellspacing="0">
      <tr>
        <td align="center" valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0">
		 <tr>
		  	<td colspan="2">
				<?php include("header.inc.php");?>				
			</td>
		  </tr>            
          <tr>
            <td height="22" colspan="2" align="left" valign="top" bgcolor="#444444">&nbsp;</td>
          </tr>
          <tr>
            <td height="22" colspan="2" align="left" valign="top" bgcolor="#bcbcbc">&nbsp;</td>
          </tr>
        </table></td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td align="left" valign="top" class="contaner">&nbsp;</td>
  </tr>
  </table>
	<div class="contaner">
	<form action="<?php echo CP;?>" method="post" name="frmSettings" id="frmSettings">
	<table width="100%" border="0" cellspacing="0" cellpadding="0">
		<?php if($msg!='&nbsp;')
		{
		?>
		   <tr>
			<td height="5" align="center"><?php echo $msg;?></td>
		  </tr>
		  <?php
		  }
		  ?>
		  <tr>
			<td colspan="2" align="left" valign="top" class="headerline1">PERSONAL SETTINGS</td>
		  </tr>
		 
	  </table>
	
	<table width="100%" border="0" cellspacing="0" cellpadding="0">
	  <tr >
		<td width="14%" align="center" scope="col" bgcolor="#d9d6d6">Your Name:</td>
		<td bgcolor="#d9d6d6" width="40%"><input  name="settings[your_name]" type="text" class="required input16" id="your_name" size="65" value="<?php echo $f->getValue($row['your_name']);?>" /></td>
		<td  bgcolor="#d9d6d6" width="60%" scope="col">*required for contact and forgotten password email</td>
	  </tr>
	 </table>
	 
	 
	 
	 
	 <table width="100%" border="0" cellspacing="0" cellpadding="0">
	  <tr >
		<td width="14%" align="center" scope="col" bgcolor="#e6e6e6">Your Email Address:</td>
		<td bgcolor="#e6e6e6" width="40%"><input name="settings[your_email_address]" type="text" class="required email input16" id="your_email_address" size="65" value="<?php echo $f->getValue($row['your_email_address']);?>" /></td>
		<td  bgcolor="#e6e6e6" width="60%" scope="col">*required for contact and forgotten password email</td>
	  </tr>
	 </table>
	
	<table width="100%" border="0" cellspacing="0" cellpadding="0">
	
			<td>&nbsp;</td>
			</tr>
	  </table>
	
	<table width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
			<td colspan="2" align="left" valign="top" class="headerline1">SMTP SETTINGS</td>
	    </tr>
		  
	  </table>
	   
	<table width="100%" border="0" cellspacing="0" cellpadding="0" height="30">
	  <tr >
		<td width="14%" align="center" scope="col" bgcolor="#d9d6d6">From Email Name</td>
		<td bgcolor="#d9d6d6" width="40%"><input name="settings[email_from_name]" type="text" class="required input16" id="email_from_name" size="65" value="<?php echo $f->getValue($row['email_from_name']);?>" /></td>
		<td  bgcolor="#d9d6d6" width="60%" scope="col">&nbsp;</td>
	  </tr>
	 </table>
	 
	 <table width="100%" border="0" cellspacing="0" cellpadding="0" height="30">
	  <tr >
		<td width="14%" align="center" scope="col" bgcolor="#e6e6e6">From Email Address</td>
		<td bgcolor="#e6e6e6" width="40%"><input name="settings[email_from_address]" type="text" class="required email input16" id="email_from_address" value="<?php echo $f->getValue($row['email_from_address']);?>" size="65" /></td>
		<td  bgcolor="#e6e6e6" width="60%" scope="col">&nbsp;</td>
	  </tr>
	 </table>
	 
	 
	 <table width="100%" border="0" cellspacing="0" cellpadding="0" height="30">
	  <tr >
		<td width="14%" align="center" scope="col" bgcolor="#d9d6d6">SMTP</td>
		<td bgcolor="#d9d6d6" width="40%"><select name="settings[smtp]" id="smtp">
                    <option value="Yes"<?php if($row['smtp']=='Yes') echo ' selected';?>>Yes</option>
                    <option value="No"<?php if($row['smtp']=='No') echo ' selected';?>>No</option>
                    </select>	</td>
		<td  bgcolor="#d9d6d6" width="60%" scope="col">&nbsp;</td>
	  </tr>
	 </table>
	 
	 <table width="100%" border="0" cellspacing="0" cellpadding="0" height="30">
	  <tr class="smtp_tr" <?php if($row['smtp']=='No') echo ' style="display: none;"';?>>
		<td width="14%" align="center" scope="col" bgcolor="#e6e6e6">SMTP Hostname</td>
		<td bgcolor="#e6e6e6" width="40%"><input name="settings[smtp_hostname]" type="text" class="smtp_control input16" id="smtp_hostname" value="<?php echo $f->getValue($row['smtp_hostname']);?>" /></td>
		<td  bgcolor="#e6e6e6" width="60%" scope="col">&nbsp;</td>
	  </tr>
	 </table>
	 
	 
	 <table width="100%" border="0" cellspacing="0" cellpadding="0" height="30">
	  <tr class="smtp_tr"<?php if($row['smtp']=='No') echo ' style="display: none;"';?>>
		<td width="14%" align="center" scope="col" bgcolor="#d9d6d6">SMTP Username</td>
		<td bgcolor="#d9d6d6" width="40%"><input name="settings[smtp_username]" type="text" class="smtp_control input16" id="smtp_username" value="<?php echo $f->getValue($row['smtp_username']);?>" /></td>
		<td  bgcolor="#d9d6d6" width="60%" scope="col">&nbsp;</td>
	  </tr>
	 </table>
	 
	 <table width="100%" border="0" cellspacing="0" cellpadding="0" height="30">
	  <tr class="smtp_tr"<?php if($row['smtp']=='No') echo ' style="display: none;"';?>>
		<td width="14%" align="center" scope="col" bgcolor="#e6e6e6">SMTP Password</td>
		<td bgcolor="#e6e6e6" width="40%"><input name="settings[smtp_password]" type="text" class="smtp_control input16" id="smtp_password" value="<?php echo $f->getValue($row['smtp_password']);?>" /></td>
		<td  bgcolor="#e6e6e6" width="60%" scope="col">&nbsp;</td>
	  </tr>
	 </table>
	   
	   
	  
	  
	  <table width="100%" border="0" cellspacing="0" cellpadding="0">
	
			<td>&nbsp;</td>
			</tr>
	  </table>
	  
	  
		
		
	  <table width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
			<td colspan="2" align="left" valign="top" class="headerline1">MISC. SETTINGS</td>
	    </tr>
		  
	  </table>  
	
	
	<table width="100%" border="0" cellspacing="0" cellpadding="0" height="30">
	  <tr >
		<td width="14%" align="center" scope="col" bgcolor="#d9d6d6">Company Name</td>
		<td bgcolor="#d9d6d6" width="40%"><input name="settings[company_name]" type="text" class="required input16" id="company_name" value="<?php echo $f->getValue($row['company_name']);?>" /></td>
		<td  bgcolor="#d9d6d6" width="60%" scope="col">&nbsp;</td>
	  </tr>
	 </table>
	 
	 
	 <table width="100%" border="0" cellspacing="0" cellpadding="0">
	
			<td>&nbsp;</td>
			</tr>
	  </table>
		
	<table width="100%" cellspacing="0" cellpadding="0">
        <tr>
          <td height="21" class="headerline1">Social Networking Settings</td>
        </tr>
        <tr>
          <td height="23" bgcolor="#d9d6d6"><table width="100%" align="left" cellpadding="0" cellspacing="0">
              <tr>
                 <td height="23" width="14%" align="center">Facebook Link:</td>
			    <td height="23" width="40%" align="left"><input name="settings[facebook_link]" type="text" class="required inputtextbox5" id="facebook_link" value="<?php echo $f->getValue($row['facebook_link']);?>" size="40" /></td>
                <td width="60%" align="left">&nbsp;</td>
              </tr>
            </table></td>
        </tr>
		
		<tr>
          <td height="23" bgcolor="#e6e6e6"><table width="100%" align="center" cellpadding="0" cellspacing="0">
              <tr>
                 <td height="23" width="14%" align="center">Twitter Link:</td>
			    <td height="23" width="40%" align="left"><input name="settings[twitter_link]" type="text" class="required inputtextbox5" id="twitter_link" value="<?php echo $f->getValue($row['twitter_link']);?>" size="40" /></td>
                <td width="60%" align="left">&nbsp;</td>
              </tr>
            </table></td>
        </tr>
		
		<tr>
          <td height="23" bgcolor="#d9d6d6"><table width="100%" align="center" cellpadding="0" cellspacing="0">
              <tr>
                 <td height="23" width="14%" align="center">Tumblr Link:</td>
			    <td height="23" width="40%" align="left"><input name="settings[tumblr_link]" type="text" class="required inputtextbox5" id="tumblr_link" value="<?php echo $f->getValue($row['tumblr_link']);?>" size="40" /></td>
                <td width="60%" align="left">&nbsp;</td>
              </tr>
            </table></td>
        </tr>
	   <tr>
          <td height="23" bgcolor="#e6e6e6"><table width="100%" align="center" cellpadding="0" cellspacing="0">
              <tr>
                 <td height="23" width="14%" align="center" class="productlink">YouTube Link:</td>
			    <td height="23" width="40%" align="left" class="productlink"><input name="settings[youtube_link]" type="text" class="required inputtextbox5" id="youtube_link" value="<?php echo $f->getValue($row['youtube_link']);?>" size="40" /></td>
                <td width="60%" align="left" class="productlink">&nbsp;</td>
              </tr>
            </table></td>
        </tr>
      </table>
	  	 <table width="100%" border="0" cellspacing="0" cellpadding="0">
	
			<td>&nbsp;</td>
			</tr>
		</table>
		
		
		<table width="100%" border="0" cellspacing="0" cellpadding="0">
	  <tr>
		<th width="16%" align="center" valign="middle"><input name="btnUpdate" type="submit" id="btnUpdate" value="Update Settings" class="input2" /></th>
		<th width="84%" scope="col">&nbsp;</th>
	  </tr>
	  <tr>
	    <th align="center" valign="middle">&nbsp;</th>
	    <th scope="col">&nbsp;</th>
	    </tr>
	</table>
	

	</form>
	<div class="clear"></div>
	</div>  
  
  
  
<div class="clear"></div>
</div>
<!--main-->
</body>
</html>