<?php
	require_once("../includes/config.inc.php");
	$f->redirectBase = WEBSITE_URL;
	$f->isLogin('_admin','index.php');
	
	$page_id = 1;
	
	define("TSF","tbl_show_flyers",true);
	$msg = $_GET['msg'];

	//Flyers are fetched
	$sql_flyers = "SELECT * FROM `".TSF."` ORDER BY `flyer_id` DESC";
	$res_flyers = $db->get($sql_flyers);	
	
	
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<?php include("js.css.inc.php");?>
</head>
<body>
<!--main-->
<div id="main">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td align="center" valign="top"><table width="1131" border="0" align="center" cellpadding="0" cellspacing="0">
      <tr>
        <td align="center" valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0">
		 <tr>
		  	<td colspan="2">
				<?php include("header.inc.php");?>				
			</td>
		  </tr>            
          <tr>
            <td height="22" colspan="2" align="left" valign="top" bgcolor="#444444"><table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <th width="30" scope="row">&nbsp;</th>
                <td width="1101" class="style3">SHOW FLYERS</td>
              </tr>
            </table></td>
          </tr>
          <tr>
            <td height="22" colspan="2" align="left" valign="top" bgcolor="#bcbcbc"><table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <th width="30" scope="row">&nbsp;</th>
                <td class="style4"><!--<a href="showsflyersdetail.php?index=Add">ADD NEW SHOW FLYER</a>--><a href="showsschedule.php">BACK TO SHOWS LIST</a></td>
                </tr>
            </table></td>
          </tr>
        </table></td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td align="left" valign="top" class="contaner">&nbsp;</td>
  </tr>
    <?php if($msg!=""){
  ?>
  <tr>
    <td align="center" valign="top" class="contaner" height="30"><?php echo urldecode($msg);?></td>
  </tr>
  <?php
  }?>

  </table>
 
<div class="contaner">
 <?php if($db->num_rows($res_flyers)>0){
 	$i=1;
 	while($row_flyers = $db->fetch_array($res_flyers)){
		$img_path = "../uploads/flyers/".$row_flyers['flyer_image'];
		if($i==1)
			$bgcolor = "#d9d6d6";
		else
			$bgcolor = "#e6e6e6";
 ?> 
 <table width="100%" border="0" cellspacing="0" cellpadding="0" >
  <tr >
    <th class="padding" width="12%" align="center" valign="top" bgcolor="<?php echo $bgcolor;?>" scope="col">FLYER <?php echo $i;?></th>
    <th width="0%" scope="col">&nbsp;</th>
    <th class="padding" width="22%" align="right" bgcolor="<?php echo $bgcolor;?>" scope="col"><img src="resize.php?imgfile=<?php echo $img_path;?>&max_width=207&max_height=234" alt="" /></th>
    <th width="37%" align="center" valign="middle" bgcolor="<?php echo $bgcolor;?>" scope="col"><h2><?php echo $f->getValue($row_flyers['caption1']);?><br/>
<?php echo $f->getValue($row_flyers['caption2']);?></h2></th>
    <th width="29%" align="center" valign="middle" bgcolor="<?php echo $bgcolor;?>" scope="col"><a href="showsflyersdetail.php?index=Edit&Id=<?php echo $row_flyers['flyer_id'];?>"><img src="images/pencil.png" width="14" height="14" alt="" /></a></th>
  </tr>
</table>
 <?php 
 		$i++;
 	}
 }?>


<div class="clear"></div>
</div>   
<div class="clear"></div>
</div>
<!--main-->
</body>
</html>
