<?php
	require_once("../includes/config.inc.php");
	$f->redirectBase = WEBSITE_URL;
	$f->isLogin('_admin','index.php');
	
	$page_id = 1;
	
	define("TSF","tbl_show_flyers",true);
	$msg = $_GET['msg'];
	$index = $_GET['index'];
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<?php include("js.css.inc.php");?>
<script type="text/javascript">
$(document).ready(function(){
	$('#frmShowFlyer').validate();
	$("#image_path").rules("add", {
		<?php if($index=="Add"){
		?>
		required: true,
		<?php
		}?>
		accept: 'jpg|jpeg|png|gif|JPG|JPEG|PNG|GIF',
		messages: {
			accept: "Accept only jpg, png, gif files"
		}
		});
	$('#caption1').focus(function(){
		if($(this).val()=="Line 1")
			$(this).val("");
	});
	$('#caption2').focus(function(){
		if($(this).val()=="Line 2")
			$(this).val("");
	});
});
</script>

</head>
<body>
<!--main-->
<div id="main">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td align="center" valign="top"><table width="1131" border="0" align="center" cellpadding="0" cellspacing="0">
      <tr>
        <td align="center" valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0">
		 <tr>
		  	<td colspan="2">
				<?php include("header.inc.php");?>				
			</td>
		  </tr>            
          <tr>
            <td height="22" colspan="2" align="left" valign="top" bgcolor="#444444"><table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <th width="30" scope="row">&nbsp;</th>
                <td width="1101" class="style3">SHOW FLYERS</td>
              </tr>
            </table></td>
          </tr>
          <tr>
            <td height="22" colspan="2" align="left" valign="top" bgcolor="#bcbcbc"><table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <th width="30" scope="row">&nbsp;</th>
                <td width="1101" class="style4"><a href="showsflyers.php">BACK TO LIST</a></td>
              </tr>
            </table></td>
          </tr>
        </table></td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td align="left" valign="top" class="contaner">&nbsp;</td>
  </tr>
  </table>
 
<div class="contaner">
  <form name="frmShowFlyer" id="frmShowFlyer" action="<?php echo CP.'?'.QS;?>" method="post" enctype="multipart/form-data">
  <?php if($index == "Add"){
  
  		if(isset($_POST['btnSave']) && empty($_POST['btnSave'])==false){
			
			if($_FILES['image_path']['name']!=""){
				require_once('../includes/file.upload.inc.php');

				$objFileUpload = new FileUpload();
										
				$objFileUpload->UploadContent = $_FILES['image_path'];

				$objFileUpload->UploadFolder = "../uploads/flyers";

				$image_return = $objFileUpload->Upload();
				
				$image_path = $image_return['server_name'];
			}
			
			
			//Record is inserted into the table
			$insert_sql_array = array("flyer_image" => $image_path,
										"caption1" => $f->post('caption1'),
										"caption2" => $f->post('caption2'));
			$db->insert(TSF,$insert_sql_array);
			
			$f->Redirect("showsflyers.php?msg=".urlencode("Record successfully added!"));

			
		}

  ?>
 <table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <th class="padding" width="12%" align="center" valign="top" bgcolor="#d9d6d6" scope="col">FLYER 1</th>
    <th width="0%" scope="col">&nbsp;</th>
    <th class="padding" width="22%" align="right" bgcolor="#d9d6d6" scope="col"><!--<img src="images/img10.jpg" width="207" height="234" alt="" /><br />-->
	<input name="image_path" id="image_path" type="file" class="required" />
	</th>
    <th width="2%" scope="col" bgcolor="#d9d6d6">&nbsp;</th>
    <th class="padding" width="64%" align="left" valign="top" bgcolor="#d9d6d6" scope="col">Size: exactly 415 x 467, 72 dpi</th>
    
  </tr>
</table>


<table width="100%" border="0" cellspacing="0" cellpadding="0" height="30">
  <tr>
    <th width="12%" align="center" valign="middle" bgcolor="#e6e6e6" scope="col">CAPTION</th>
    <th width="0%" scope="col" >&nbsp;</th>
    <th width="2%" align="center" valign="middle" bgcolor="#e6e6e6" scope="col">&nbsp;</th>
    <th width="34%" align="right" valign="middle" bgcolor="#e6e6e6" scope="col"><input name="caption1" id="caption1" type="text" value="Line 1" class="input16 required" /></th>
    <th width="52%" align="center" valign="middle" bgcolor="#e6e6e6" scope="col">&nbsp;</th>
    
  </tr>
</table>

<table width="100%" border="0" cellspacing="0" cellpadding="0" height="30px">
  <tr>
    <th width="12%" align="center" valign="middle" bgcolor="#e6e6e6" scope="col">&nbsp;</th>
    <th width="0%" scope="col" >&nbsp;</th>
    <th width="2%" align="center" valign="middle" bgcolor="#e6e6e6" scope="col">&nbsp;</th>
    <th width="34%" align="right" valign="middle" bgcolor="#e6e6e6" scope="col"><input name="caption2" id="caption2" type="text" value="Line 2" class="input16 required" /></th>
    <th width="52%" align="center" valign="middle" bgcolor="#e6e6e6" scope="col">&nbsp;</th>
    
  </tr>
</table>



<table class="savesort" width="100%"  border="0" cellpadding="0" cellspacing="0">
  <tr>
    <td  width="12%" scope="col">&nbsp;</td>
    <td  width="4%" scope="col">&nbsp;</td>
    <td  width="84%" scope="col"><input name="btnSave" id="btnSave" type="submit" value=" SAVE " class="input17" /></td>
  </tr>
</table>
<?php }else if($index == "Edit"){

		$Id=$_GET['Id'];
		
		//Flyer details are fetched
		$sql_flyers = "SELECT * FROM `".TSF."` WHERE `flyer_id`='".$Id."'";
		$res_flyers = $db->get($sql_flyers);	
		$row_flyers = $db->fetch_array($res_flyers);
		$image = "../uploads/flyers/".$row_flyers['flyer_image'];
  
  		if(isset($_POST['btnSave']) && empty($_POST['btnSave'])==false){
			
			if($_FILES['image_path']['name']!=""){
				require_once('../includes/file.upload.inc.php');

				$objFileUpload = new FileUpload();
				
				$objFileUpload->UploadMode = 'Edit';

				$objFileUpload->OldFileName = $row_flyers['flyer_image'];

				$objFileUpload->UploadContent = $_FILES['image_path'];

				$objFileUpload->UploadFolder = "../uploads/flyers";

				$image_return = $objFileUpload->Upload();
				
				$image_path = $image_return['server_name'];
			}else
				$image_path = $row_flyers['flyer_image'];
			
			
			//Record is inserted into the table
			$insert_sql_array = array("flyer_image" => $image_path,
										"caption1" => $f->post('caption1'),
										"caption2" => $f->post('caption2'));
			$db->update(TSF,$insert_sql_array,"flyer_id",$Id);
			
			$f->Redirect(CP."?index=Edit&Id=".$Id."&msg=".urlencode("Record successfully updated!"));

			
		}

  ?>
  <?php if($msg!=""){
   ?>
<table width="100%"  border="0" cellpadding="0" cellspacing="0">
  <tr>
    <td colspan="3" scope="col" height="30" align="center"><?php echo $msg;?></td>
    </tr>

</table>
	<?php
	}?>

 <table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <th class="padding" width="12%" align="center" valign="top" bgcolor="#d9d6d6" scope="col">FLYER 1</th>
    <th width="0%" scope="col">&nbsp;</th>
    <th class="padding" width="22%" align="right" bgcolor="#d9d6d6" scope="col"><img src="resize.php?imgfile=<?php echo $image;?>&max_width=207&max_height=234" alt="" /><br />
	<input name="image_path" id="image_path" type="file" />
	</th>
    <th width="2%" scope="col" bgcolor="#d9d6d6">&nbsp;</th>
    <th class="padding" width="64%" align="left" valign="top" bgcolor="#d9d6d6" scope="col">Size: exactly 415 x 467, 72 dpi</th>
    
  </tr>
</table>


<table width="100%" border="0" cellspacing="0" cellpadding="0" height="30">
  <tr>
    <th width="12%" align="center" valign="middle" bgcolor="#e6e6e6" scope="col">CAPTION</th>
    <th width="0%" scope="col" >&nbsp;</th>
    <th width="2%" align="center" valign="middle" bgcolor="#e6e6e6" scope="col">&nbsp;</th>
    <th width="34%" align="right" valign="middle" bgcolor="#e6e6e6" scope="col"><input name="caption1" id="caption1" type="text" value="<?php echo $f->getValue($row_flyers['caption1']);?>" class="input16 required" /></th>
    <th width="52%" align="center" valign="middle" bgcolor="#e6e6e6" scope="col">&nbsp;</th>
    
  </tr>
</table>

<table width="100%" border="0" cellspacing="0" cellpadding="0" height="30px">
  <tr>
    <th width="12%" align="center" valign="middle" bgcolor="#e6e6e6" scope="col">&nbsp;</th>
    <th width="0%" scope="col" >&nbsp;</th>
    <th width="2%" align="center" valign="middle" bgcolor="#e6e6e6" scope="col">&nbsp;</th>
    <th width="34%" align="right" valign="middle" bgcolor="#e6e6e6" scope="col"><input name="caption2" id="caption2" type="text" value="<?php echo $f->getValue($row_flyers['caption2']);?>" class="input16 required" /></th>
    <th width="52%" align="center" valign="middle" bgcolor="#e6e6e6" scope="col">&nbsp;</th>
    
  </tr>
</table>



<table class="savesort" width="100%"  border="0" cellpadding="0" cellspacing="0">
  <tr>
    <td  width="12%" scope="col">&nbsp;</td>
    <td  width="4%" scope="col">&nbsp;</td>
    <td  width="84%" scope="col"><input name="btnSave" id="btnSave" type="submit" value=" SAVE " class="input17" /></td>
  </tr>
</table>
<?php }?>
</form>
 
 


<div class="clear"></div>
</div>   
<div class="clear"></div>
</div>
<!--main-->
</body>
</html>
