<?php
	require_once("../includes/config.inc.php");
	$f->redirectBase = WEBSITE_URL;
	$f->isLogin('_admin','index.php');
	
	$page_id = 1;
	
	define("TS","tbl_shows",true);
	$msg = $_GET['msg'];
	$index = $_GET['index'];
	
	//All the current shows are fetched
	$sql_cur = "SELECT * FROM ".TS." WHERE `show_type`='1' ORDER BY `sequence`";
	$res_cur = $db->get($sql_cur);
	$records = $db->num_rows($res_cur);
	
	//All the past shows are fetched
	$sql_past = "SELECT * FROM ".TS." WHERE `show_type`='2' ORDER BY `sequence`";
	$res_past = $db->get($sql_past);
	$records_past = $db->num_rows($res_past);
	
	if(isset($_GET['action']) && $_GET['action']=="delete"){
		$Id = $_GET['Id'];
		
		//image path is fetched
		$sql_show = "SELECT * FROM ".TS." WHERE `show_id`='".$Id."'";
		$res_show = $db->get($sql_show);
		$row_show = $db->fetch_array($res_show);
		unlink("../uploads/shows/".$row_show['image_path']);
		
		$sql = "DELETE FROM `".TS."` WHERE `show_id`='".$Id."'";
		$db->get($sql);
		
		$f->Redirect(CP."?msg=".urlencode("Record successfully deleted!"));
	}

	if($index == 'Sequence' && $_GET['action']=='SQ'){
		$sql_up = "UPDATE `".TS."` SET `display`='No' WHERE `show_type`='1'";
		$db->get($sql_up);
		
		foreach($_POST['the_display'] as $val):
			$sql = "UPDATE `".TS."` SET `display`='Yes' WHERE `show_id`=".$val." AND `show_type`='1'";
			$db->get($sql,__FILE__,__LINE__);							
		endforeach;
		
		
		$sql = "SELECT * FROM `".TS."` WHERE `show_type`='1' ORDER BY `sequence` ASC";
		$res = $db->get($sql);
		while($row = $db->fetch_array($res)) {
			$sq = $_POST['sq'.$row['show_id']];
			if($sq != $row['sequence']) {
				$sql = "SELECT * FROM ".TS." WHERE `show_id`=".$row['show_id'];
				$res_faq = $db->get($sql,__FILE__,__LINE__);
				$faq = $db->fetch_array($res_faq);
				$sql = "UPDATE `".TS."` SET `sequence`=".$faq['sequence']." WHERE `sequence`=".$sq." AND `show_type`='1' ";
				$db->get($sql,__FILE__,__LINE__);
				$sql = "UPDATE `".TS."` SET `sequence`='".$sq."' WHERE `show_id`=".$row['show_id']." ";
				$db->get($sql,__FILE__,__LINE__);
			}
		}
		$f->Redirect(CP."?index=List&msg=".urlencode("Sequence has been successfully updated!"));

	}
	
	if($index == 'Sequence' && $_GET['action']=='SQ_PAST'){
		$sql = "SELECT * FROM `".TS."` WHERE `show_type`='2' ORDER BY `sequence` ASC";
		$res = $db->get($sql);
		while($row = $db->fetch_array($res)) {
			$sq = $_POST['sq'.$row['show_id']];
			if($sq != $row['sequence']) {
				$sql = "SELECT * FROM ".TS." WHERE `show_id`=".$row['show_id'];
				$res_faq = $db->get($sql,__FILE__,__LINE__);
				$faq = $db->fetch_array($res_faq);
				$sql = "UPDATE `".TS."` SET `sequence`=".$faq['sequence']." WHERE `sequence`=".$sq." AND `show_type`='2' ";
				$db->get($sql,__FILE__,__LINE__);
				$sql = "UPDATE `".TS."` SET `sequence`='".$sq."' WHERE `show_id`=".$row['show_id']." ";
				$db->get($sql,__FILE__,__LINE__);
			}
		}
		$f->Redirect(CP."?index=List&msg=".urlencode("Sequence has been successfully updated!"));

	}

	if($index == "changetype"){
		$Id = $_GET['Id'];
		
		//Max sequence is fetched
		$sql="SELECT MAX(sequence) FROM `".TS."`";
		$res=$db->get($sql);
		$row=$db->fetch_array($res);
		$sequence = $row[0]+1;
		
		
		$update_sql_array = array("show_type" => "2","sequence"=>$sequence);
		$db->update(TS,$update_sql_array,"show_id",$Id);
		$f->Redirect(CP."?index=List&msg=".urlencode("Show status changed successfully!"));
	}

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<?php include("js.css.inc.php");?>
</head>
<script type="text/javascript">
$(document).ready(function(){
		$('.delete').click(function()

		{

			var href = $(this).attr('href');

			var title = $(this).attr('rel');

			var text = '<div id="a" align="center"><strong>Are you sure you want to delete the Show?</div><br><div id="b" align="center"><strong>'+title+'</strong><div>';

			jConfirm(text, 'Confirmation', function(r){

				if(r == true){

					window.location.href = href;

				}

			});

			return false;

		});
		

});
function __doPostSQ() {
    var str = "";
    flag = false;
    var sq = document.getElementById('sq').value;
    sq = sq.split(",");
    //alert(sq.length);
    for(var j=0;j<sq.length;j++) {
        str = document.getElementById('sq' + sq[j]).value;
        if(str == 0 || str == "") {
            flag = true;
        }
    }
    if(flag == true) {
        alert("Sequence can not be blank and should be greater than zero");
    } else {
        document.frmSchedule.submit();
    }
}
function __doPostPastSQ() {
    var str = "";
    flag = false;
    var sq = document.getElementById('sq_past').value;
    sq = sq.split(",");
    //alert(sq.length);
    for(var j=0;j<sq.length;j++) {
        str = document.getElementById('sq_past' + sq[j]).value;
        if(str == 0 || str == "") {
            flag = true;
        }
    }
    if(flag == true) {
        alert("Sequence can not be blank and should be greater than zero");
    } else {
        document.frmPastSchedule.submit();
    }
}
</script>

<body>
<!--main-->
<div id="main">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td align="center" valign="top"><table width="1131" border="0" align="center" cellpadding="0" cellspacing="0">
      <tr>
        <td align="center" valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0">
		  <tr>
		  	<td colspan="2">
				<?php include("header.inc.php");?>				
			</td>
		  </tr>            
          <tr>
            <td height="22" colspan="2" align="left" valign="top" bgcolor="#444444"><table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <th width="30" scope="row">&nbsp;</th>
                <td width="1101" class="style3">CURRENT SCHEDULE</td>
              </tr>
            </table></td>
          </tr>
          <tr>
            <td height="22" colspan="2" align="left" valign="top" bgcolor="#bcbcbc"><table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <th width="30" scope="row">&nbsp;</th>
                <td width="143" class="style4"><a href="showsscheduledetail.php?index=Add">ADD NEW SHOW</a></td>
                <td width="958" class="style4"><a href="showsflyers.php">SHOW FLYERS</a></td>
              </tr>
            </table></td>
          </tr>
        </table></td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td align="left" valign="top" class="contaner">&nbsp;</td>
  </tr>
  <?php if($msg!=""){
  ?>
  <tr>
    <td align="center" valign="top" class="contaner" height="30"><?php echo urldecode($msg);?></td>
  </tr>
  <?php
  }?>
  </table>
<div class="contaner">
<form name="frmSchedule" id="frmSchedule" action="<?php echo CP.'?index=Sequence&action=SQ';?>" method="post">
<?php
	if($db->num_rows($res_cur)>0){ 
?>
<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td align="center" valign="top">
		<table  border="0" cellspacing="0" cellpadding="0">
		<?php
				$cnt = 0;
				while($row_cur = $db->fetch_array($res_cur)){
					if($cnt == 0){
						$bgcolor = "#d9d6d6";
						$cnt++;
					}
					else if($cnt == 1){
						$bgcolor = "#e6e6e6";
						$cnt--;
					}
					
					if($row_cur['headline']!="")
						$headline = $f->getValue($row_cur['headline']);
					else
						$headline = $f->getValue($row_cur['location']);
		?>
		  <tr>
			<th width="24" height="25" align="center" bgcolor="<?php echo $bgcolor;?>" scope="col">&nbsp;</th>
			<th width="165" align="left" valign="middle" bgcolor="<?php echo $bgcolor;?>" scope="col"><?php echo $headline;?></th>
			<th width="1" align="center"></th>
			<th width="60" align="right" valign="middle" bgcolor="<?php echo $bgcolor;?>" scope="col"><a href="showsscheduledetail.php?index=Edit&Id=<?php echo $row_cur['show_id'];?>"><img src="images/pencil.png" width="14" height="14" alt="" /></a></th>
			<th width="77" align="center" valign="middle" bgcolor="<?php echo $bgcolor;?>" scope="col"><a href="<?php echo CP;?>?action=delete&Id=<?php echo $row_cur['show_id'];?>" rel="<?php echo $f->getValue($row_cur['headline']);?>" class="delete"><img src="images/delite.png" width="14" height="13" alt="" /></a></th>
			<th width="70" align="left" bgcolor="<?php echo $bgcolor;?>" scope="col"><a href="<?php echo CP;?>?index=changetype&Id=<?php echo $row_cur['show_id'];?>"><img src="images/stamp.png" width="18" height="18" alt="" /></a></th>
			<th width="1" align="center"></th>
            <th width="90" bgcolor="<?php echo $bgcolor;?>"><input type="text" size="1" class="input7" value="<?php echo $row_cur['sequence'];?>" id="sq<?php echo $row_cur['show_id'];?>" name="sq<?php echo $row_cur['show_id'];?>" style="text-align: center;" /></th>
			<th width="1" align="center"></th>
            <th width="143" bgcolor="<?php echo $bgcolor;?>">About Display</th>
            <th width="23" bgcolor="<?php echo $bgcolor;?>"><input name="the_display[]" type="checkbox" value="<?php echo $row_cur['show_id'];?>"<?php if($row_cur['display'] == 'Yes'){ echo ' checked';}?> /></th>
            <th width="484" bgcolor="<?php echo $bgcolor;?>" scope="col">&nbsp;</th>
		  </tr>
		 <?php 
			   $counter.= $row_cur['show_id'].",";

			}
			$counter = substr($counter,0,strlen($counter)-1);

		 ?>
		<input name="sq" type="hidden" id="sq" value="<?php echo $counter;?>" />
        <input name="type" type="hidden" id="type" value="Sequence" />

		</table>    
    </td>
    </tr>
 </table>
<table width="100%" border="0" cellpadding="0" cellspacing="0">
  <tr>
    <td width="545" scope="col">&nbsp;</td>
    <td width="586" scope="col">&nbsp;</td>
  </tr>
  <tr>
    <td scope="col">&nbsp;</td>
    <td scope="col"><input type="button" value="SAVE" class="input2" onclick="javascript:__doPostSQ();" name="btnChangeSQ"  id="btnChangeSQ" <?php if($records == 0) {?> disabled="disabled"<?php }?> /></td>
  </tr>
  <tr>
    <td scope="col">&nbsp;</td>
    <td scope="col">&nbsp;</td>
  </tr>
</table>
<?php }else{
?>
<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td align="center" valign="top">
		<table width="100%" border="0" cellspacing="0" cellpadding="0">
		  <tr>
			<th height="25" align="center" bgcolor="#d9d6d6" scope="col">No Results Found</th>
			</tr>
		</table>    
    </td>
    </tr>
 </table>
 <table width="100%" border="0" cellpadding="0" cellspacing="0">
  <tr>
    <td width="400" scope="col">&nbsp;</td>
    <td width="731" scope="col">&nbsp;</td>
  </tr>
</table>
<?php
}?>
</form>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
        <td colspan="2" align="left" valign="top" class="headerline1"><table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <th width="30" scope="row">&nbsp;</th>
            <td width="1101" class="style3">PAST SHOWS</td>
          </tr>
        </table></td>
        </tr>
      <tr>
        <td colspan="2" align="left" valign="top" class="headerline2">&nbsp;</td>
        </tr>
  </table>
<?php
	if($db->num_rows($res_past)>0){ 
?>
<form name="frmPastSchedule" id="frmPastSchedule" action="<?php echo CP.'?index=Sequence&action=SQ_PAST';?>" method="post">
<table width="100%" border="0" cellspacing="0" cellpadding="0">

        <td>&nbsp;</td>
        </tr>
    </table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr >	
    <td align="center"><table width="100%" border="0" cellspacing="0" cellpadding="0">
	<?php
			$counter = "";
			while($row_past = $db->fetch_array($res_past)){
			
			if($row_past['headline']!="")
				$headline = $f->getValue($row_past['headline']);
			else
				$headline = $f->getValue($row_past['location']);
	?>
      <tr>
        <th width="30" height="25" align="center" bgcolor="#d9d6d6" scope="col">&nbsp;</th>
        <th width="165" align="left" valign="middle" bgcolor="#d9d6d6" scope="col"><?php echo $headline;?></th>
        <th width="1" align="center"></th>
        <th width="70" align="right" valign="middle" bgcolor="#d9d6d6" scope="col"><a href="showsscheduledetail.php?index=Edit&Id=<?php echo $row_past['show_id'];?>"><img src="images/pencil.png" width="14" height="14" alt="" /></a></th>
        <th width="90" align="center" valign="middle" bgcolor="#d9d6d6" scope="col"><a href="<?php echo CP;?>?action=delete&Id=<?php echo $row_past['show_id'];?>" rel="<?php echo $f->getValue($row_past['headline']);?>" class="delete"><img src="images/delite.png" width="14" height="13" alt="" /></a></th>
        <th width="80" align="left" bgcolor="#d9d6d6" scope="col"><input type="text" size="1" class="input7" value="<?php echo $row_past['sequence'];?>" id="sq_past<?php echo $row_past['show_id'];?>" name="sq<?php echo $row_past['show_id'];?>" style="text-align: center;" /></th>
        <th width="100" bgcolor="#d9d6d6">&nbsp;</th>
        <th width="630" bgcolor="#d9d6d6" scope="col">&nbsp;</th>
      </tr>
	 <?php 
	 		$counter.= $row_past['show_id'].",";
		}
		$counter = substr($counter,0,strlen($counter)-1);
	 ?>
	 	<input name="sq" type="hidden" id="sq_past" value="<?php echo $counter;?>" />
        <input name="type" type="hidden" id="type" value="Sequence" />
    </table></td>
    </tr>
</table>    
<table width="100%" border="0" cellpadding="0" cellspacing="0">
  <tr>
    <td width="400" scope="col">&nbsp;</td>
    <td width="731" scope="col">&nbsp;</td>
  </tr>
  <tr>
    <td scope="col">&nbsp;</td>
    <td scope="col"><input type="button" value="SAVE SORT" class="input2" onclick="javascript:__doPostPastSQ();" name="btnChangeSQ"  id="btnChangeSQ" <?php if($records_past == 0) {?> disabled="disabled"<?php }?> /></td>
  </tr>
  <tr>
    <td scope="col">&nbsp;</td>
    <td scope="col">&nbsp;</td>
  </tr>
</table>
</form>
<?php }else{
?>
<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td align="center" valign="top">
		<table width="100%" border="0" cellspacing="0" cellpadding="0">
		  <tr>
			<th height="25" align="center" bgcolor="#d9d6d6" scope="col">No Results Found</th>
			</tr>
		</table>    
    </td>
    </tr>
 </table>
 <table width="100%" border="0" cellpadding="0" cellspacing="0">
  <tr>
    <td width="400" scope="col">&nbsp;</td>
    <td width="731" scope="col">&nbsp;</td>
  </tr>
</table>
<?php
}?>
 

<div class="clear"></div>
</div>  
  
  
  
<div class="clear"></div>
</div>
<!--main-->
</body>
</html>
