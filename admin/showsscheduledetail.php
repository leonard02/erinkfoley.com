<?php
	require_once("../includes/config.inc.php");
	$f->redirectBase = WEBSITE_URL;
	$f->isLogin('_admin','index.php');
	
	$page_id = 1;
	
	define("TS","tbl_shows",true);
	define("TSD","tbl_show_dates",true);

	$index = $_GET['index'];
	
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<?php include("js.css.inc.php");?>
<script type="text/javascript">
$(document).ready(function(){
	$('#frmShow').validate();
	$("#image_path").rules("add", {
		<?php if($index=="Add"){
		?>
		required: true,
		<?php
		}?>
		accept: 'jpg|jpeg|png|gif|JPG|JPEG|PNG|GIF',
		messages: {
			accept: "Accept only jpg, png, gif files"
		}
		});
	$('.summarizedate').rules("add",{
		required: true
	});
	$('#location').focus(function(){
		if($(this).val()=="Name")
			$(this).val("");
	});
	$('#city_state').focus(function(){
		if($(this).val()=="City, State")
			$(this).val("");
	});
/*	$('#summarized_dates').click(function(){
		if($('#summarized_dates').is(':checked') == false){
			$('#from_date').val("");
			$('#to_date').val("");
		}else
			$("#spanerror").hide();
		
	});
	$('#list_dates').click(function(){
		if($('#list_dates').is(':checked') == false){
			$('.showdate').val("");
			$('.showtime').val("");
		}else
			$("#spanerror").hide();
	});

*/
	$('.summarizedate').click(function(){
		if($(this).attr('id')=="summarized_dates2"){
				$('#from_date').val("");
				$('#to_date').val("");
				$("#spanerror").hide();
		}else if($(this).attr('id')=="summarized_dates1"){
				$('.showdate').val("");
				$('.showtime').val("");
				$("#spanerror").hide();
		}else
			$("#spanerror").show();
	});


});
</script>
<link rel="stylesheet" href="http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css" />
<script src="http://code.jquery.com/jquery-1.9.1.js"></script>
<script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
<script src="../js/jquery-ui-timepicker-addon.js" type="text/javascript"></script>
<script type="text/javascript">

var $j = jQuery.noConflict();

$j(function(){
	$j("#from_date").datepicker({
            dateFormat: 'mm-dd-yy',
            changeMonth: true,
            changeYear: true,
            showOn: 'button',
            buttonImage: 'images/calender.png'
        });

	$j("#to_date").datepicker({
            dateFormat: 'mm-dd-yy',
            changeMonth: true,
            changeYear: true,
            showOn: 'button',
            buttonImage: 'images/calender.png'
        });
	$j('#summarize_time').timepicker({
		timeFormat: "hh:mm tt"
	});
	$j('.showdate').datepicker({
		dateFormat: 'mm-dd-yy'
	});
	$j( ".showtime" ).timepicker({
		timeFormat: "hh:mm tt"
	});

});
</script>
</head>
<body>
<!--main-->
<div id="main">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td align="center" valign="top"><table width="1131" border="0" align="center" cellpadding="0" cellspacing="0">
      <tr>
        <td align="center" valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0">
		  <tr>
		  	<td colspan="2">
				<?php include("header.inc.php");?>				
			</td>
		  </tr>            
          <tr>
            <td height="22" colspan="2" align="left" valign="top" bgcolor="#444444"><table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <th width="30" scope="row">&nbsp;</th>
                <td width="1101" class="style3"><?php echo strtoupper($index);?> SCHEDULE</td>
              </tr>
            </table></td>
          </tr>
          <tr>
            <td height="22" colspan="2" align="left" valign="top" bgcolor="#bcbcbc"><table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <th width="30" scope="row">&nbsp;</th>
                <td width="1101" class="style4"><a href="showsschedule.php">BACK TO LIST</a></td>
              </tr>
            </table></td>
          </tr>
        </table></td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td align="left" valign="top" class="contaner">&nbsp;</td>
  </tr>
  </table>

<div class="contaner">
<?php
	if($index=="Add"){ 
	
		if(isset($_POST['btnSave']) && empty($_POST['btnSave'])==false){
			
			if($_FILES['image_path']['name']!=""){
				require_once('../includes/file.upload.inc.php');

				$objFileUpload = new FileUpload();
										
				$objFileUpload->UploadContent = $_FILES['image_path'];

				$objFileUpload->UploadFolder = "../uploads/shows";

				$image_return = $objFileUpload->Upload();
				
				$image_path = $image_return['server_name'];
			}
			
			$summarized_dates = $f->post('summarized_dates');
			
			if($summarized_dates=="summarized_dates"){
				$from_date = $dt->dd_mm_yyyy2yyyy_mm_dd2($_POST['from_date'],"-");
				$to_date = $dt->dd_mm_yyyy2yyyy_mm_dd2($_POST['to_date'],"-");
				$summarize_time = $f->post('summarize_time');
				$summarized_dates = "Y";
				$list_dates = "N";
			}else{
				$list_dates = "Y";
				$summarized_dates = "N";
			}
			
			$sequence_id = $m->getMaxSequence(TS,"1");
			
			//Record is inserted into the table
			$insert_sql_array = array("headline" => $f->post('headline'),
										"image_path" => $image_path,
										"location" => $f->post('location'),
										"city_state" => $f->post('city_state'),
										"link" => $f->post('link'),
										"summarized_dates" => $summarized_dates,
										"list_dates" => $list_dates,
										"from_date" => $from_date,
										"to_date" => $to_date,
										"summarize_time" => $summarize_time,
										"sequence" => $sequence_id,
										"create_date" => 'CURDATE()',
										"create_time" => 'CURTIME()');
			$db->insert(TS,$insert_sql_array);
			$show_id = $db->last_insert_id();
			
			
			if($list_dates=="Y"){
				$from_time1 = $_POST['from_time'];
				$to_time1 = $_POST['to_time'];
				foreach($_POST['show_date'] as $key=>$val){
					if($val!=""){
					$show_date = $dt->dd_mm_yyyy2yyyy_mm_dd2($val,"-");
					$from_time = $from_time1[$key];
					$to_time = $to_time1[$key];
					
					
					$insert_sql_array = array("show_id" => $show_id,
										  "show_date" => $show_date,
										  "from_time" => $from_time,
										  "to_time" => $to_time);
				
					$db->insert(TSD,$insert_sql_array);
					}


				}
				
			}
			$f->Redirect("showsschedule.php?msg=".urlencode("Record successfully added!"));

			
		}
?>
<form name="frmShow" id="frmShow" action="<?php echo CP;?>?<?php echo QS;?>" method="post" enctype="multipart/form-data">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="21" align="left" valign="middle" bgcolor="#d9d6d6" scope="col">&nbsp;</td>
    <td width="130" align="left" valign="top" bgcolor="#d9d6d6" scope="col">IMAGE</td>
    <td width="1" align="left" valign="top"></td>
    <td width="30" align="right" valign="middle" bgcolor="#d9d6d6" scope="col">&nbsp;</td>
    <td width="108" align="left" valign="middle" bgcolor="#d9d6d6" scope="col">&nbsp;</td>
    <td width="841" align="left" valign="top" scope="col" bgcolor="#d9d6d6"></td>
  </tr>
  <tr>
    <td width="21" align="left" valign="middle" bgcolor="#d9d6d6" scope="col">&nbsp;</td>
    <td width="130" align="left" valign="middle" bgcolor="#d9d6d6" scope="col">&nbsp;</td>
    <td  width="1" align="left" valign="top"></td>
    <td  width="30" align="left" valign="middle" bgcolor="#d9d6d6" scope="col">&nbsp;</td>
    <td  width="108" align="left" valign="middle" bgcolor="#d9d6d6" scope="col">Upload Image<br />
	(Size: exactly 82 x 94, 72 dpi)
	</td>
    <td width="841" align="left" valign="top" bgcolor="#d9d6d6" class="padding" scope="col" ><input name="image_path" id="image_path" type="file" class="required" />&nbsp;<span class="errorContainer"></span></td>
  </tr>
  <tr>
    <td align="left" valign="middle" bgcolor="#d9d6d6" scope="col">&nbsp;</td>
    <td align="left" valign="middle" bgcolor="#d9d6d6" scope="col">&nbsp;</td>
    <td align="left" valign="top"></td>
    <td align="left" valign="middle" bgcolor="#d9d6d6" scope="col">&nbsp;</td>
    <td align="left" valign="middle" bgcolor="#d9d6d6" scope="col">&nbsp;</td>
    <td align="left" valign="middle" bgcolor="#d9d6d6" class="padding" scope="col" >&nbsp;</td>
  </tr>
  <tr>
    <td align="left" valign="middle" bgcolor="#e6e6e6">&nbsp;</td>
    <td align="left" valign="top" bgcolor="#e6e6e6">DATE</td>
    <td align="left" valign="top"></td>
    <td align="left" valign="middle" bgcolor="#e6e6e6">&nbsp;</td>
    <td colspan="2" align="left" valign="middle"><table width="100%" border="0" cellspacing="0" cellpadding="0" height="225" bgcolor="#e6e6e6">
      <tr>
        <th height="25" colspan="9" align="left" valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0" height="35">
          <tr>
            <th width="13%" height="35" align="left" valign="middle" bgcolor="#e6e6e6" scope="col">Summarize Dates</th>
            <th width="17%" align="left" valign="middle" bgcolor="#e6e6e6" scope="col"><input name="summarized_dates" id="summarized_dates1" class="summarizedate" type="radio" value="summarized_dates" ></th>
            <th width="4%" align="right" valign="middle" bgcolor="#e6e6e6" scope="col">&nbsp;</th>
            <th width="8%" align="left" valign="middle" bgcolor="#e6e6e6" scope="col">List Dates</th>
            <th width="4%" align="left" valign="middle" bgcolor="#e6e6e6" scope="col"><input name="summarized_dates" id="summarized_dates2" type="radio" value="list_dates" class="summarizedate" /></th>
            <th width="54%" align="left" bgcolor="#e6e6e6" scope="col"><span id="spanerror">Please select either Summarize Dates or List Dates</span></th>
          </tr>
        </table></th>
        </tr>
      <tr>
        <th height="25" colspan="3" align="left" valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0">
		  <tr>
			<th width="18%" height="25" align="left" valign="middle">From</th>
			<th width="82%" align="left" valign="middle"><input name="from_date" id="from_date" type="text" value="" class="input18" /></th>
		  </tr>
		  <tr>
			<th width="18%" height="25" align="left" valign="middle">To</th>
			<th width="82%" align="left" valign="middle"><input name="to_date" id="to_date" type="text" value="" class="input18" /></th>
		  </tr>
		  <tr>
		    <th height="25" align="left" valign="middle">Time</th>
		    <th align="left" valign="middle"><input name="summarize_time" id="summarize_time" type="text" value="" class="input18" /></th>
		    </tr>
        </table></th>
        <th colspan="6" align="right" valign="top" id="divScheduleDates"><table width="97%" border="0" cellspacing="0" cellpadding="0">
			<?php 
			for($i=1;$i<=7;$i++){
			?>
			  <tr>
				<th align="left" valign="middle">Day <?php echo $i;?></th>
				<th align="left" valign="middle"><input name="show_date[]" id="show_date<?php echo $i;?>" type="text" value="" class="input18 showdate" /></th>
				<th align="left" valign="middle">Time 1</th>
				<th align="left" valign="middle" scope="col"><input name="from_time[]" id="from_time<?php echo $i;?>" type="text" value="" class="input19 showtime" /></th>
				<th align="left" valign="middle">Time 2</th>
				<th align="left" valign="middle"><input name="to_time[]" id="to_time<?php echo $i;?>" type="text" value="" class="input19 showtime" /></th>
			  </tr>
			 <?php }?>
        </table></th>
        </tr>
    </table></td>
    </tr>
  <tr>
    <td height="36" align="left" valign="middle" bgcolor="#d9d6d6" scope="col">&nbsp;</td>
    <td align="left" valign="middle" bgcolor="#d9d6d6" scope="col">HEADLINE (optional)</td>
    <td align="left" valign="top"></td>
    <td align="left" valign="middle" bgcolor="#d9d6d6" scope="col">&nbsp;</td>
    <td colspan="2" align="left" valign="middle" bgcolor="#d9d6d6" scope="col"><input name="headline" id="headline" type="text" value="" class="input6" /></td>
    </tr>
  <tr>
    <td align="left" valign="middle" bgcolor="#e6e6e6">&nbsp;</td>
    <td align="left" valign="middle" bgcolor="#e6e6e6">LOCATION</td>
    <td align="left" valign="top"></td>
    <td align="left" valign="middle" bgcolor="#e6e6e6">&nbsp;</td>
    <td colspan="2" align="left" valign="middle" bgcolor="#e6e6e6"><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <th width="33%" height="2" align="left" valign="middle" bgcolor="#e6e6e6"><input name="location" id="location" type="text" value="Name" class="input16 required" />&nbsp;<span class="errorContainer"></span></th>
        <th width="8%" align="center" valign="middle" bgcolor="#e6e6e6" scope="col">Linc</th>
        <th width="59%" align="left" valign="middle" bgcolor="#e6e6e6" scope="col"><input name="link" id="link" type="text" value="http://www.itunes.com/exampletest" class="input16" />&nbsp;<span class="errorContainer"></span></th>
      </tr>
    </table></td>
    </tr>
  <tr>
    <td align="left" valign="middle" bgcolor="#e6e6e6">&nbsp;</td>
    <td align="left" valign="middle" bgcolor="#e6e6e6">&nbsp;</td>
    <td align="left" valign="top"></td>
    <td align="left" valign="middle" bgcolor="#e6e6e6">&nbsp;</td>
    <td colspan="2" align="left" valign="middle" bgcolor="#e6e6e6"><input name="city_state" id="city_state" type="text" value="City, State" class="input16 required" />&nbsp;<span class="errorContainer"></span></td>
    </tr>
</table>
<table width="100%"  border="0" cellpadding="0" cellspacing="0">
  <tr>
    <td colspan="3" scope="col" height="10"></td>
    </tr>
  <tr>
    <td width="14%" scope="col">&nbsp;</td>
    <td width="2%" scope="col">&nbsp;</td>
    <td width="84%" scope="col"><input name="btnSave" type="submit" value=" SAVE " class="input2" /></td>
  </tr>
  <tr>
    <td scope="col">&nbsp;</td>
    <td scope="col">&nbsp;</td>
    <td scope="col">&nbsp;</td>
  </tr>
</table>
</form>
<?php }else if($index=="Edit"){ 

		$Id = $_GET['Id'];
		
		//Show related info is fetched
		$sql_show = "SELECT * FROM ".TS." WHERE `show_id`='".$Id."'";
		$res_show = $db->get($sql_show);
		$row_show = $db->fetch_array($res_show);
		
		if(isset($_POST['btnSave']) && empty($_POST['btnSave'])==false){
			
			if($_FILES['image_path']['name']!=""){
				require_once('../includes/file.upload.inc.php');

				$objFileUpload = new FileUpload();
				
				$objFileUpload->UploadMode = 'Edit';

				$objFileUpload->OldFileName = $row_show['image_path'];
										
				$objFileUpload->UploadContent = $_FILES['image_path'];

				$objFileUpload->UploadFolder = "../uploads/shows";

				$image_return = $objFileUpload->Upload();
				
				$image_path = $image_return['server_name'];
			}else
				$image_path = $row_show['image_path'];
			
			$summarized_dates = $f->post('summarized_dates');
			
			if($summarized_dates=="summarized_dates"){
				$from_date = $dt->dd_mm_yyyy2yyyy_mm_dd2($_POST['from_date'],"-");
				$to_date = $dt->dd_mm_yyyy2yyyy_mm_dd2($_POST['to_date'],"-");
				$summarize_time = $f->post('summarize_time');
				$summarized_dates = "Y";
				$list_dates = "N";
			}else{
				$list_dates = "Y";
				$summarized_dates = "N";
			}
			
			//Record is inserted into the table
			$update_sql_array = array("headline" => $f->post('headline'),
										"image_path" => $image_path,
										"location" => $f->post('location'),
										"city_state" => $f->post('city_state'),
										"link" => $f->post('link'),
										"summarized_dates" => $summarized_dates,
										"list_dates" => $list_dates,
										"from_date" => $from_date,
										"to_date" => $to_date,
										"summarize_time" => $summarize_time);
			$db->update(TS,$update_sql_array,"show_id",$Id);
			
			//Delete all the records in the tbl_show_dates against the show id
			$db->get("DELETE FROM `".TSD."` WHERE `show_id`='".$Id."'");
			
			if($list_dates=="Y"){
				$from_time1 = $_POST['from_time'];
				$to_time1 = $_POST['to_time'];
				foreach($_POST['show_date'] as $key=>$val){
					if($val!=""){
					$show_date = $dt->dd_mm_yyyy2yyyy_mm_dd2($val,"-");
					$from_time = $from_time1[$key];
					$to_time = $to_time1[$key];
					
					
					$insert_sql_array = array("show_id" => $Id,
										  "show_date" => $show_date,
										  "from_time" => $from_time,
										  "to_time" => $to_time);
				
					$db->insert(TSD,$insert_sql_array);
					}


				}
				
			}			
			$msg = "Record successfully updated!";

			
		}
		
		//Show related info is fetched
		$sql_show = "SELECT * FROM ".TS." WHERE `show_id`='".$Id."'";
		$res_show = $db->get($sql_show);
		$row_show = $db->fetch_array($res_show);
		
		$image = "../uploads/shows/".$row_show['image_path'];

	
?>
<form name="frmShow" id="frmShow" action="<?php echo CP;?>?<?php echo QS;?>" method="post" enctype="multipart/form-data">
<?php if($msg!=""){
?>
<table width="100%"  border="0" cellpadding="0" cellspacing="0">
  <tr>
    <td colspan="3" scope="col" height="30" align="center"><?php echo $msg;?></td>
    </tr>

</table>
<?php
}?>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td height="5" colspan="6" align="left" valign="middle" bgcolor="#d9d6d6" scope="col"></td>
    </tr>
  <tr>
    <td width="21" align="left" valign="middle" bgcolor="#d9d6d6" scope="col">&nbsp;</td>
    <td width="130" align="left" valign="top" bgcolor="#d9d6d6" scope="col">IMAGE</td>
    <td width="1" align="left" valign="top"></td>
    <td width="30" align="right" valign="middle" bgcolor="#d9d6d6" scope="col">&nbsp;</td>
    <td width="108" align="left" valign="bottom" bgcolor="#d9d6d6" scope="col"><img src="<?php echo $image;?>" width="82" height="94" alt="" /></td>
    <td width="841" align="left" valign="top" scope="col" bgcolor="#d9d6d6">Size: exactly 82 x 94, 72 dpi</td>
  </tr>
  <tr>
    <td width="21" align="left" valign="middle" bgcolor="#d9d6d6" scope="col">&nbsp;</td>
    <td width="130" align="left" valign="middle" bgcolor="#d9d6d6" scope="col">&nbsp;</td>
    <td  width="1" align="left" valign="top"></td>
    <td  width="30" align="left" valign="middle" bgcolor="#d9d6d6" scope="col">&nbsp;</td>
    <td  width="108" align="left" valign="middle" bgcolor="#d9d6d6" scope="col">Upload Image	</td>
    <td width="841" align="left" valign="top" bgcolor="#d9d6d6" class="padding" scope="col" ><input name="image_path" id="image_path" type="file" />&nbsp;<span class="errorContainer"></span></td>
  </tr>
  <tr>
    <td align="left" valign="middle" bgcolor="#d9d6d6" scope="col">&nbsp;</td>
    <td align="left" valign="middle" bgcolor="#d9d6d6" scope="col">&nbsp;</td>
    <td align="left" valign="top"></td>
    <td align="left" valign="middle" bgcolor="#d9d6d6" scope="col">&nbsp;</td>
    <td align="left" valign="middle" bgcolor="#d9d6d6" scope="col">&nbsp;</td>
    <td align="left" valign="middle" bgcolor="#d9d6d6" class="padding" scope="col" >&nbsp;</td>
  </tr>
  <tr>
    <td align="left" valign="middle" bgcolor="#e6e6e6">&nbsp;</td>
    <td align="left" valign="top" bgcolor="#e6e6e6">DATE</td>
    <td align="left" valign="top"></td>
    <td align="left" valign="middle" bgcolor="#e6e6e6">&nbsp;</td>
    <td colspan="2" align="left" valign="middle"><table width="100%" border="0" cellspacing="0" cellpadding="0" height="225" bgcolor="#e6e6e6">
      <tr>
        <th height="25" colspan="9" align="left" valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0" height="35">
		  <tr>
            <th width="13%" height="35" align="left" valign="middle" bgcolor="#e6e6e6" scope="col">Summarize Dates</th>
            <th width="17%" align="left" valign="middle" bgcolor="#e6e6e6" scope="col"><input name="summarized_dates" id="summarized_dates1" class="summarizedate" type="radio" value="summarized_dates" <?php if($row_show['summarized_dates']=="Y") echo "checked='checked'";?>></th>
            <th width="4%" align="right" valign="middle" bgcolor="#e6e6e6" scope="col">&nbsp;</th>
            <th width="8%" align="left" valign="middle" bgcolor="#e6e6e6" scope="col">List Dates</th>
            <th width="4%" align="left" valign="middle" bgcolor="#e6e6e6" scope="col"><input name="summarized_dates" id="summarized_dates2" type="radio" value="list_dates" class="summarizedate" <?php if($row_show['list_dates']=="Y") echo "checked='checked'";?>/></th>
            <th width="54%" align="left" bgcolor="#e6e6e6" scope="col"><span id="spanerror">Please select either Summarize Dates or List Dates</span></th>
          </tr>
        </table></th>
        </tr>
      <tr>
        <th height="25" colspan="3" align="left" valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0">
		  <tr>
			<th width="18%" height="25" align="left" valign="middle">From</th>
			<th width="82%" align="left" valign="middle"><input name="from_date" id="from_date" type="text" value="<?php if($row_show['from_date']!="0000-00-00") echo date("m-d-Y",strtotime($row_show['from_date']));?>" class="input18" /></th>
		  </tr>
		  <tr>
			<th width="18%" height="25" align="left" valign="middle">To</th>
			<th width="82%" align="left" valign="middle"><input name="to_date" id="to_date" type="text" value="<?php if($row_show['to_date']!="0000-00-00") echo date("m-d-Y",strtotime($row_show['to_date']));?>" class="input18" /></th>
		  </tr>
		  <tr>
		    <th height="25" align="left" valign="middle">Time</th>
		    <th align="left" valign="middle"><input name="summarize_time" id="summarize_time" type="text" value="<?php if($row_show['summarize_time']!="") echo $row_show['summarize_time'];?>" class="input18" /></th>
		  </tr>
        </table></th>
        <th colspan="6" align="right" valign="top" id="divScheduleDates"><table width="97%" border="0" cellspacing="0" cellpadding="0">
			<?php 
			if($row_show['list_dates']=="Y"){
			//all the dates are fetched 
			$sql_show_dates = "SELECT * FROM `".TSD."` WHERE `show_id`='".$Id."' ORDER BY `show_date` ASC";
			$res_show_dates = $db->get($sql_show_dates);
			$num_shows = $db->num_rows($res_show_dates);

			$i=1;
			while($row_show_dates = $db->fetch_array($res_show_dates)){
			?>
			  <tr>
				<th align="left" valign="middle">Day <?php echo $i;?></th>
				<th align="left" valign="middle"><input name="show_date[]" id="show_date<?php echo $i;?>" type="text" value="<?php echo date("m-d-Y",strtotime($row_show_dates['show_date']));?>" class="input18 showdate" /></th>
				<th align="left" valign="middle">Time 1</th>
				<th align="left" valign="middle" scope="col"><input name="from_time[]" id="from_time<?php echo $i;?>" type="text" value="<?php echo $row_show_dates['from_time'];?>" class="input19 showtime" /></th>
				<th align="left" valign="middle">Time 2</th>
				<th align="left" valign="middle"><input name="to_time[]" id="to_time<?php echo $i;?>" type="text" value="<?php echo $row_show_dates['to_time'];?>" class="input19 showtime" /></th>
			  </tr>
			 <?php 
			 	$i++;
			 	}
				 
			 }
			 
			 if($num_shows<7 || $row_show['list_dates']=="N"){
			 
			 	if($num_shows<7 && $row_show['list_dates']=="Y"){
					$j=7-$num_shows;
					$k=$i-1;
				}else{
					$k=1;
					$j=7;
				}
			 
				for($i=$k;$i<=$j;$i++){
				?>
				  <tr>
					<th align="left" valign="middle">Day <?php echo $i;?></th>
					<th align="left" valign="middle"><input name="show_date[]" id="show_date<?php echo $i;?>" type="text" value="" class="input18 showdate" /></th>
					<th align="left" valign="middle">Time 1</th>
					<th align="left" valign="middle" scope="col"><input name="from_time[]" id="from_time<?php echo $i;?>" type="text" value="" class="input19 showtime" /></th>
					<th align="left" valign="middle">Time 2</th>
					<th align="left" valign="middle"><input name="to_time[]" id="to_time<?php echo $i;?>" type="text" value="" class="input19 showtime" /></th>
				  </tr>
				 <?php }
			 }?>
        </table></th>
        </tr>
    </table></td>
    </tr>
  <tr>
    <td height="36" align="left" valign="middle" bgcolor="#d9d6d6" scope="col">&nbsp;</td>
    <td align="left" valign="middle" bgcolor="#d9d6d6" scope="col">HEADLINE (optional)</td>
    <td align="left" valign="top"></td>
    <td align="left" valign="middle" bgcolor="#d9d6d6" scope="col">&nbsp;</td>
    <td colspan="2" align="left" valign="middle" bgcolor="#d9d6d6" scope="col"><input name="headline" id="headline" type="text" value="<?php echo $f->getValue($row_show['headline']);?>" class="input6" /></td>
    </tr>
  <tr>
    <td align="left" valign="middle" bgcolor="#e6e6e6">&nbsp;</td>
    <td align="left" valign="middle" bgcolor="#e6e6e6">LOCATION</td>
    <td align="left" valign="top"></td>
    <td align="left" valign="middle" bgcolor="#e6e6e6">&nbsp;</td>
    <td colspan="2" align="left" valign="middle" bgcolor="#e6e6e6"><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <th width="33%" height="2" align="left" valign="middle" bgcolor="#e6e6e6"><input name="location" id="location" type="text" value="<?php echo $f->getValue($row_show['location']);?>" class="input16 required" />&nbsp;<span class="errorContainer"></span></th>
        <th width="8%" align="center" valign="middle" bgcolor="#e6e6e6" scope="col">Linc</th>
        <th width="59%" align="left" valign="middle" bgcolor="#e6e6e6" scope="col"><input name="link" id="link" type="text" value="<?php echo $f->getValue($row_show['link']);?>" class="input16" />&nbsp;<span class="errorContainer"></span></th>
      </tr>
    </table></td>
    </tr>
  <tr>
    <td align="left" valign="middle" bgcolor="#e6e6e6">&nbsp;</td>
    <td align="left" valign="middle" bgcolor="#e6e6e6">&nbsp;</td>
    <td align="left" valign="top"></td>
    <td align="left" valign="middle" bgcolor="#e6e6e6">&nbsp;</td>
    <td colspan="2" align="left" valign="middle" bgcolor="#e6e6e6"><input name="city_state" id="city_state" type="text" value="<?php echo $f->getValue($row_show['city_state']);?>" class="input16 required" />&nbsp;<span class="errorContainer"></span></td>
    </tr>
</table>
<table width="100%"  border="0" cellpadding="0" cellspacing="0">
  <tr>
    <td colspan="3" scope="col" height="10"></td>
    </tr>
  <tr>
    <td width="14%" scope="col">&nbsp;</td>
    <td width="2%" scope="col">&nbsp;</td>
    <td width="84%" scope="col"><input name="btnSave" type="submit" value=" SAVE " class="input2" /></td>
  </tr>
  <tr>
    <td scope="col">&nbsp;</td>
    <td scope="col">&nbsp;</td>
    <td scope="col">&nbsp;</td>
  </tr>
</table>
</form>
<?php }else if($index=="View"){

		$Id = $_GET['Id'];
		
		//Show related info is fetched
		$sql_show = "SELECT * FROM ".TS." WHERE `show_id`='".$Id."'";
		$res_show = $db->get($sql_show);
		$row_show = $db->fetch_array($res_show);
		
		$image = "../uploads/shows/".$row_show['image_path'];

	
?>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="21" align="left" valign="middle" bgcolor="#d9d6d6" scope="col">&nbsp;</td>
    <td width="130" align="left" valign="top" bgcolor="#d9d6d6" scope="col">IMAGE</td>
    <td width="1" align="left" valign="top"></td>
    <td width="30" align="right" valign="middle" bgcolor="#d9d6d6" scope="col">&nbsp;</td>
    <td width="108" align="left" valign="middle" bgcolor="#d9d6d6" scope="col"><img src="<?php echo $image;?>" width="82" height="94" alt="" /></td>
    <td width="841" align="left" valign="top" scope="col" bgcolor="#d9d6d6"></td>
  </tr>
  <tr>
    <td align="left" valign="middle" bgcolor="#d9d6d6" scope="col">&nbsp;</td>
    <td align="left" valign="middle" bgcolor="#d9d6d6" scope="col">&nbsp;</td>
    <td align="left" valign="top"></td>
    <td align="left" valign="middle" bgcolor="#d9d6d6" scope="col">&nbsp;</td>
    <td align="left" valign="middle" bgcolor="#d9d6d6" scope="col">&nbsp;</td>
    <td align="left" valign="middle" bgcolor="#d9d6d6" class="padding" scope="col" >&nbsp;</td>
  </tr>
  <tr>
    <td align="left" valign="middle" bgcolor="#e6e6e6">&nbsp;</td>
    <td align="left" valign="top" bgcolor="#e6e6e6">DATE</td>
    <td align="left" valign="top"></td>
    <td align="left" valign="middle" bgcolor="#e6e6e6">&nbsp;</td>
    <td colspan="2" align="left" valign="middle"><table width="100%" border="0" cellspacing="0" cellpadding="0" height="225" bgcolor="#e6e6e6">
      <tr>
        <th height="25" colspan="9" align="left" valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0" height="35">
          <tr>
            <th width="13%" height="35" align="left" valign="middle" bgcolor="#e6e6e6" scope="col">Summarize Dates</th>
            <th width="17%" align="left" valign="middle" bgcolor="#e6e6e6" scope="col"><input name="summarized_dates" id="summarized_dates" type="checkbox" value="Y" class="required" <?php if($row_show['summarized_dates']=="Y") echo "checked='checked'";?> /></th>
            <th width="4%" align="right" valign="middle" bgcolor="#e6e6e6" scope="col">&nbsp;</th>
            <th width="8%" align="left" valign="middle" bgcolor="#e6e6e6" scope="col">List Dates</th>
            <th width="12%" align="left" valign="middle" bgcolor="#e6e6e6" scope="col"><input name="list_dates" id="list_dates" type="checkbox" value="Y" class="required"  <?php if($row_show['list_dates']=="Y") echo "checked='checked'";?>/></th>
            <th width="46%" scope="col" bgcolor="#e6e6e6">&nbsp;</th>
          </tr>
        </table></th>
        </tr>
      <tr>
        <th height="25" colspan="3" align="left" valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0">
		  <tr>
			<th width="18%" height="25" align="left" valign="middle">From</th>
			<th width="82%" align="left" valign="middle"><input name="from_date" id="from_date" type="text" value="<?php echo date("m-d-Y",strtotime($row_show['from_date']));?>" class="input18 required" /></th>
		  </tr>
		  <tr>
			<th width="18%" height="25" align="left" valign="middle">To</th>
			<th width="82%" align="left" valign="middle"><input name="to_date" id="to_date" type="text" value="<?php echo date("m-d-Y",strtotime($row_show['to_date']));?>" class="input18 required" /></th>
		  </tr>
        </table></th>
        <th colspan="6" align="right" valign="top" id="divScheduleDates"><table width="97%" border="0" cellspacing="0" cellpadding="0">
			<?php 
			if($row_show['list_dates']=="Y"){
			//all the dates are fetched 
			$sql_show_dates = "SELECT * FROM `".TSD."` WHERE `show_id`='".$Id."' ORDER BY `show_date` ASC";
			$res_show_dates = $db->get($sql_show_dates);
			$i=1;
			while($row_show_dates = $db->fetch_array($res_show_dates)){
			?>
			  <tr>
				<th align="left" valign="middle">Day <?php echo $i;?></th>
				<th align="left" valign="middle"><input name="show_date[]" id="show_date<?php echo $i;?>" type="text" value="<?php echo date("m-d-Y",strtotime($row_show_dates['show_date']));?>" class="input18" /></th>
				<th align="left" valign="middle">Time 1</th>
				<th align="left" valign="middle" scope="col"><input name="from_time[]" id="from_time<?php echo $i;?>" type="text" value="<?php echo $row_show_dates['from_time'];?>" class="input19" /></th>
				<th align="left" valign="middle">Time 2</th>
				<th align="left" valign="middle"><input name="to_time[]" id="to_time<?php echo $i;?>" type="text" value="<?php echo $row_show_dates['to_time'];?>" class="input19" /></th>
			  </tr>
			 <?php 
			 	$i++;
			 }
			 }?>
        </table></th>
        </tr>
    </table></td>
    </tr>
  <tr>
    <td height="36" align="left" valign="middle" bgcolor="#d9d6d6" scope="col">&nbsp;</td>
    <td align="left" valign="middle" bgcolor="#d9d6d6" scope="col">HEADLINE (optional)</td>
    <td align="left" valign="top"></td>
    <td align="left" valign="middle" bgcolor="#d9d6d6" scope="col">&nbsp;</td>
    <td colspan="2" align="left" valign="middle" bgcolor="#d9d6d6" scope="col"><?php echo $f->getValue($row_show['headline']);?></td>
    </tr>
  <tr>
    <td align="left" valign="middle" bgcolor="#e6e6e6">&nbsp;</td>
    <td align="left" valign="middle" bgcolor="#e6e6e6">LOCATION</td>
    <td align="left" valign="top"></td>
    <td align="left" valign="middle" bgcolor="#e6e6e6">&nbsp;</td>
    <td colspan="2" align="left" valign="middle" bgcolor="#e6e6e6"><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <th width="33%" height="2" align="left" valign="middle" bgcolor="#e6e6e6"><?php echo $f->getValue($row_show['location']);?></th>
        <th width="8%" align="center" valign="middle" bgcolor="#e6e6e6" scope="col">Linc</th>
        <th width="59%" align="left" valign="middle" bgcolor="#e6e6e6" scope="col"><?php echo $f->getValue($row_show['link']);?></th>
      </tr>
    </table></td>
    </tr>
  <tr>
    <td align="left" valign="middle" bgcolor="#e6e6e6">&nbsp;</td>
    <td align="left" valign="middle" bgcolor="#e6e6e6">&nbsp;</td>
    <td align="left" valign="top"></td>
    <td align="left" valign="middle" bgcolor="#e6e6e6">&nbsp;</td>
    <td colspan="2" align="left" valign="middle" bgcolor="#e6e6e6"><?php echo $f->getValue($row_show['city_state']);?></td>
    </tr>
</table>
<?php }?>
<div class="clear"></div>
</div>  

<div class="clear"></div>
</div>
<!--main-->
</body>
</html>

