<?php include("includes/config.inc.php");

	define("TS","tbl_shows",true);
	define("TSD","tbl_show_dates",true);
	
	$offset = $_POST['offset'];
	$limit = $_POST['limit'];
	$action = $_POST['action'];

	//Present Shows info are fetched
	//$sql_shows = "SELECT * FROM `".TS."` WHERE `show_type`='present' AND `location`!='Name' AND `city_state`!='City, State'";
	//$sql_shows .= " UNION (SELECT * FROM `".TS."` WHERE `show_type`='past' AND `location`!='Name' AND `city_state`!='City, State') ORDER BY `sequence`";
	
	$sql_shows = "SELECT * FROM `".TS."` ORDER BY `show_type`, `sequence`";

	
	$res_shows = $db->get($sql_shows);
	$num_shows1 = $db->num_rows($res_shows);

	$sql_shows .= " LIMIT ".$offset.",".$limit;
	
	$res_shows = $db->get($sql_shows);
	$num_shows = $db->num_rows($res_shows);
	
	if($num_shows>0){
		//Array is populated
		$i=1;
		while($row_shows = $db->fetch_array($res_shows)){
			$show_array[$i]['show_id'] = $f->getValue($row_shows['show_id']);
			$show_array[$i]['headline'] = $f->getValue($row_shows['headline']);
			$show_array[$i]['image_path'] = $f->getValue($row_shows['image_path']);
			$show_array[$i]['location'] = $f->getValue($row_shows['location']);
			$show_array[$i]['city_state'] = $f->getValue($row_shows['city_state']);
			$show_array[$i]['link'] = $f->getValue($row_shows['link']);
			$show_array[$i]['summarized_dates'] = $f->getValue($row_shows['summarized_dates']);
			$show_array[$i]['list_dates'] = $f->getValue($row_shows['list_dates']);
			$show_array[$i]['from_date'] = $f->getValue($row_shows['from_date']);
			$show_array[$i]['to_date'] = $f->getValue($row_shows['to_date']);
			$show_array[$i]['summarize_time'] = $f->getValue($row_shows['summarize_time']);
			$show_array[$i]['show_type'] = $f->getValue($row_shows['show_type']);
			$i++;
		}
	}
	
	
	
$cnt = $_POST['cnt'];

?>
	<?php if($num_shows>0){?>
	
	<table width="671" border="0" align="right" cellpadding="0" cellspacing="0">
		<tr>
			<td>
			<table width="100%" border="0" cellpadding="0" cellspacing="0">
			<?php 
			$show_type_arr = array();

		  	for($i=1;$i<=count($show_array);$i++){
				if($cnt == 0){
					$bgcolor = "#f1f2f1";
					$cnt=1;
				}else{
					$bgcolor = "#e6e6e6";
					$cnt=0;
				}
				if($bgcolor == "#f1f2f1")
					$bgcolor1 = "#e6e6e6";
				else if($bgcolor == "#e6e6e6")
					$bgcolor1 = "#f1f2f1";
				
				//Image path
				$show_image="uploads/shows/".$show_array[$i]['image_path'];
				
				if($show_array[$i]['link']!=""){
					if($show_array[$i]['location']!="Name")
						$location = "<a href='".$show_array[$i]['link']."' target='_blank' style='color:#333333;text-decoration:none;'>".$f->getValue($show_array[$i]['location'])."</a>";
					else
						$location = "";
					if($show_array[$i]['city_state']!="City, State")
						$city_state = "<a href='".$show_array[$i]['link']."' target='_blank' style='color:#333333;text-decoration:none;'>".$f->getValue($show_array[$i]['city_state'])."</a>";
					else
						$city_state = "";
				}else{
					if($show_array[$i]['location']!="Name")
						$location = $f->getValue($show_array[$i]['location']);
					else
						$location = "";
					if($show_array[$i]['city_state']!="City, State")
						$city_state = $f->getValue($show_array[$i]['city_state']);
					else
						$city_state = "";
				}
					
				$headline = $f->getValue($show_array[$i]['headline']);

		  	  if(!in_array($show_array[$i]['show_type'],$show_type_arr)){
					$show_type_arr[$i]=$show_array[$i]['show_type'];
				
				if($show_type_arr[$i]=="1")
					$show_type = "LIVE";
				else
					$show_type = "PAST SHOWS";
				
		  ?>
		  <tr>
            <td height="36" align="center" valign="middle" bgcolor="<?php echo $bgcolor1;?>" class="style4"><?php echo $show_type;?></td>
          </tr>
		  <?php }?>
          <tr>
            <td align="center" valign="top"><table width="100%" border="0" align="center" cellpadding="0" cellspacing="0" bgcolor="<?php echo $bgcolor;?>" class="shows">
              <tr>
                <td width="18%" rowspan="9" align="left" valign="top"><img src="<?php echo $show_image;?>" height="97" width="84" alt="" /></td>
                <td colspan="2" align="left" valign="top">&nbsp;</td>
                <td width="47%" colspan="2" align="left" valign="top">&nbsp;</td>
              </tr>
              <tr>
                <td colspan="2" align="left" valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0">
				 <?php if($show_array[$i]['summarized_dates']=="Y"){?>
                  <tr>
                    <td width="40%" align="left"><?php echo date("D, n.j",strtotime($show_array[$i]['from_date']));?><br />
                      -<br /><?php echo date("D, n.j",strtotime($show_array[$i]['to_date']));?></td>
                    <td width="60%" align="left" valign="top"><?php echo strtoupper($show_array[$i]['summarize_time']);?></td>
                  </tr>
				  <?php }else if($show_array[$i]['list_dates']=="Y"){
				  	//Show dates are fetched
					$sql_show_dates = "SELECT * FROM `".TSD."` WHERE `show_id`='".$show_array[$i]['show_id']."' ORDER BY `show_date`";
					$res_show_dates = $db->get($sql_show_dates);
					while($row_show_dates = $db->fetch_array($res_show_dates)){
				  ?>
                  <tr>
                    <td width="40%" align="left"><?php echo date("D, n.j",strtotime($row_show_dates['show_date']));?></td>
                    <td width="60%" align="left"><?php echo strtoupper($row_show_dates['from_time']);?> <?php if($row_show_dates['to_time']!="") echo ",".strtoupper($row_show_dates['to_time']);?></td>
                  </tr>
				  <?php }
				  }
				  ?>
                  <tr>
                    <td width="40%">&nbsp;</td>
                    <td width="60%">&nbsp;</td>
                  </tr>
                </table></td>
                <td colspan="2" align="left" valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                  <tr>
                    <td width="52%" align="left"><?php echo $headline;?></td>
                    <td width="48%" align="left"><?php echo $location;?></td>
                  </tr>
                  <tr>
                    <td>&nbsp;</td>
                    <td align="left"><?php echo $city_state;?></td>
                  </tr>
                </table></td>
                </tr>
            </table></td>
          </tr>
		  <?php 
			}
		  ?>
			</table>	
			</td>
		  </tr>
        </table>
		<?php }
		 
			if($offset+$limit<$num_shows1){
		  ?>
		  	<div id="navigation" align="center">
			  <table border="0" cellpadding="0" cellspacing="0" width="100%">
			  <tr>
				<td align="center" valign="top"><img src="images/shows/morebg1.jpg" width="152" height="1" alt="" /></td>
			  </tr>
			  <tr>
				<td align="center" valign="top" class="style5"><a href="javascript:void(0);" onclick="__showMoreShows(<?php echo $limit;?>);">SEE MORE</a></td>
			  </tr>
			  <tr>
				<td align="center" valign="top"><a href="javascript:void(0);" onclick="__showMoreShows(<?php echo $limit;?>);"><img src="images/shows/more-button.jpg" width="154" height="24" alt="" /></a></td>
			  </tr>
			  </table>
			 </div>
	  <?php }?>!@#$%^&*()_+<?php echo $cnt;?>