<?php include("includes/config.inc.php");

	$page_id = 2;

	define("TC","tbl_clips",true);

	//Clips are fetched
	$cnt = 1;
	$sql_clips = "SELECT * FROM `".TC."`";
	$sql_clips .= " ORDER BY `sequence`";
	$res_clips = $db->get($sql_clips);
	while($row_clips = $db->fetch_array($res_clips)){
		$clips_arr[$cnt]['clip_id']=$f->getValue($row_clips['clip_id']);
		$clips_arr[$cnt]['name']=$f->getValue($row_clips['name']);
		$clips_arr[$cnt]['image_path']=$f->getValue($row_clips['image_path']);
		$clips_arr[$cnt]['video_type']=$f->getValue($row_clips['video_type']);
		$clips_arr[$cnt]['utube']=$f->getValue($row_clips['utube']);
		$clips_arr[$cnt]['file_path']=$f->getValue($row_clips['file_path']);
		$clips_arr[$cnt]['embed_code']=$f->getValue($row_clips['embed_code']);
		$clips_arr[$cnt]['custom_file']=$f->getValue($row_clips['custom_file']);
		$cnt++;
	}
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<?php include("js.css.inc.php");?>
<link rel="stylesheet" type="text/css" href="flowplayer/skin/minimalist.css">
<script type="text/javascript" src="flowplayer/flowplayer.min.js"></script>
<style type="text/css">
.flowplayer { width: 675px; height: 512px; }
</style>

</head>
<body>
<div class="social_media"><?php include("socialmedia.inc.php");?></div>
<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td align="center" valign="top">
<table width="1126" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td height="33" align="center" valign="top">&nbsp;</td>
      </tr>
  <tr>
    <td align="center" valign="top"><?php include("header.inc.php");?></td>
  </tr>
  <tr>
    <td align="center" valign="top"><table width="100%" border="0" align="center" cellpadding="0" cellspacing="0" class="clips_area">
      <tr>
        <td align="center" valign="middle" height="512">
		<?php if(count($clips_arr)>0){
			if($_GET['id']!="")
				$k = $_GET['id'];
			else
				$k = 1;
			if($clips_arr[$k]['video_type']=="utube"){
				include('includes/youtube.class.php');
				$script = $clips_arr[$k]['utube'];
				$youtube = new YoutubeParser;
				$youtube->set('source',$script);
				$youtube->set('unique',true);
				$youtube->width='675';
				$youtube->height='512';
				$video = $youtube->getall();
				echo $video[0]['embed'];
			}else if($clips_arr[$k]['video_type']=="embed_code"){
				echo $clips_arr[$k]['embed_code'];
			}else if($clips_arr[$k]['video_type']=="file" || $clips_arr[$k]['video_type']=="custom_file"){
				
				if($clips_arr[$k]['file_path']!=""){
					$file = "uploads/videos/".$clips_arr[$k]['file_path'];
					$ext = explode('.',$clips_arr[$k]['file_path']);
				}
				else{
					$file = "uploads/videos/".$clips_arr[$k]['custom_file'];
					$ext = explode('.',$clips_arr[$k]['custom_file']);
				}
																							
				switch(end($ext)) {
					case "mp4":
						$video_type = "video/mp4";
						break;
					case "mov":
						$video_type = "video/mp4";
						break;
					case "wmv":
						$video_type = "video/x-ms-wmv";
						break;
					case "avi":
						$video_type = "video/x-msvideo";
						break;
					case "flv":
						$video_type = "video/x-flv";
						break;
					case "m4v":
						$video_type = "video/mp4";
						break;			
				}
				$data_key = '$289122895653393';
				//$data_key = '$660596421855171';
				?>
				<div class="flowplayer" data-swf="flowplayer/flowplayer.swf" data-key="<?php echo $data_key;?>" data-ratio="0.6000">
					<video>
						<source type="<?php echo $video_type?>" src="<?php echo $file;?>">
					</video>
				 </div>
				<?php
			}
		?>
		<?php }?>
		</td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td height="30" align="center" valign="top">&nbsp;</td>
  </tr>
  <tr>
    <td align="center" valign="top"><table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
	  <?php if(count($clips_arr)>1){
	  ?>
      <tr>
	    <?php 
			$j=1;
		for($i=1;$i<$cnt;$i++){
			if($j>4){
				echo "</tr><tr><td colspan='7' height='30'>&nbsp;</td></tr><tr>";
				$j=1;
			}
			if($j!=1){
				$width = "5%";
				$j++;
			}else{
				$width = "6%";
				$j++;
			}
			
			$thumb_image = "uploads/videos/image/".$clips_arr[$i]['image_path'];
		?>
        <td width="21%" align="center" valign="top"><table width="100%" border="0" align="left" cellpadding="0" cellspacing="0" class="clips_block">
        <tr>
            <td height="10" align="center" valign="top"></td>
          </tr>
          <tr>
            <td height="105" align="center" valign="top"><a href="clips.php?id=<?php echo $i;?>"><img src="<?php echo $thumb_image;?>" width="202" height="105" alt="" border="0" /></a></td>
          </tr>
          <tr>
            <td height="26" align="center" valign="middle"><a href="clips.php?id=<?php echo $i;?>" style="color:#333333; text-decoration:none"><?php echo $clips_arr[$i]['name'];?></a></td>
          </tr>
        </table></td>
		<?php if($j<=4){?>
        <td width="<?php echo $width;?>" align="center" valign="top">&nbsp;</td>
		<?php 
			}
		}?>
      </tr>
      <tr>
        <td height="30" align="center" valign="top">&nbsp;</td>
        <td align="center" valign="top">&nbsp;</td>
        <td align="center" valign="top">&nbsp;</td>
        <td align="center" valign="top">&nbsp;</td>
        <td align="center" valign="top">&nbsp;</td>
        <td align="center" valign="top">&nbsp;</td>
        <td align="center" valign="top">&nbsp;</td>
      </tr>
	  <?php }?>
    </table></td>
  </tr>
  <tr>
    <td align="center" valign="top">&nbsp;</td>
  </tr>
  <tr>
    <td align="center" valign="top">&nbsp;</td>
  </tr>
  <tr>
    <td align="center" valign="top"><img src="images/shows/devide-line.jpg" width="1046" height="2" alt="" /></td>
  </tr>
  <tr>
    <td align="center" valign="top">&nbsp;</td>
  </tr>
  <tr>
    <td align="center" valign="top">&nbsp;</td>
  </tr>
  <tr>
    <td align="center" valign="top"><?php include("footer.inc.php");?></td>
  </tr>
  <tr>
    <td align="center" valign="top">&nbsp;</td>
  </tr>
  <tr>
    <td align="center" valign="top">&nbsp;</td>
  </tr>
  <tr>
    <td align="center" valign="top">&nbsp;</td>
  </tr>
    </table></td>
  </tr>
</table>
    </td>
  </tr>
</table>
</body>
</html>
