<?php include("includes/config.inc.php");

	$page_id = 6;

	if(isset($_POST['btnSubmit'])){
		
		extract($_POST);
				
		// Generating Mail Content
		$mail_content = "<font style=\"font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 13px;\">"; 
		$mail_content.= "Dear Administrator,<br /><br />";
		$mail_content.= "This user has joined your mailing list:<br /><br />";
		$mail_content.= "Name:".$name."<br /><br />";
		$mail_content.= "Email:".$email."<br /><br />";
		$mail_content.= "Your City:".$city."<br /><br />";
		$mail_content.= "</font>";
		// Sending Mail
		
		$settings = $m->getSettings();
		

		$objMail = new PHPMailer();
		$objMail->From = $email;
		$objMail->FromName = $name;
		$objMail->Subject = "ErinkFoley:Join Mailing List";
		if($settings['smtp']=='Yes'):
			$objMail->IsSMTP();
			$objMail->Host = $f->getValue($settings['smtp_hostname']);
			$objMail->SMTPAuth = true;
			$objMail->Username = $f->getValue($settings['smtp_username']);
			$objMail->Password = $f->getValue($settings['smtp_password']);
		endif;
		$objMail->IsHTML(true);
		$objMail->Body = $mail_content;
		$objMail->CharSet = 'UTF-8';
		$objMail->AddAddress($f->getValue($settings['your_email_address']),$f->getValue($settings['your_name']));
		if($objMail->Send()){
			$msg = "<span style='font-size:12px;'>Thanks for registration!</b></span>"; 
		}else
			$msg = "<span class='error' style='font-size:12px;'>Sorry registration failed! Please try again later!</b></span>"; 
	}
	
	if(isset($_POST['btnRegister'])){
		
		extract($_POST);
				
		// Generating Mail Content
		$mail_content = "<font style=\"font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 13px;\">"; 
		$mail_content.= "Dear Administrator,<br /><br />";
		$mail_content.= "This user has asked for PR info:<br /><br />";
		$mail_content.= "Name:".$name."<br /><br />";
		$mail_content.= "Title:".$title."<br /><br />";
		$mail_content.= "Email:".$email."<br /><br />";
		$mail_content.= "Company:".$company."<br /><br />";
		$mail_content.= "Phone:".$phone."<br /><br />";
		$mail_content.= "Message:".$message."<br /><br />";
		$mail_content.= "</font>";
		// Sending Mail
		
		$settings = $m->getSettings();
		

		$objMail = new PHPMailer();
		$objMail->From = $email;
		$objMail->FromName = $name;
		$objMail->Subject = "ErinkFoley:Request for PR info";
		if($settings['smtp']=='Yes'):
			$objMail->IsSMTP();
			$objMail->Host = $f->getValue($settings['smtp_hostname']);
			$objMail->SMTPAuth = true;
			$objMail->Username = $f->getValue($settings['smtp_username']);
			$objMail->Password = $f->getValue($settings['smtp_password']);
		endif;
		$objMail->IsHTML(true);
		$objMail->Body = $mail_content;
		$objMail->CharSet = 'UTF-8';
		$objMail->AddAddress($f->getValue($settings['your_email_address']),$f->getValue($settings['your_name']));
		if($objMail->Send()){
			$msg1 = "<span style='font-size:12px;'>Thanks for your request! We will contact you soon!</b></span>"; 
		}else
			$msg1 = "<span class='error' style='font-size:12px;'>Sorry mail delivery failed! Please try again later!</b></span>"; 
	}

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<?php include("js.css.inc.php");?>
<script type="text/javascript">
$(document).ready(function(){
	$("#frmcontact").validate({

			errorPlacement: function(error, element){

				element.parent().parent().children().children(".errorContainer").append(error);

			}

	});
	$("#frmregister").validate({

			errorPlacement: function(error, element){

				element.parent().parent().children().children(".errorContainer").append(error);

			}

	});
});
</script>
<style type="text/css">
	.error{color:#FF0000};
</style>
</head>
<body>
<div class="social_media"><?php include("socialmedia.inc.php");?></div>
<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td align="center" valign="top">
<table width="1126" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td height="33" align="center" valign="top">&nbsp;</td>
      </tr>
  <tr>
    <td align="center" valign="top"><?php include("header.inc.php");?></td>
  </tr>
  <tr>
   <td align="center" valign="top"><table width="910" border="0" align="center" cellpadding="0" cellspacing="0" style="font-size:12px; line-height:16px;">
      <tr>
        <td width="56" align="left" valign="top" class="style8">&nbsp;</td>
        <td width="219" align="left" valign="top" class="style8">&nbsp;</td>
        <td width="255" align="left" valign="top" class="style8">&nbsp;</td>
        <td width="258" align="left" valign="top" class="style8">&nbsp;</td>
        <td width="122" align="left" valign="top" class="style8">&nbsp;</td>
      </tr>
      <tr>
        <td width="56" align="left" valign="top" class="style8">&nbsp;</td>
        <td width="219" align="left" valign="top" class="style8">MANAGEMENT</td>
        <td width="255" align="left" valign="top" class="style8">PERSONAL<br />APPEARANCE</td>
        <td width="258" align="left" valign="top" class="style8">COMMERCIAL</td>
        <td width="122" align="left" valign="top" class="style8">INFO</td>
      </tr>
      <tr>
        <td align="left" valign="top">&nbsp;</td>
        <td height="20" align="left" valign="top">&nbsp;</td>
        <td align="left" valign="top">&nbsp;</td>
        <td align="left" valign="top">&nbsp;</td>
        <td align="left" valign="top">&nbsp;</td>
      </tr>
      <tr>
        <td align="left" valign="top">&nbsp;</td>
        <td align="left" valign="top">Jodi Lieberman<br />
JLC Entertainment Group<br /><br />
LA Office: 323.380.5569<br />
NYC Office: 917.696.0069<br /><br />
Cell: 310.775.7040<br />
jodi.lieberman@jlcentertainmentgroup.com<br />
<br />
        <td align="left" valign="top">
Natalie Heflin<br />
NAT Productions<br /><br />
415-3-COMEDY<br />
natalie@natbookings.com
<br />
</td>
        <td align="left" valign="top">
Laura Soo Hoo<br />
Reign Agency<br /><br />
400 S Beverly Dr #250<br />Beverly Hills, CA 90212<br />
<br />
t 310.396.6462 <br />
f 310.396.6415 <br />
<br />
</td>
        <td align="left" valign="top">
Request PR info:<br />
<a href="mailto:pr@erinkfoley.com" style="color:#000; text-decoration:none">pr@erinkfoley.com</a><br />
<br />
General info:<br />
<a href="mailto:info@erinkfoley.com" style="color:#000; text-decoration:none">info@erinkfoley.com</a><br />
<br />
Say “hi!” to Erin:<br />
<a href="mailto:erin@erinkfoley.com" style="color:#000; text-decoration:none">erin@erinkfoley.com</a></td>
      </tr>
      <tr>
        <td align="left" valign="top">&nbsp;</td>
        <td align="left" valign="top">&nbsp;</td>
        <td align="left" valign="top">&nbsp;</td>
        <td align="left" valign="top">&nbsp;</td>
        <td align="left" valign="top">&nbsp;</td>
      </tr>
      <tr>
        <td align="left" valign="top">&nbsp;</td>
        <td align="left" valign="top">&nbsp;</td>
        <td align="left" valign="top">&nbsp;</td>
        <td align="left" valign="top">&nbsp;</td>
        <td align="left" valign="top">&nbsp;</td>
      </tr>
      <tr>
        <td align="left" valign="top">&nbsp;</td>
        <td align="left" valign="top">&nbsp;</td>
        <td align="left" valign="top">&nbsp;</td>
        <td align="left" valign="top">&nbsp;</td>
        <td align="left" valign="top">&nbsp;</td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td align="left" valign="top" class="style8" style="padding-left:90px;">JOIN THE MAILING LIST!</td>
  </tr>
   <tr>
    <td align="center" valign="top">
	<form name="frmcontact" id="frmcontact" action="<?php echo $_SERVER['PHP_SELF'];?>" method="post">
	<table width="910" border="0" align="center" cellpadding="0" cellspacing="0" class="form_area">
      <tr>
        <td width="56" align="left" valign="top">&nbsp;</td>
        <td width="90" align="left" valign="top">&nbsp;</td>
        <td width="764" align="left" valign="top">&nbsp;</td>
        </tr>
		<?php if($msg!=""){?>
		<tr>
        <td width="56" align="left" valign="top">&nbsp;</td>
        <td height="40" colspan="2" align="left" valign="top"><?php echo $msg;?></td>
        </tr>
		<?php }?>
      <tr>
        <td align="left" valign="middle">&nbsp;</td>
        <td height="23" align="left" valign="middle">NAME<span class="style9">*</span></td>
        <td align="left" valign="middle"><input type="text" name="name" id="name" class="input1 required" value="" /><br /><span class="errorContainer"></span></td>
        </tr>
      <tr>
        <td align="left" valign="middle">&nbsp;</td>
        <td height="23" align="left" valign="middle">EMAIL<span class="style9">*</span></td>
        <td align="left" valign="middle"><input type="text" name="email" id="email" class="input1 required email" /><br /><span class="errorContainer"></span></td>
        </tr>
      <tr>
        <td align="left" valign="middle">&nbsp;</td>
        <td height="23" align="left" valign="middle">YOUR CITY<span class="style9">*</span></td>
        <td align="left" valign="middle"><input type="text" name="city" id="city" class="input1 required" /><br /><span class="errorContainer"></span></td>
        </tr>
      <tr>
        <td align="left" valign="middle">&nbsp;</td>
        <td height="23" align="left" valign="middle">&nbsp;</td>
        <td align="left" valign="middle"><input type="submit" name="btnSubmit" id="btnSubmit" value="SUBMIT" class="submit" /></td>
        </tr>
      <tr>
        <td align="left" valign="middle">&nbsp;</td>
        <td align="left" valign="middle">&nbsp;</td>
        <td align="left" valign="middle">&nbsp;</td>
        </tr>
    </table>
	</form>
	</td>
  </tr>
   <tr>
    <td align="center" valign="top">&nbsp;</td>
  </tr>
   <tr>
    <td align="left" valign="top" class="style8" style="padding-left:90px;">REQUEST PR INFO</td>
  </tr>
   <tr>
    <td align="center" valign="top">
	<form name="frmregister" id="frmregister" action="<?php echo $_SERVER['PHP_SELF'];?>" method="post">
	<table width="910" border="0" align="center" cellpadding="0" cellspacing="0" class="form_area">
      <tr>
        <td width="56" align="left" valign="top">&nbsp;</td>
        <td width="90" align="left" valign="top">&nbsp;</td>
        <td width="764" align="left" valign="top">&nbsp;</td>
      </tr>
	    <?php if($msg!=""){?>
		<tr>
        <td width="56" align="left" valign="top">&nbsp;</td>
        <td height="40" colspan="2" align="left" valign="top"><?php echo $msg;?></td>
        </tr>
		<?php }?>

      <tr>
        <td align="left" valign="middle">&nbsp;</td>
        <td height="23" align="left" valign="middle">NAME<span class="style9">*</span></td>
        <td align="left" valign="middle"><input type="text" name="name" id="name" class="input1 required" /><br /><span class="errorContainer"></span></td>
      </tr>
      <tr>
        <td align="left" valign="middle">&nbsp;</td>
        <td height="23" align="left" valign="middle">TITLE</td>
        <td align="left" valign="middle"><input type="text" name="title" id="title" class="input1" /></td>
      </tr>
      <tr>
        <td align="left" valign="middle">&nbsp;</td>
        <td height="23" align="left" valign="middle">COMPANY</td>
        <td align="left" valign="middle"><input type="text" name="company" id="company" class="input1" /></td>
      </tr>
      <tr>
        <td align="left" valign="middle">&nbsp;</td>
        <td height="23" align="left" valign="middle">EMAIL<span class="style9">*</span></td>
        <td align="left" valign="middle"><input type="text" name="email" id="email" class="input1 required email" /><br /><span class="errorContainer"></span></td>
      </tr>
      <tr>
        <td align="left" valign="middle">&nbsp;</td>
        <td height="23" align="left" valign="middle">PHONE</td>
        <td align="left" valign="middle"><input type="text" name="phone" id="phone" class="input1" /></td>
      </tr>
      <tr>
        <td height="3" colspan="3" align="left" valign="top"></td>
        </tr>
      <tr>
        <td align="left" valign="top">&nbsp;</td>
        <td height="23" align="left" valign="top">MESSAGE<span class="style9">*</span></td>
        <td align="left" valign="middle"><textarea name="message" id="message" cols="45" rows="5" class="input2 required"></textarea><br /><span class="errorContainer"></span></td>
      </tr>
       <tr>
        <td height="3" colspan="3" align="left" valign="top"></td>
        </tr>

      <tr>
        <td align="left" valign="middle">&nbsp;</td>
        <td height="23" align="left" valign="middle">&nbsp;</td>
        <td align="left" valign="middle"><input type="submit" name="btnRegister" id="btnRegister" value="SUBMIT" class="submit" /></td>
      </tr>
    </table>
	</form>
	</td>
  </tr>
   <tr>
    <td align="center" valign="top">&nbsp;</td>
  </tr>
  <tr>
    <td align="center" valign="top">&nbsp;</td>
  </tr>
  <tr>
    <td align="center" valign="top"><img src="images/shows/devide-line.jpg" width="1046" height="2" alt="" /></td>
  </tr>
  <tr>
    <td align="center" valign="top">&nbsp;</td>
  </tr>
  <tr>
    <td align="center" valign="top">&nbsp;</td>
  </tr>
  <tr>
    <td align="center" valign="top"><?php include("footer.inc.php");?></td>
  </tr>
  <tr>
    <td align="center" valign="top">&nbsp;</td>
  </tr>
  <tr>
    <td align="center" valign="top">&nbsp;</td>
  </tr>
  <tr>
    <td align="center" valign="top">&nbsp;</td>
  </tr>
    </table></td>
  </tr>
</table>
    </td>
  </tr>
</table>
</body>
</html>
