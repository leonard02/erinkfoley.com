<?php include("includes/config.inc.php");

define("TS","tbl_shows",true);
define("TSD","tbl_show_dates",true);

$tot_count = 9;

//Shows info are fetched
$sql_shows = "SELECT * FROM `".TS."` ORDER BY `sequence` DESC";
$res_shows = $db->get($sql_shows);
$num_shows = $db->num_rows($res_shows);

$sql_shows .= " LIMIT 0,".$tot_count;
$res_shows = $db->get($sql_shows);

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<?php include("js.css.inc.php");?>
<script type="text/javascript">
	function __showMoreShows(limit){
			var offset = Number($("#hidoffset").val());
			var cnt = Number($("#cnt").val());
			var data = "limit="+limit+"&offset="+offset+"&action=showmoreshows&cnt="+cnt;
			$.ajax({
				type: 'POST',
				url: 'ajax.php',
				data: data,
				dataType: 'html',
				success: function(data){
					var dataarray = Array();
					dataarray = data.split('!@#$%^&*()_+');
					data = dataarray[0];
					cnt = dataarray[1];
					offset = Number(offset)+Number(limit);
					$("#hidoffset").val(Number(offset));
					$("#cnt").val(Number(cnt));
					$('#showlist').append(data);
				}
			});

	}
</script>
</head>
<body>
<div class="social_media"><?php include("socialmedia.inc.php");?></div>
<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td align="center" valign="top">
<table width="1126" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td height="33" align="center" valign="top">&nbsp;</td>
      </tr>
  <tr>
    <td align="center" valign="top"><?php include("header.inc.php");?></td>
  </tr>
  <tr>
    <td align="center" valign="top">
	<table width="100%" border="0" align="right" cellpadding="0" cellspacing="0">
      <tr>
        <td height="36" align="center" valign="middle" bgcolor="#e6e6e6" class="style4">LIVE</td>
      </tr>
	  <tr>
	  	<td>
		<div id="showlist">
	  	<table width="100%" border="0" cellpadding="0" cellspacing="0">

		  <?php if($db->num_rows($res_shows)>0){
		  	$cnt = 0;
		  	while($row_shows = $db->fetch_array($res_shows)){
				if($cnt == 0){
					$bgcolor = "#f1f2f1";
					$cnt++;
				}else{
					$bgcolor = "#e6e6e6";
					$cnt--;
				}
				
				//Image path
				$show_image="uploads/shows/".$row_shows['image_path'];
				
				if($row_shows['link']!=""){
					$location = "<a href='".$row_shows['link']."' target='_blank' style='color:#333333;text-decoration:none;'>".$f->getValue($row_shows['location'])."</a>";
					$city_state = "<a href='".$row_shows['link']."' target='_blank' style='color:#333333;text-decoration:none;'>".$f->getValue($row_shows['city_state'])."</a>";
				}else{
					$location = $f->getValue($row_shows['location']);
					$city_state = $f->getValue($row_shows['city_state']);
				}
					
				$headline = $f->getValue($row_shows['headline']);
		  ?>
          <tr>
            <td align="center" valign="top"><table width="100%" border="0" align="center" cellpadding="0" cellspacing="0" bgcolor="<?php echo $bgcolor;?>" class="shows">
              <tr>
                <td width="18%" rowspan="9" align="left" valign="top"><img src="<?php echo $show_image;?>" height="97" width="84" alt="" /></td>
                <td colspan="2" align="left" valign="top">&nbsp;</td>
                <td colspan="2" align="left" valign="top">&nbsp;</td>
              </tr>
              <tr>
                <td colspan="2" align="left" valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0">
				 <?php if($row_shows['summarized_dates']=="Y"){?>
                  <tr>
                    <td width="40%"><?php echo date("D, n.j",strtotime($row_shows['from_date']));?><br />
                      -<br /><?php echo date("D, n.j",strtotime($row_shows['to_date']));?></td>
                    <td width="60%"><?php echo strtoupper($row_shows['summarize_time']);?></td>
                  </tr>
				  <?php }else if($row_shows['list_dates']=="Y"){
				  	//Show dates are fetched
					$sql_show_dates = "SELECT * FROM `".TSD."` WHERE `show_id`='".$row_shows['show_id']."' ORDER BY `show_date`";
					$res_show_dates = $db->get($sql_show_dates);
					while($row_show_dates = $db->fetch_array($res_show_dates)){
				  ?>
                  <tr>
                    <td width="40%"><?php echo date("D, n.j",strtotime($row_show_dates['show_date']));?></td>
                    <td width="60%"><?php echo strtoupper($row_show_dates['from_time']);?> <?php if($row_show_dates['to_time']!="") echo ",".strtoupper($row_show_dates['to_time']);?></td>
                  </tr>
				  <?php }
				  }
				  ?>
                  <tr>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                  </tr>
                </table></td>
                <td colspan="2" align="left" valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                  <tr>
                    <td width="52%"><?php echo $headline;?></td>
                    <td width="48%" align="left"><?php echo $location;?></td>
                  </tr>
                  <tr>
                    <td>&nbsp;</td>
                    <td><?php echo $city_state;?></td>
                  </tr>
                </table></td>
                </tr>
              <tr>
                <td width="21%" align="left" valign="top">&nbsp;</td>
                <td width="18%" align="left" valign="top">&nbsp;</td>
                <td width="23%" align="left" valign="top">&nbsp;</td>
                <td width="20%" align="left" valign="top">&nbsp;</td>
              </tr>
              <tr>
                <td align="left" valign="top">&nbsp;</td>
                <td align="left" valign="top">&nbsp;</td>
                <td align="left" valign="top">&nbsp;</td>
                <td align="left" valign="top">&nbsp;</td>
              </tr>
              <tr>
                <td align="left" valign="top">&nbsp;</td>
                <td align="left" valign="top">&nbsp;</td>
                <td align="left" valign="top">&nbsp;</td>
                <td align="left" valign="top">&nbsp;</td>
              </tr>
              <tr>
                <td align="left" valign="top"></td>
                <td align="left" valign="top"></td>
                <td align="left" valign="top"></td>
                <td align="left" valign="top"></td>
              </tr>
            </table></td>
          </tr>
		  <?php 
		  	}
		  }
		  ?>
			</table>	
		</div>
	  	</td>
	  </tr>
	  <?php
	  if($num_shows>$tot_count){?>
	  <tr>
		  <td>
		  	<div id="navigation" align="center">
			  <table border="0" cellpadding="0" cellspacing="0" width="100%">
			  <tr>
				<td align="center" valign="top"><img src="images/shows/morebg1.jpg" width="152" height="1" alt="" /></td>
			  </tr>
			  <tr>
				<td align="center" valign="top" class="style5"><a href="javascript:void(0);" onclick="__showMoreShows(<?php echo $tot_count;?>);">SEE MORE</a></td>
			  </tr>
			  <tr>
				<td align="center" valign="top"><a href="javascript:void(0);" onclick="__showMoreShows(<?php echo $tot_count;?>);"><img src="images/shows/more-button.jpg" width="154" height="24" alt="" /></a></td>
			  </tr>
			  </table>
			 </div>
		  </td>
	  </tr>
	  <?php }?>
    </table>
	</td>
  </tr>
  <tr>
    <td align="center" valign="top">&nbsp;</td>
  </tr>
  <tr>
    <td align="center" valign="top">&nbsp;</td>
  </tr>
  <tr>
    <td align="center" valign="top"><img src="images/shows/devide-line.jpg" width="1046" height="2" alt="" /></td>
  </tr>
  <tr>
    <td align="center" valign="top">&nbsp;</td>
  </tr>
  <tr>
    <td align="center" valign="top">&nbsp;</td>
  </tr>
  <tr>
    <td align="center" valign="top"><?php include("footer.inc.php");?></td>
  </tr>
  <tr>
    <td align="center" valign="top">&nbsp;</td>
  </tr>
  <tr>
    <td align="center" valign="top">&nbsp;</td>
  </tr>
  <tr>
    <td align="center" valign="top">&nbsp;</td>
  </tr>
    </table></td>
  </tr>
</table>
    </td>
  </tr>
</table>
<input type="hidden" name="hidoffset" id="hidoffset" value="<?php echo $tot_count;?>" />
<input type="hidden" name="cnt" id="cnt" value="<?php echo $cnt;?>" />

</body>
</html>
