<?php
	define("TF","tbl_footer",true);
	
	//All the footers are fetched
	$sql_footer = "SELECT * FROM `".TF."` ORDER BY `footer_id` DESC";
	$res_footer = $db->get($sql_footer);
	$records = $db->num_rows($res_footer);

?>
<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
      <tr>
	  	<?php
		if($db->num_rows($res_footer)>0){
		$i=1;
		$cnt = 1;
		while($row_footer = $db->fetch_array($res_footer)){
			$img_path = "uploads/footers/".$row_footer['image_path'];
			if($i==1)
				$width = "34%";
			else
				$width = "33%";
			
			if($row_footer['image_link']!=""){
				$img_link = '<a href="'.$row_footer['image_link'].'" target="_blank" style="color:#000;text-decoration:none;"><img src="'.$img_path.'" width="132" height="135" alt="" /></a>';
				$name = '<a href="'.$row_footer['image_link'].'" target="_blank" style="color:#000;text-decoration:none;">'.$f->getValue($row_footer['name']).'</a>';
				if($row_footer['subheadline']!="")
					$subheadline = '<a href="'.$row_footer['image_link'].'" target="_blank" style="color:#000;text-decoration:none;">'.$f->getValue($row_footer['subheadline']).'</a>';
				else
					$subheadline = "";
			}else{
				$img_link = '<img src="'.$img_path.'" width="132" height="135" alt="" />';
				$name = $f->getValue($row_footer['name']);
				if($row_footer['subheadline']!="")
					$subheadline = $f->getValue($row_footer['subheadline']);
				else
					$subheadline = "";
			}
				
			if($row_footer['text_link']!=""){
				$footer_text = '<a href="'.$row_footer['text_link'].'" target="_blank" style="color:#000;text-decoration:none;">'.$f->getValue($row_footer['footer_text']).'</a>';
			}else
				$footer_text = $f->getValue($row_footer['footer_text']);
			
			if($row_footer['additional_text_link']!=""){
				$additional_text = '<a href="'.$row_footer['additional_text_link'].'" target="_blank" style="color:#000;text-decoration:none;">'.$f->getValue($row_footer['additional_text']).'</a>';
			}else
				$additional_text = $f->getValue($row_footer['additional_text']);
			
				
			
		?>
        <td width="<?php echo $width;?>" align="left" valign="top"><table width="355" border="0" align="left" cellpadding="0" cellspacing="0" class="shows_block">
          <tr>
            <td width="177" rowspan="10" align="left" valign="middle"><?php echo $img_link;?></td>
            <td width="178" height="29" align="left" valign="top">&nbsp;</td>
          </tr>
          <tr>
            <td align="left" valign="top" class="style6"><?php echo $name;?></td>
          </tr>
		  <?php if($subheadline!=""){?>
          <tr>
            <td align="left" valign="top" style="padding-right:10px;"><?php echo $subheadline;?></td>
          </tr>
		  <?php }?>
          <tr>
            <td align="left" valign="top">&nbsp;</td>
          </tr>
		  <tr>
            <td align="left" valign="top" style="padding-right:10px;"><?php echo $footer_text;?></td>
          </tr>
          <tr>
            <td align="left" valign="top" style="padding-right:10px;"><?php echo $additional_text;?></td>
          </tr>
          <tr>
            <td align="left" valign="top">&nbsp;</td>
          </tr>
          <tr>
            <td align="left" valign="top">&nbsp;</td>
          </tr>
          <tr>
            <td align="left" valign="top">&nbsp;</td>
          </tr>
        </table></td>
		<?php 
			}
		}
		?>
      </tr>
    </table>