<?php include("includes/config.inc.php");

	$page_id = 4;

	define("TGAL","tbl_gallery",true);

	//Gallery images are fetched
	$sql_gal = "SELECT * FROM `".TGAL."` ORDER BY `sequence`";
	$res_gal = $db->get($sql_gal);
	
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<?php include("js.css.inc.php");?>
<link rel="stylesheet" href="css/style.css" />
</head>
<body>
<ul id="slideshow" style="display:none;">
		<?php 
		while($row_gal = $db->fetch_array($res_gal)){
			$large_image = "uploads/gallery/".$row_gal['main_image'];
			$thumb_image = "uploads/gallery/thumbnail/".$row_gal['thumbnail_image'];
			
		?>
		<li>
			<h3></h3>
			<span><?php echo $large_image;?></span>
			<p></p>
			<a href="#"><img src="<?php echo $thumb_image;?>" height="55" width="55" /></a>
		</li>
		<?php }?>
	</ul>

<div class="social_media"><?php include("socialmedia.inc.php");?></div>
<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td align="center" valign="top">
<table width="1126" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td height="33" align="center" valign="top">&nbsp;</td>
      </tr>
  <tr>
    <td align="center" valign="top"><?php include("header.inc.php");?></td>
  </tr>
  <tr>
    <td align="center" valign="top"><table width="1126" border="0" align="center" cellpadding="0" cellspacing="0">
      <tr>
        <td width="1124" height="511" align="center" valign="middle">
			<div id="wrapper">		
					<div id="fullsize" class="clips_area">			
						<div id="imgprev" class="imgnav" title="Previous Image"><img src="images/press2/left.png" alt="" /></div>
						<div id="imglink"></div>
						<div id="imgnext" class="imgnav" title="Next Image"><img src="images/press2/right.png" alt="" /></div>
						<div id="image"></div>
					</div>
					<div id="thumbnails">
						<div id="slideleft" title="Slide Left"><img src="images/gallery/left_button.jpg" width="23" height="47" alt="" style="padding:0px;" /></div>
						<div id="slidearea">
							<div id="slider"></div>
						</div>
						<div id="slideright" title="Slide Right"><img src="images/gallery/right_button.jpg" width="23" height="47" alt="" style="padding:0px;" /></div>
					</div>
			</div>
			
		</td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td align="center" valign="top">&nbsp;</td>
  </tr>
    <tr>
    <td align="center" valign="top">&nbsp;</td>
  </tr>
  <tr>
    <td align="center" valign="top">&nbsp;</td>
  </tr>
  <tr>
    <td align="center" valign="top"><img src="images/shows/devide-line.jpg" width="1046" height="2" alt="" /></td>
  </tr>
  <tr>
    <td align="center" valign="top">&nbsp;</td>
  </tr>
  <tr>
    <td align="center" valign="top">&nbsp;</td>
  </tr>
  <tr>
    <td align="center" valign="top"><?php include("footer.inc.php");?></td>
  </tr>
  <tr>
    <td align="center" valign="top">&nbsp;</td>
  </tr>
  <tr>
    <td align="center" valign="top">&nbsp;</td>
  </tr>
  <tr>
    <td align="center" valign="top">&nbsp;</td>
  </tr>
    </table>
	</td>
  </tr>
</table>
    </td>
  </tr>
</table>
<script type="text/javascript" src="js/compressed.js"></script>
<script type="text/javascript">
	$('slideshow').style.display='none';
	$('wrapper').style.display='block';
	var slideshow=new TINY.slideshow("slideshow");
	window.onload=function(){
		slideshow.auto=true;
		slideshow.speed=5;
		//slideshow.link="linkhover";
		//slideshow.info="information";
		slideshow.thumbs="slider";
		slideshow.left="slideleft";
		slideshow.right="slideright";
		slideshow.scrollSpeed=5;
		slideshow.spacing=5;
		//slideshow.active="#000";
		slideshow.init("slideshow","image","imgprev","imgnext","imglink");
	}
</script>
</body>
</html>
