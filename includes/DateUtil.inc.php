<?php	 

class DateUtil{

		public function today(){

			//return @date('Y-m-d H:i:s');

			return @date('Y-m-d');

		}

		

		public function today_daytime(){

			return @date('Y-m-d H:i:s');

		}

		

		public function getDateByTime($timestamp){

			//return @date('Y-m-d H:i:s');

			//return @date('Y-m-d');

			$hour = @date("H", $timestamp);

			$minute = @date("i", $timestamp);

			$second = @date("s", $timestamp);

		

			$day = @date("d", $timestamp); 

			$month = @date("m", $timestamp);

			$year = @date("Y", $timestamp);

			

			$date = @date("Y-m-d H:i:s", mktime ($hour,$minute,$second,$month,$day,$year));

			return $date; 

		}

		

		public function unixTm($strDT){ 

			$arrDT = explode(" ", $strDT); 

			$arrD = explode("-", $arrDT[0]); 

			$arrT = explode(":", $arrDT[1]); 

			return @mktime($arrT[0], $arrT[1], $arrT[2], $arrD[1], $arrD[2], $arrD[0]); 

		} 

		

		public function convertType_mmddyyyy($date1){

			// Coverts a date into "Sep 13 2004" format

			$timestamp = @strtotime($date1);

			$hour = @date("H", $timestamp);

			$minute = @date("i", $timestamp);

			$second = @date("s", $timestamp);

			

			$day = @date("d", $timestamp); 

			$month = @date("m", $timestamp);

			$year = @date("Y", $timestamp);

			

			$date1 = @date("M d, Y", mktime ($hour,$minute,$second,$month,$day,$year));

			return $date1; 

		}



		public function dateDiff($date1,$date2, $opt=1){
		

			// This function returns difference between 2 dates

			// it accepts 3 arguments viz., End date, Start Date & Format Option in which it will print

			// the result

			$dt2=$this->unixTm($date2); 

			$dt1=$this->unixTm($date1); 

			$r =  $dt1 - $dt2; 

			$dd=floor($r / 86400); 

			//if ($dd<=9) $dd="0".$dd; 

			$r=$r % 86400; 

			$hh=floor($r/3600); 

			//if ($hh<=9) $hh="0".$hh; 

			$r=$r % 3600; 

			$mm=floor($r/60); 

			if ($mm<=9) $mm="0".$mm; 

			$r=$r % 60; 

			$ss=$r; 

			if ($ss<=9) $ss="0".$ss;

			//if only no. of days

			if($opt==1)

				$resd="$dd" ;

			//if only day + hours

			elseif($opt==2)

				$resd="$dd day, $hh hour+" ;			

			//if only day + hours + min

			elseif($opt==3){

				$resd="$dd day, $hh hour, $mm min+";			

				/*if($dd > 0 && $hh > 0 && $mm > 0){

					$resd="$dd day, $hh hour, $mm min+";

				}elseif($dd <= 0 && $hh > 0 && $mm > 0){

					$resd="$hh hour, $mm min+";

				}elseif($dd <= 0 && $hh <= 0 && $mm > 0){

					$resd="$mm min+";

				}*/

			}

			elseif($opt==4)

				$resd=$ss;

			

			return $resd;

		} 

		

		

		public function hourDifference($date1, $date2){

			// This function returns difference between 2 dates

			// it accepts 3 arguments viz., End date, Start Date & Format Option in which it will print

			// the result

			$dt2= $this->unixTm($date2); 

			$dt1= $this->unixTm($date1); 

			$r =  $dt1 - $dt2; 

			$hh= floor($r / 3600); 

			return $hh;

		} 

		

		public function dayadd($date1, $val){

			$timestamp = @strtotime($date1);

			$day = @date("d", $timestamp); 

			$month = @date("m", $timestamp);

			$year = @date("Y", $timestamp);

			$hour = @date("H", $timestamp);

			$minute = @date("i", $timestamp);

			$second = @date("s", $timestamp);

			//$date1 = @date("Y-m-d H:i:s", mktime ($hour,$minute,$second,$month,$day+$val,$year));

			$date1 = @date("Y-m-d", mktime ($hour,$minute,$second,$month,$day+$val,$year));

			return $date1; 

		}

		

		public function monthadd($date1, $val){

			$timestamp = @strtotime($date1);

			$day = @date("d", $timestamp); 

			$month = @date("m", $timestamp);

			$year = @date("Y", $timestamp);

			$hour = @date("H", $timestamp);

			$minute = @date("i", $timestamp);

			$second = @date("s", $timestamp);

		

			$date1 = @date("Y-m-d H:i:s", mktime ($hour,$minute,$second,$month+$val,$day,$year));

			return $date1; 

		}

		

		public function yearadd($date1, $val){

			$timestamp = @strtotime($date1);

			$day = @date("d", $timestamp); 

			$month = @date("m", $timestamp);

			$year = @date("Y", $timestamp);

			$hour = @date("H", $timestamp);

			$minute = @date("i", $timestamp);

			$second = @date("s", $timestamp);

			$date1 = @date("Y-m-d H:i:s", mktime ($hour,$minute,$second,$month,$day,$year+$val));

			return $date1; 

		}

		

		public function houradd($date1, $val){

			$timestamp = @strtotime($date1);

			$day = @date("d", $timestamp); 

			$month = @date("m", $timestamp);

			$year = @date("Y", $timestamp);

			$hour = @date("H", $timestamp);

			$minute = @date("i", $timestamp);

			$second = @date("s", $timestamp);

			$date1 = @date("Y-m-d H:i:s", mktime ($hour+$val,$minute,$second,$month,$day,$year));

			return $date1; 

		}



		public function minadd($date1, $val){

			$timestamp = @strtotime($date1);

			$day = @date("d", $timestamp); 

			$month = @date("m", $timestamp);

			$year = @date("Y", $timestamp);

			$hour = @date("H", $timestamp);

			$minute = @date("i", $timestamp);

			$second = @date("s", $timestamp);

			$date1 = @date("Y-m-d H:i:s", mktime ($hour,$minute+$val,$second,$month,$day,$year));

			return $date1; 

		}

		public function daydiff($date1, $val){

			$timestamp = @strtotime($date1);

			$day = @date("d", $timestamp); 

			$month = @date("m", $timestamp);

			$year = @date("Y", $timestamp);

			$hour = @date("H", $timestamp);

			$minute = @date("i", $timestamp);

			$second = @date("s", $timestamp);

			$date1 = @date("Y-m-d H:i:s", mktime($hour,$minute,$second,$month,$day-$val,$year));

			return $date1;

		}

		public function firstdayofmonth($date1){

			$timestamp = @strtotime($date1);

			$day = @date("d", $timestamp); 

			$month = @date("m", $timestamp);

			$year = @date("Y", $timestamp);

			$hour = @date("H", $timestamp);

			$minute = @date("i", $timestamp);

			$second = @date("s", $timestamp);

			//first day of the month is where $month=1;

			$day=1;

			$date1 = @date("Y-m-d H:i:s", mktime ($hour,$minute,$second,$month,$day,$year));

			return $date1; 

		}

		public function dateonly($date1){

			$timestamp = @strtotime($date1);

			$hour = @date("H", $timestamp);

			$minute = @date("i", $timestamp);

			$second = @date("s", $timestamp);

		

			$day = @date("d", $timestamp); 

			$month = @date("m", $timestamp);

			$year = @date("Y", $timestamp);

			

			$date1 = @date("Y-m-d H:i:s", mktime ($hour,$minute,$second,$month,$day,$year));

			return $date1; 

		}

		

		public function houronly($date1){

			$timestamp = @strtotime($date1);

			$hour = @date("H", $timestamp);

			return $hour; 

		}



		public function minonly($date1){

			$timestamp = @strtotime($date1);

			$hour = @date("i", $timestamp);

			return $hour; 

		}

		

		public function dayonly($date1){

			// Coverts a date into "Mon Sep 13 06:44:47 2004" format

			$timestamp = @strtotime($date1);

			$hour = @date("H", $timestamp);

			$minute = @date("i", $timestamp);

			$second = @date("s", $timestamp);



			$day = @date("d", $timestamp); 

			$month = @date("m", $timestamp);

			$year = @date("Y", $timestamp);

			$date1 = @date("D M j G:i:s Y", mktime ($hour,$minute,$second,$month,$day,$year));

			$dayname = @date("D", $timestamp);

			return $dayname; 

		}		





		public function dateonly2($date1){

			$timestamp = @strtotime($date1);

			$day = @date("d", $timestamp); 

			$month = @date("m", $timestamp);

			$year = @date("Y", $timestamp);

		

			$date1 = @date("Y-m-d", mktime($hour,$minute,$second,$month,$day,$year));

			return $date1; 

		}



		public function convertType1($date1){

			// Coverts a date into "Mon Sep 13 06:44:47 2004" format

			$timestamp = @strtotime($date1);

			$hour = @date("H", $timestamp);

			$minute = @date("i", $timestamp);

			$second = @date("s", $timestamp);

		

			$day = @date("d", $timestamp); 

			$month = @date("m", $timestamp);

			$year = @date("Y", $timestamp);

		

			$date1 = @date("D M j G:i:s Y", mktime ($hour,$minute,$second,$month,$day,$year));

			return $date1; 

		}		

		public function convertType2($date1){

			// Coverts a date into "Sep 13 2004" format

			$timestamp = @strtotime($date1);

			$hour = @date("H", $timestamp);

			$minute = @date("i", $timestamp);

			$second = @date("s", $timestamp);

		

			$day = @date("d", $timestamp); 

			$month = @date("m", $timestamp);

			$year = @date("Y", $timestamp);

		

			$date1 = @date("M j, Y", mktime ($hour,$minute,$second,$month,$day,$year));

			return $date1; 

		}		

		public function convertType3($date1){

			// Coverts a date into "Sep 13 2004" format

			$timestamp = @strtotime($date1);

			$hour = @date("H", $timestamp);

			$minute = @date("i", $timestamp);

			$second = @date("s", $timestamp);

			

			$day = @date("d", $timestamp); 

			$month = @date("m", $timestamp);

			$year = @date("Y", $timestamp);

			

			$date1 = @date("Y-m-d", mktime ($hour,$minute,$second,$month,$day,$year));

			return $date1; 

		}		

		public function convertType4($date1){

			// Coverts a date into "Sep 13 2004" format

			$timestamp = @strtotime($date1);

			$hour = @date("H", $timestamp);

			$minute = @date("i", $timestamp);

			$second = @date("s", $timestamp);

			$day = @date("d", $timestamp); 

			$month = @date("m", $timestamp);

			$year = @date("Y", $timestamp);

			

			$date1 = @date("d-m-Y", mktime ($hour,$minute,$second,$month,$day,$year));

			return $date1; 

		}



		public function convertType5($date1){

			// Coverts a date into "Mon Sep 13 06:44:47 2004" format

			$timestamp = @strtotime($date1);

			$hour = @date("H", $timestamp);

			$minute = @date("i", $timestamp);

			$second = @date("s", $timestamp);

		

			$day = @date("d", $timestamp); 

			$month = @date("m", $timestamp);

			$year = @date("Y", $timestamp);

		

			$date1 = @date("d/m/Y H:i", mktime ($hour,$minute,$second,$month,$day,$year));

			return $date1; 

		}		





		public function convertType6($date1){

			// Coverts a date into "Sep 13 2004" format

			$timestamp = @strtotime($date1);

			$hour = @date("H", $timestamp);

			$minute = @date("i", $timestamp);

			$second = @date("s", $timestamp);

			

			$day = @date("d", $timestamp); 

			$month = @date("m", $timestamp);

			$year = @date("Y", $timestamp);

			

			$date1 = @date("m/d/Y", mktime ($hour,$minute,$second,$month,$day,$year));

			return $date1; 

		}



		public function convertType7($date1){

			// Coverts a date into "Sep 13 2004" format

			$timestamp = @strtotime($date1);

			$hour = @date("H", $timestamp);

			$minute = @date("i", $timestamp);

			$second = @date("s", $timestamp);

			$day = @date("d", $timestamp); 

			$month = @date("m", $timestamp);

			$year = @date("Y", $timestamp);

			

			$date1 = @date("d/m/Y", mktime ($hour,$minute,$second,$month,$day,$year));

			return $date1; 

		}



		public function convertType8($date1){

			// Coverts a date into "Mon Sep 13 06:44:47 2004" format

			$timestamp = @strtotime($date1);

			$hour = @date("H", $timestamp);

			$minute = @date("i", $timestamp);

			$second = @date("s", $timestamp);

		

			$day = @date("d", $timestamp); 

			$month = @date("m", $timestamp);

			$year = @date("Y", $timestamp);

		

			$date1 = @date("D, M j G:i", mktime ($hour,$minute,$second,$month,$day,$year));

			return $date1; 

		}		



		public function convertType9($date1){

			// Coverts a date into "Mon Sep 13 06:44:47 2004" format

			$timestamp = @strtotime($date1);

			$hour = @date("H", $timestamp);

			$minute = @date("i", $timestamp);

			$second = @date("s", $timestamp);

			$day = @date("d", $timestamp); 

			$month = @date("m", $timestamp);

			$year = @date("Y", $timestamp);

		

			$date1 = @date("F d, Y", mktime ($hour,$minute,$second,$month,$day,$year));

			return $date1; 

		}	

		

		

		public function convertType1Deal($date1){

			// Coverts a date into "Mon Sep 13 06:44:47 2004" format

			$timestamp = @strtotime($date1);

			$hour = @date("H", $timestamp);

			$minute = @date("i", $timestamp);

			$second = @date("s", $timestamp);

			$day = @date("d", $timestamp); 

			$month = @date("m", $timestamp);

			$year = @date("Y", $timestamp);

		

			$date1 = @date("d.m.Y", mktime ($hour,$minute,$second,$month,$day,$year));

			return $date1; 

		}	



		public function daynumberonly($date1){

			$timestamp = @strtotime($date1);

			$day = @date("d", $timestamp); 

			return $day;

		}

		

		public function monthonly($date1){

			$timestamp = @strtotime($date1);

			$mon = @date("m", $timestamp); 

			return $mon;

		}

		

		public function yearonly($date1){

			$timestamp = @strtotime($date1);

			$year = @date("Y", $timestamp); 

			return $year;

		}

		

		public function getTime($date1){

			$timestamp = @strtotime($date1);

			$time = @date("H:i", $timestamp); 

			return $time;

		}



		public function timestampToDate($timestamp){

			$year = substr($timestamp, 0, 4);

			$mon = substr($timestamp, 4, 2);

			$day = substr($timestamp, 6, 2);

			$hr = substr($timestamp, 8, 2);

			$min = substr($timestamp, 10, 2);

			$sec = substr($timestamp, 12, 2);

			

			return $day."/".$mon."/".$year." ".$hr.":".$min.":".$sec;

		}



		public function timestampToDate1($timestamp){

				$year = substr($timestamp, 0, 4);

				$mon = substr($timestamp, 5, 2);

				$day = substr($timestamp, 8, 2);

				$hr = substr($timestamp, 11, 2);

				$min = substr($timestamp, 14, 2);

				$sec = substr($timestamp, 17, 2);

				

				return $day."/".$mon."/".$year;

		}



		public function timestampToDate2($timestamp){

			$hour = @date("H", $timestamp);

			$minute = @date("i", $timestamp);

			$second = @date("s", $timestamp);

			$day = @date("d", $timestamp); 

			$month = @date("m", $timestamp);

			$year = @date("Y", $timestamp);

			

			$date1 = @date("D M j G:i:s Y", mktime ($hour,$minute,$second,$month,$day,$year));

			return $date1; 

		}



		// this function converts a 10-digit date string like 25-10-2005

		// to a database compatible date string

		public function strToDate($dateste, $src_sep="/", $des_sep="/"){

			$dt_array = array();

			$dt_array = explode($src_sep, $dateste);

			$dd = $dt_array[0];

			$mm = $dt_array[1];

			$yy = $dt_array[2];

			$dt = $yy . $des_sep .$mm . $des_sep .$dd;

			return $dt;

		}

		

		// this function converts a 10-digit date string like 2005-10-25 (yyyy-mm-dd)

		// into dd/mm/yyyy format

		public function yyyy_mm_dd2dd_mm_yyyy($dateste, $src_sep="-", $des_sep="-"){

			$dt_array = array();

			$dt_array = explode($src_sep, $dateste);

			$dd = $dt_array[2];

			$mm = $dt_array[1];

			$yy = $dt_array[0];

			$dt = $dd. $des_sep .$mm.$des_sep .$yy;

			return $dt;

		}

		

		// this function converts a 10-digit date string like 2005=10-25 (yyyy-mm-dd)

		// into dd/mm/yyyy format

		public function yyyy_mm_dd2mm_dd_yyyy($dateste, $src_sep="-", $des_sep="/"){

			$dt_array = array();

			$dt_array = explode($src_sep, $dateste);

			$dd = $dt_array[2];

			$mm = $dt_array[1];

			$yy = $dt_array[0];

			$dt = $mm. $des_sep .$dd. $des_sep .$yy;

			return $dt;

		}

		

		

		// $monthName will be the string "February"

		public function yyyy_mm_dd2month_dd_yyyy($dateste, $src_sep="-", $des_sep="  "){

			$dt_array = array();

			$dt_array = explode($src_sep, $dateste);

			$dd = $dt_array[2];

			$mm = $dt_array[1];

			$yy = $dt_array[0];

			$monthName = jdmonthname ( juliantojd( $mm, $dd, $yy ), 2 );

			$dt = $monthName. $des_sep. $dd. ", "  .$yy;

			return $dt;

		}

		

		

		// this function converts a 10-digit date string like 2005=10-25 (yyyy-mm-dd)

		// into dd/mm/yyyy format

		public function mm_dd_yyyy2yyyy_mm_dd($dateste, $src_sep="/", $des_sep="-"){

			$dt_array = array();

			$dt_array = explode($src_sep, $dateste);

			$dd = $dt_array[1];

			$mm = $dt_array[0];

			$yy = $dt_array[2];

			$dt = $yy.$des_sep.$mm.$des_sep.$dd;

			return $dt;

		}

		

		public function dd_mm_yyyy2yyyy_mm_dd($dateste, $src_sep="/", $des_sep="-"){

			$dt_array = array();

			$dt_array = explode($src_sep, $dateste);

			$dd = $dt_array[0];

			$mm = $dt_array[1];

			$yy = $dt_array[2];

			$dt = $yy.$des_sep.$mm.$des_sep.$dd;

			return $dt;

		}

		

		

		public function dd_mm_yyyy2yyyy_mm_dd2($dateste, $src_sep="/", $des_sep="-"){

			$dt_array = array();

			$dt_array = explode($src_sep, $dateste);

			$mm = $dt_array[0];

			$dd = $dt_array[1];

			$yy = $dt_array[2];

			$dt = $yy.$des_sep.$mm.$des_sep.$dd;

			return $dt;

		}



}

?>