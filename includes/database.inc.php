<?php	 	
	/*****************************************************************************
		Database Class for MySQL Server. Please do not change anything
	*****************************************************************************/
	class Database 
	{
		private $Con;
				
		public function __construct($db) 
		{
			$this->Con = @mysql_connect($db['DATABASE_HOST'].":".$db['DATABASE_PORT'],$db['DATABASE_USER'],$db['DATABASE_PASSWORD']) or die($this->error(mysql_error(),__FILE__,__LINE__));
			@mysql_select_db($db['DATABASE_NAME'],$this->Con) or die($this->error(mysql_error(),__FILE__,__LINE__));
		}
		
		public function __destruct() 
		{
      		$this->close();
   		}
		
		public function get($sql,$errorFile = __FILE__,$errorLine = __LINE__) 
		{
			$query = $sql;
			$result = @mysql_query($query,$this->Con) or die($this->error($query."<br />".mysql_error(),$errorFile,$errorLine));
			return $result;
		}
		
		public function check($table,$arrFieldValue = NULL, $arrOrderBy = NULL, $print = false)
		{
			$sql = "SELECT * FROM `".$table."`";
			if($arrFieldValue!==NULL)
			{
				if(is_array($arrFieldValue)===true && empty($arrFieldValue)===false)
				{
					$sql.= " WHERE ";
					$counter = 1;
					foreach($arrFieldValue as $field => $value)
					{
						if($counter > 1) $sql.=" AND ";
						$sql.="`".$field."`='".$value."'";
						$counter++;
					}
				}
			}
			
			if($arrOrderBy!==NULL)
			{
				if(is_array($arrOrderBy)===true && empty($arrOrderBy)===false)
				{
					$sql.= " ORDER BY ";
					$counter = 1;
					foreach($arrOrderBy as $field => $value)
					{
						if($counter > 1) $sql.=", ";
						$sql.="`".$field."` ".$value;
						$counter++;
					}
				}
			}
			
			if($print === true)
			{
				echo $sql;
			}
			else
			{
				$res = $this->get($sql,__FILE__,__LINE__);
				return $this->num_rows($res);
			}
		}
		
		public function fetch_array($result) 
		{
			return mysql_fetch_array($result);
		}
		
		public function last_insert_id() 
		{
			return mysql_insert_id($this->Con);
		}
		
		public function result($result,$row,$column) 
		{
			return mysql_result($result,$row,$column);
		}
		
		public function num_rows($result) 
		{
			return mysql_num_rows($result);
		}
		
		public function free_result($result) 
		{
			mysql_free_result($result);
		}

		public function insert($table,$DataArray,$printSQL = false) 
		{
			if(count($DataArray) == 0) 
			{
				die($this->error("INSERT INTO statement has not been created",__FILE__,__LINE__));
			}
			foreach($DataArray as $key => $val) 
			{
				$strFields.= "`".$key."`,";
				if($val == "0") 
				{
					$strValues.= "0,";
				} 
				elseif($val == "CURDATE()") 
				{
					$strValues.= "CURDATE(),";
				} 
				elseif($val == "CURTIME()") 
				{
					$strValues.= "CURTIME(),";
				} 
				else 
				{
					$strValues.= "'".$val."',";	
				}
			}
			$strFields = substr($strFields,0,strlen($strFields)-1);
			$strValues = substr($strValues,0,strlen($strValues)-1);
			$sql = "INSERT INTO `".$table."`(".$strFields.") VALUES(".$strValues.")";
			if($printSQL == true) 
			{
				echo $this->error($sql,__FILE__,__LINE__);
			} 
			else 
			{
				$this->get($sql,__FILE__,__LINE__);
			}
		}

		public function update($table,$DataArray,$updateOnField,$updateOnFieldValue,$printSQL = false) 
		{
			if(count($DataArray) == 0) 
			{
				die($this->error("UPDATE statement has not been created",__FILE__,__LINE__));
			}
			$sql = "UPDATE ".$table." SET ";
			foreach($DataArray as $key => $val) 
			{
				$strFields = "`".$key."`";
				if($val == "0") 
				{
					$strValues = "0";
				} 
				elseif($val == "CURDATE()") 
				{
					$strValues = "CURDATE()";
				} 
				elseif($val == "CURTIME()") 
				{
					$strValues = "CURTIME()";
				} 
				else 
				{
					$strValues = "'".$val."'";	
				}
				$sql.= $strFields."=".$strValues.", ";
			}
			$sql = substr($sql,0,strlen($sql)-2);
			$sql.= " WHERE `".$updateOnField."`='".$updateOnFieldValue."'";
			if($printSQL == true) 
			{
				echo $this->error($sql,__FILE__,__LINE__);
			} 
			else 
			{
				$this->get($sql,__FILE__,__LINE__);
			}
		}

		public function getDateDiff($coming_date) 
		{
			$diff_sql = "SELECT DATEDIFF('".$coming_date."','".date('Y-m-d')."')";
			$diff_res = $this->get($diff_sql);
			return $this->result($diff_res,0,0);
		}
		
		public function record_number($sql) 
		{
			$result = $this->get($sql);
			$cnt = $this->num_rows($result);
			return $cnt;
		}
		
		public function getFields($result)
		{
			$i = 0;
			$data = array();
			while($i < mysql_num_fields($result))
			{
				$meta = mysql_fetch_field($result,$i);
				$data[] = $meta->name;
				$i++;	
			}
			return $data;
		}
		
		public function pagination($sql,$DividedRecordNumber,$Page) 
		{
			$PageResult = $this->record_number($sql);
			if($Page == "" || $Page == 1) 
			{
				$Page = 0;
			} 
			else 
			{
				$Page = ($Page-1) * $DividedRecordNumber;
			}
			$RecordPerPage = ceil($PageResult/$DividedRecordNumber);
			$ReturnResult = $this->get($sql." LIMIT ".$Page.",".$DividedRecordNumber."");
			return $ReturnResult;
		}
		
		public function pagination_page_number($sql,$DividedRecordNumber,$Page,$PageName,$QueryString) 
		{
			$PageResult = $this->record_number($sql);
			$RecordPerPage = ceil($PageResult/$DividedRecordNumber);
			if($Page == "") 
			{
				$Page = 1;
			}
			$str = "<select name=\"cmbPage\" id=\"cmbPage\" onchange=\"javascript:_doPagination('".$PageName."','".$QueryString."');\">\n";
			for($i = 1;$i <= $RecordPerPage;$i++) 
			{
				if($Page == $i) 
				{
					$selected = ' selected';
				} 
				else 
				{
					$selected = '';
				}
				$str.= "<option value=\"".$i."\"".$selected.">Page ".$i."</option>\n";
			}
			$str.= "</select>";
			echo $str;
		}
		
		public function paging($sql,$DividedRecordNumber,$Page,$PageName,$QueryStringName) 
		{
			$PageResult = $this->record_number($sql);
			if($PageResult > $DividedRecordNumber):
				echo "<table cellpadding=\"0\" cellspacing=\"0\" border=\"0\">
						<tr>";
				$RecordPerPage = ceil($PageResult/$DividedRecordNumber);
				if($Page == "") 
				{
					$Page = 1;
				}
				$PageCount = $Page - 1;
				if($PageCount > 0) 
				{
					if(empty($QueryStringName)) 
					{
						echo "<td class=\"footermenulink\" width=\"100\" align=\"center\"><a href='".$PageName."?page=".$PageCount."'>&lt;&lt; Previous</a></td>";
					} 
					else 
					{
						echo "<td class=\"footermenulink\" width=\"100\" align=\"center\"><a href='".$PageName."?page=".$PageCount."&".$QueryStringName."'>&lt;&lt; Previous</a></td>";
					}
				} 
				else 
				{
					echo "";
				}

				if($RecordPerPage > 2)
				{
					echo "<td width=\"4\"></td>";
				}
				/*for($i = 1;$i <= $RecordPerPage;$i++) 
				{
					if($Page == $i) 
					{
						echo "<b>".$i."</b>&nbsp;";
					} 
					else 
					{
						if(empty($QueryStringName)) 
						{
							echo "<a href='".$PageName."?page=".$i."'>".$i."</a>&nbsp;";
						} 
						else 
						{
							echo "<a href='".$PageName."?page=".$i."&".$QueryStringName."'>".$i."</a>&nbsp;";
						}
					}
				}*/
				$PageCount = $Page + 1;
				if($PageCount < $RecordPerPage + 1) 
				{
					if(empty($QueryStringName)) 
					{
						echo "<td class=\"footermenulink\" width=\"100\" align=\"center\"><a href='".$PageName."?page=".$PageCount."'>Next &gt;&gt;</font></td>";
					} 
					else 
					{
						echo "<td class=\"footermenulink\" width=\"100\" align=\"center\"><a href='".$PageName."?page=".$PageCount."&".$QueryStringName."'>Next &gt;&gt;</a></td>";
					}
				} 
				else 
				{
					echo "";
				}
				echo "</tr>
					</table>";
			else:
				echo "&nbsp;";
			endif;
		}
		
		public function TablePagination($sql,$DividedRecordNumber,$Page,$PageName,$Class,$SelectedClass,$QueryString='',$NumberOfLinks=10) {
			// calculating basics
			$PageResult = $this->record_number($sql); // total number of rows in database
			$total_page = ceil($PageResult/$DividedRecordNumber); // calculating total number of pages
			$NumberOfLinks_half = ceil($NumberOfLinks/2);
			
			// starting the table structure
			$output = '
				<table align="center" cellpadding="0" cellspacing="0" border="0">
				  <tr><td class="padingright">';
				 if($Page > 1) {
				$PageCount = $Page - 1;
				
				$output.= '<span class="'.$Class.'"><a href="'.WEBSITE_URL."/".$PageName.'?page='.$PageCount."&".$QueryString.'">&laquo; Previous</a></span>&nbsp;';
			} else {
				$output.= '<span class="page_navs">&laquo; Previous</a></span>&nbsp;';
			}
			$output.= '</td><td valign="bottom" align="right">
			';
			
			if($PageResult > $DividedRecordNumber){ // if calculated number of pages are more than 1
				$RecordPerPage = ceil($PageResult/$DividedRecordNumber);
				if($Page == "") {
					$Page = 1;
				}
				if(empty($QueryString)==true) {
					$QueryString = '';
				}else{
					$QueryString = '&amp;'.$QueryString;
				}
				// Calculating if the page greater than record per page display
				if($Page>1) {
					$output.= '<span class="'.$Class.'"><a href="'.WEBSITE_URL."/".$PageName.'?page=1'.$QueryString.'">1</a></span>&nbsp;';
				}else{
					$output.= '<span class="'.$SelectedClass.'">1</span>&nbsp;';
				}
				
				$forward = $Page + $NumberOfLinks;
				
				if($forward <= $total_page) {
					$start = $Page-$NumberOfLinks_half;
					if($Page>=$NumberOfLinks_half){
						$end = $forward-$NumberOfLinks_half;
					}else{
						$end = $forward;
					}
				} else {
					$start = $total_page - $NumberOfLinks;
					$end = $total_page;
				}
				if($start<2){
					$start = 2;
				}
				
				//echo $start."|".$end;
				if($start>2){
					$output.= '...&nbsp;';
				}
				for($i = $start;$i < $end;$i++) {
					if($Page == $i) {
						$output.= '<span class="'.$SelectedClass.'">'.$i.'</span>&nbsp;';
					} else {
						if($i>0){
							$output.= '<span class="'.$Class.'"><a href="'.WEBSITE_URL."/".$PageName.'?page='.$i.$QueryString.'">'.$i.'</a></span>&nbsp;';
						}
					}
				}

				if($total_page!=$Page) {
					if($end<$total_page)
					$output.= '...&nbsp;';
					$output.= '<span class="'.$Class.'"><a href="'.WEBSITE_URL."/".$PageName.'?page='.$total_page.$QueryString.'" class="'.$Class.'">'.$total_page.'</a></span>';
				} else {
					$output.= '<span class="'.$SelectedClass.'">'.$total_page.'</span>';
				}
			}else{ // if there is only 1 page
				$output.= '<span class="'.$SelectedClass.'">1</span>';
			}
			$output.= '
				</td>
				<td  valign="bottom" align="right" class="padingleft">
			';
			// end of showing page number and start of showing next-previous links
			
			
			$PageCount = $Page + 1;
			if($PageCount < $RecordPerPage + 1) {
				$output.= '&nbsp;<span class="'.$Class.'"><a href="'.WEBSITE_URL."/".$PageName.'?page='.$PageCount.$QueryString.'">Next &raquo;</a></span>';
			} else {
				$output.= '&nbsp;<span class="page_navs">Next &raquo;</a></span>';
			}
			// ending the table structure
			$output.= '
				</td>
			  </tr>
			</table>
			';
			$output = trim($output);
			echo $output;
		}
		
		public function getProductAdmin($pId){
			$value = "";
			$pId = $pId;	
			$pId = explode(",",$pId);
			$pIds = count($pId);
			for($i=0;$i<$pIds;$i++){
				$sql = "SELECT * FROM `tbl_product` WHERE `product_id`=".$pId[$i];	
				$res = $this->get($sql);
				$row = $this->fetch_array($res);
				$value .= $row['product_name'].",";
			}			
			$value = substr($value,0,-1);
			return $value;
		}
		
		public function getProduct($pId,$type){
			$sql = "SELECT * FROM `tbl_product` WHERE `product_id`=".$pId;	
			$res = $this->get($sql);
			$row = $this->fetch_array($res);
			if($type == "SCNAME"){
				$value = $row['sc_name'];
			}
			if($type == 'PRICE'){
				$value = $row['product_price'];
			}	
			if($type == 'NAME'){
				$value = $row['product_name'];
			}	
			return $value;
		}
		
		public function getProductId($scName){
			$sql = "SELECT * FROM `tbl_videos` WHERE `sc_name`='".$scName."'";	
			$res = $this->get($sql);
			$row = $this->fetch_array($res);
			$value = $row['product_id'];				
			return $value;
		}
		
		public function close() 
		{
			mysql_close($this->Con);
		}

		public function error($arg_error_msg) 
		{
			if(empty($arg_error_msg)==false) 
			{
				$error_msg = "<div style=\"font-family: Tahoma; font-size: 11px; padding: 10px; background-color: #FFD1C4; color: #990000; font-weight: bold; border: 1px solid #FF0000; text-align: center;\">";
				$error_msg.= $arg_error_msg;
				$error_msg.= "</div>";
				return $error_msg;
			}
		}
		
		public function getBasket(){
			//$sql_basket = "SELECT * FROM `tbl_cart` WHERE `session_id`='".session_id()."'";
			$sql_basket = "SELECT SUM(`quantity`) FROM `tbl_cart` WHERE `session_id`='".session_id()."' AND `status`='Inactive'";
			$res_basket = $this->get($sql_basket);
			$row_basket = $this->fetch_array($res_basket);
			$qtyis = $row_basket[0];
			if($qtyis == ''){				
				$qtyis = '0';
			}	
			$value = "<ul><li>".$qtyis." item(s) &raquo;</li></ul>";
			echo $value;
		}
		
		public function getAdditionalInfo($product_additional_image_id){
			$sql = "SELECT * FROM `tbl_product_additional_image` WHERE `product_additional_image_id`=".$product_additional_image_id;
			$res = $this->get($sql);
			$row = $this->fetch_array($res);
			$value_image = $row['additional_small_image'];
			$value_caption = $row['additional_caption'];			
			return $value_image."|".$value_caption;
		}
		
		public function getAdditionalInfo_byproduct($product_id){
			$sql = "SELECT * FROM `tbl_product_additional_image` WHERE `product_id`=".$product_id;
			$res = $this->get($sql);
			$row = $this->fetch_array($res);
			$value_image = $row['additional_small_image'];
			$value_caption = $row['additional_caption'];
			$value_id = $row['product_additional_image_id'];			
			return $value_image."|".$value_caption."|".$value_id;
		}		
		
		public function getSettings() {
			$sql = "SELECT * FROM `tbl_admin` WHERE `admin_id`=1";
			$res = $this->get($sql,__FILE__,__LINE__);
			$row = $this->fetch_array($res);
			$fa = array("your_email_address","smtp","email_from_name","email_from_address","smtp_hostname","smtp_username","smtp_password","tax","tax2");
			$array = array();
			foreach($fa as $val) {
				$array[$val] = trim($row[$val]);
			}
			return $array;
		}
		public function getMaxSQ($table){
			$sql = "SELECT MAX(`display_order`) FROM ".$table;
			$res = $this->get($sql);
			$row = $this->fetch_array($res);
			return $row[0];
		}
		public function getCountry($iso2){
			$sql = "SELECT * FROM `tbl_country_list` WHERE `countries_iso_code_2`='".$iso2."'";
			$res = $this->get($sql,__FILE__,__LINE__);
			$row = $this->fetch_array($res);
			$value = $row['countries_name'];
			return $value;
		}
		public function updateFreeCouponProduct($couponnumber){
			$sql_carts = "SELECT a.*, b.`cupon_number`, b.`cupon_product_id`,b.`free_price` FROM `tbl_cart` AS a, `tbl_cupon` AS b WHERE a.`session_id`='".session_id()."' AND b.`cupon_number`='".$couponnumber."' AND a.`product_id`=b.`cupon_product_id`";
			$res_carts = $this->get($sql_carts);
			$rec_carts = $this->num_rows($res_carts);
			if($rec_carts > 0){
				$row_carts = $this->fetch_array($res_carts);
				$freePrice = $row_carts['free_price'] * $row_carts['quantity'];
				$sql_up_free_product = "UPDATE `tbl_cart` SET `product_free`='".$row_carts['quantity']."', `price`='".$freePrice."' WHERE `product_id`='".$row_carts['product_id']."' AND `session_id`='".session_id()."'";
				$this->get($sql_up_free_product);
			}
		}
		public function updateNormalCouponProduct($couponnumber){
			$sql_carts = "SELECT a.*, b.`cupon_number`, b.`cupon_product_id`, b.`calculation_type`, b.`disc_amount` FROM `tbl_cart` AS a, `tbl_cupon` AS b WHERE a.`session_id`='".session_id()."' AND b.`cupon_number`='".$couponnumber."' AND a.`product_id`=b.`cupon_product_id`";
			$res_carts = $this->get($sql_carts);
			$rec_carts = $this->num_rows($res_carts);
			if($rec_carts > 0){
				$row_carts = $this->fetch_array($res_carts);
				//$freePrice = $row_carts['free_price'] * $row_carts['quantity'];
				$productOriginalPrice = $this->getProduct($row_carts['product_id'],'PRICE');
				if($row_carts['calculation_type'] == 'amount'){
					$discAmount = $row_carts['disc_amount'];
				}else{
					$discAmount = $productOriginalPrice * $row_carts['disc_amount']/100;
				}
				$_SESSION['normalDiscountAmout'] = $discAmount;
				$productDiscPrice = $productOriginalPrice - $discAmount;
				$thePrice = $productDiscPrice * $row_carts['quantity'];	
				$sql_up_free_product = "UPDATE `tbl_cart` SET `price`='".$thePrice."' WHERE `product_id`='".$row_carts['product_id']."' AND `session_id`='".session_id()."'";
				$this->get($sql_up_free_product);
			}
		}
		
		public function updateCart(){
			$sql_select = "SELECT * FROM `tbl_cart` WHERE `session_id`='".session_id()."' ORDER BY `cart_id` ASC";
			$res_select = $this->get($sql_select);
			while($row_select = $this->fetch_array($res_select)){
				$productOriginalPrice = $this->getProduct($row_select['product_id'],'PRICE');
				$cartPrice = $productOriginalPrice * $row_select['quantity'];
				$sql = "UPDATE `tbl_cart` SET `price`='".$cartPrice."' WHERE `cart_id`='".$row_select['cart_id']."'";
				$this->get($sql);
			}
		}
		
		public function getCategory($cat_id){
			$sql = "SELECT * FROM `tbl_category` WHERE `cat_id`='".$cat_id."'";
			$res = $this->get($sql);
			$row = $this->fetch_array($res);
			$value = $row['cat_name'];
			return $value;
		}
		
		public function getSubCategory($sub_cat_id){
			$sql = "SELECT * FROM `tbl_sub_category` WHERE `sub_cat_id`='".$sub_cat_id."'";
			$res = $this->get($sql);
			$row = $this->fetch_array($res);
			$value = $row['sub_cat_name'];
			return $value;
		}
		
		public function getSubCategory2($sub_cat_id_2){
			$sql = "SELECT * FROM `tbl_sub_category_2` WHERE `sub_cat_id_2`='".$sub_cat_id_2."'";
			$res = $this->get($sql);
			$row = $this->fetch_array($res);
			$value = $row['sub_cat_2_name'];
			return $value;
		}
		
		public function getCoupon($cupon_number,$type){
			$sql = "SELECT * FROM `tbl_cupon` WHERE `cupon_number`='".$cupon_number."'";
			$res = $this->get($sql);
			$row = $this->fetch_array($res);
			if($type == 'FreePrice'){
				$value = $row['free_price'];
			}
			return $value;
		}
		
		public function getStaticContent($id){
			$sql = "SELECT * FROM `tbl_static_page_content` WHERE `page_id`=".$id;
			$res = $this->get($sql);
			$row = $this->fetch_array($res);
			$value = $row['page_content'];
			return $value;
		}	
		public function mysqlDate($date){
			$sqldt = "SELECT DATE_FORMAT('".$date."','%W, %M %D, %Y')";
			$resdt = $this->get($sqldt);
			$rowdt = $this->fetch_array($resdt);	
			return $rowdt[0];		
		}
		
		/*public function getvideoImage($product_id,$type){
			$sql = "SELECT * FROM `tbl_videos` WHERE `product_id`=".$product_id;	
			$res = $this->get($sql);
			$row = $this->fetch_array($res);
			
			$value = $row['image_is'];
			return $value;
		}*/
		
		public function getCommentCount($product_id){
			$sql = "SELECT * FROM `tbl_reviews` WHERE `product_id`=".$product_id;	
			$res = $this->get($sql);
			$rec = $this->num_rows($res);			
			$value = $rec;
			return $value;
		}
		
		function displayCart($type='Item') {
			if($type == 'Item'){
				if(empty($_SESSION['_user_id']) == false){					
					//$sql = "SELECT SUM(`quantity`) FROM `tbl_cart` WHERE `session_id`='".session_id()."' AND `customer_id`=".$_SESSION['_user_id'];
					$sql = "SELECT count(*) FROM `tbl_cart` WHERE `session_id`='".session_id()."' AND `customer_id`=".$_SESSION['_user_id'];					
				}else{
					$sql = "SELECT count(*) FROM `tbl_cart` WHERE `session_id`='".session_id()."'";
					//$sql = "SELECT SUM(`quantity`) FROM `tbl_cart` WHERE `session_id`='".session_id()."'";
				}
				$res = $this->get($sql);
				//return $this->num_rows($res);
				$row = $this->fetch_array($res);
				if(empty($row[0]) == true){
					return '0';
				}else{
					return $row[0];
				}	
			}elseif($type == "Amount"){
				if(empty($_SESSION['_user_id']) == false){
					$sql = "SELECT SUM(`total_price`) FROM `tbl_cart` WHERE `session_id`='".session_id()."' AND `customer_id`=".$_SESSION['_user_id'];
				}else{
					$sql = "SELECT SUM(`total_price`) FROM `tbl_cart` WHERE `session_id`='".session_id()."'";
				}				
				$res = $this->get($sql);
				$row = $this->fetch_array($res);
				return $row[0];
			}elseif($type == "Tax"){
				if(empty($_SESSION['_user_id']) == false){
					$sql = "SELECT SUM(`tax_amount`) FROM `tbl_cart` WHERE `session_id`='".session_id()."' AND `customer_id`=".$_SESSION['_user_id'];
				}else{
					$sql = "SELECT SUM(`tax_amount`) FROM `tbl_cart` WHERE `session_id`='".session_id()."'";
				}					
				$res = $this->get($sql);
				$row = $this->fetch_array($res);
				return $row[0];
			}
			
		}
		
		public function getuserInfo($userId,$type){
			$sql = "SELECT * FROM `tbl_customer` WHERE `customer_id`=".$userId;
			$res = $this->get($sql);
			$row = $this->fetch_array($res);
			if($type == "STATE"){
				$value = $row['state'];
			}elseif($type == "ZIP"){
				$value = $row['zip'];
			}elseif($type == "COUNTRY"){
				$value = $row['country'];
			}
			return $value;
		}
		
		public function getFreeShipping(){
			$sql = "SELECT COUNT(*) FROM `tbl_free_shipping` WHERE curdate()>=`start_date` AND curdate()<=`end_date`";
			$res = $this->get($sql);
			$row = $this->fetch_array($res);
			$value = $row[0];
			return $value;
		}
		public function getCupon($cupon_number){
			if(empty($cupon_number) == false){
				$sql = "SELECT `cupon_number`, `disc_amount` FROM `tbl_cupon` WHERE `cupon_number`='".$cupon_number."'";
				$res = $this->get($sql);
				$row = $this->fetch_array($res);
				$value = round($row['disc_amount'],2)."%";
				return $value;
			}else{
				return "0%";
			}	
		}
	}
?>