<?php
	class Functions {
		public $redirectBase;
		
		public function Redirect($url,$msg = NULL) 
		{
			if($msg!=NULL)
			{
				if(strstr($url, '?') == true)
				{
					$char = "&";
				}
				else
				{
					$char = "?";
				}
				$url = $url.$char."msg=".urlencode($msg);
			}
			header("Location: ".$url);
			exit();
		}
		
		public function setValue($String, $decode = FALSE) 
		{
			$str = trim($String);
			if($decode === TRUE) $str = urldecode($str); 
			if(!get_magic_quotes_gpc()) 
			{
				$str = addslashes($str);
			}
			return $str;
		}
		
		public function getValue($String) 
		{
			$String = stripslashes(trim($String));
			return $String;
		}
		
		public function br($str) 
		{
			$str = $this->getValue(trim($str));
			return nl2br($str);
		}

		public function alert($msg) 
		{
			$scr = "<script language=\"javascript\" type=\"text/javascript\">\n";
			$scr.= "alert(\"".$msg."\");\n";
			$scr.= "</script>\n";
			echo $scr;
		}
		
		public function location($path,$opener=false) 
		{
			$scr = "<script type=\"text/javascript\">\n";
			if($opener===true) 
			{
				$scr.= "window.opener.location.href=\"".$path."\";\n";
			} else 
			{
				$scr.= "window.location.href=\"".$path."\";\n";
			}			
			$scr.= "</script>\n";
			echo $scr;
		}
		
		public function close() 
		{
			$scr = "<script language=\"javascript\" type=\"text/javascript\">\n";
			$scr.= "self.close();\n";
			$scr.= "</script>\n";
			echo $scr;
		}
		
		public function reload($opener=false) 
		{
			$scr = "<script type=\"text/javascript\">\n";
			if($opener == true) 
			{
				$scr.= "window.opener.location.reload();\n";
			} 
			else 
			{
				$scr.= "window.location.reload();\n";
			}			
			$scr.= "</script>\n";
			echo $scr;
		}
		
		public function isLogin($getSessionName,$getBackPage) 
		{
			if(!isset($_SESSION[$getSessionName])) 
			{
				if(empty($_SERVER['QUERY_STRING'])===false) 
				{
					$redirectTo = basename($_SERVER['PHP_SELF'])."?".$_SERVER['QUERY_STRING'];
				} 
				else 
				{
					$redirectTo = basename($_SERVER['PHP_SELF']);
				}

				$this->location($getBackPage."?redirectTo=".base64_encode($this->redirectBase."/".$redirectTo));
				exit();
			}
		}
		
		public function getHtmlError($msg) 
		{
			return "<div class=\"error\">".$msg."</div>";
		}
		
		public function getHtmlMessage($msg) 
		{
			return "<div class=\"message\">".$msg."</div>";
		}
				
		public function CreateFolder($Path,$Permission) 
		{
			if(!is_dir($Path) == true) 
			{
				@mkdir($Path,$Permission) or $this->error($php_errormsg);
				@chmod($Path,$Permission) or $this->error($php_errormsg);
			}
		}

		public function CreateFile($filename,$path,$text) 
		{
			$output_file = $path."/".$filename;
			if(is_file($output_file)===true) @unlink($output_file);
			$handle = fopen($output_file,"w+");
			fwrite($handle,$text);
			@chmod($output_file,0777);
			fclose($handle);
		}

		public function DeleteFolder($path) 
		{
			/*
				While using this function make sure that the folder and all the
				sub folders and files must have 0777 permission otherwise
				the function may will give error
			*/
			if(is_dir($path)===true) 
			{
				$d = dir($path); 
				while($entry = $d->read()) 
				{ 
					if($entry!="." && $entry!="..") 
					{ 
						if(is_dir($path."/".$entry)===true) 
						{
							$this->DeleteFolder($path."/".$entry);
						} 
						else 
						{
							@unlink($path."/".$entry);
						}
					} 
				} 
				$d->close();
				@rmdir($path);
			}
		}

		public function DeleteFile($filename) 
		{
			if(file_exists($filename)===true && is_file($filename)===true) @unlink($filename);
		}

		public function getWebsiteFullURL($url) 
		{
			$url = $this->getValue($url);
			if($url == "#") 
			{
				$website = "#";
			} 
			else 
			{
				if(preg_match("/\bhttp\b/i",$url)) 
				{
					$website = $url;
				} 
				else 
				{
					$website = "http://".$url;
				}
			}
			return $website;
		}

		public function makePassword($pwd, $decode = FALSE) 
		{
			$pass = trim($pwd);
			if($decode === TRUE) $pass = urldecode($pass); 
			$pass = md5(strrev($pass));
			return $pass;
		}
		
		public function RandomName($filename) 
		{
			$file_array = explode(".",$filename);
			$file_ext = $file_array[count($file_array)-1];
			$new_file_name = uniqid().date('m').date('d').date('Y').date('G').date('i').date('s').".".$file_ext;
			return $new_file_name;
		}
		
		public function getRandomNameImage($filename) 
		{
			$file_array = explode(".",$filename);
			$file_ext = $file_array[count($file_array)-1];
			$new_file_name = $this->RandomNumber(20).".".$file_ext;
			return $new_file_name;
		}

		public function getRandomName($filename) 
		{
			$file_array = explode(".",$filename);
			$file_ext = end($file_array);
			$new_file_name = $this->RandomNumber(10).".".$file_ext;
			return $new_file_name;
		}

		public function RandomNumber($length='', $datetime = TRUE) 
		{
			if(empty($length)===true) 
			{
				$random = uniqid().date('m').date('d').date('Y').date('G').date('i').date('s');
			} 
			else 
			{				 
				srand((double)microtime()*1000000); 
				$data = "123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ"; 
				for($i = 0;$i < $length;$i++) 
				{
					$random.= substr($data,(rand()%(strlen($data))),1); 
				}
				if($datetime === TRUE) $random = $random."-".date("Ymd")."-".date("His");
			}
			return $random;
		}

		public function SendMail($MailToAddress,$MailMessage,$MailSubject,$Cc,$Bcc,$MailFromAddress) 
		{
			$header = "MIME-Version: 1.0\r\n";
			$header.= "Content-Type: text/html; charset=iso-8859-1\r\n";
			$header.= "From: ".$MailFromAddress."\r\n";
			if(empty($Cc)===false) 
			{
				$header.= "Cc: ".$Cc."\r\n";
			}
			if(empty($Bcc)===false) 
			{
				$header.= "Bcc: ".$Bcc."\r\n";
			}
			@mail($MailToAddress,$MailSubject,$MailMessage,$header) or die($this->error($php_errormsg));
		}
		public function ChangeFormatDate2($dt) 
		{
			$VarTotalDate = explode("/",$dt);
			$VarDate = $VarTotalDate[1];
			$VarMonth = $VarTotalDate[0];
			$VarYear = $VarTotalDate[2];
			$CompleteDate = $VarYear.'-'.$VarMonth.'-'.$VarDate;		
			return $CompleteDate;
		}
		public function ChangeFormatDate($dt,$type="mysql") 
		{
			$VarTotalDate = explode("-",$dt);
			if($type == 'mysql') 
			{
				$VarDate = $VarTotalDate[1];
				$VarMonth = $VarTotalDate[0];
				$VarYear = $VarTotalDate[2];
				$CompleteDate = $VarYear.'-'.$VarMonth.'-'.$VarDate;
			} 
			elseif($type == 'expand') 
			{
				$VarDate = $VarTotalDate[2];
				$VarMonth = $VarTotalDate[1];
				$VarYear = $VarTotalDate[0];
				$CompleteDate = date("F j, Y",mktime(0,0,0,$VarTotalDate[1],$VarTotalDate[2],$VarTotalDate[0]));
			} 
			elseif($type == 'short') 
			{
				$VarDate = $VarTotalDate[2];
				$VarMonth = $VarTotalDate[1];
				$VarYear = $VarTotalDate[0];
				$CompleteDate = date("M j",mktime(0,0,0,$VarTotalDate[1],$VarTotalDate[2]));
			} 
			elseif($type == 'medium') 
			{
				$VarDate = $VarTotalDate[2];
				$VarMonth = $VarTotalDate[1];
				$VarYear = $VarTotalDate[0];
				$CompleteDate = date("M j, Y",mktime(0,0,0,$VarTotalDate[1],$VarTotalDate[2],$VarTotalDate[0]));
			} 
			else 
			{
				$VarDate = $VarTotalDate[2];
				$VarMonth = $VarTotalDate[1];
				$VarYear = $VarTotalDate[0];
				// Indian Format
				//$CompleteDate = $VarDate.'-'.$VarMonth.'-'.$VarYear;
				// USA Format
				$CompleteDate = $VarMonth.'-'.$VarDate.'-'.$VarYear;
			}
			return $CompleteDate;
		}
		
		public function error($arg_error_msg) 
		{
			if(empty($arg_error_msg)===false) 
			{
				$error_msg = "<div style=\"font-family: Tahoma; font-size: 11px; padding: 10px; background-color: #FFD1C4; color: #990000; font-weight: bold; border: 1px solid #FF0000; text-align: center;\">";
				$error_msg.= $arg_error_msg;
				$error_msg.= "</div>";
				return $error_msg;
			}
		}
		function TimeFormat($time) {
			$time_array = explode(":",$time);
			$mktime = mktime($time_array[0],$time_array[1],$time_array[2],0,0,0);
			return date("g:i A",$mktime);
		}
		public function getSize($size) 
		{
			if($size >= 1048576) 
			{
				$return = round($size / 1048576,2)." MB";
			} 
			elseif($size >= 1024) 
			{
				$return = round($size / 1024,2)." KB";
			} 
			else 
			{
				$return = $size." Bytes";
			}
			return $return;
		}
		
		 function calculateSize($size, $sep = ' '){
			$unit = null;
			$units = array('B', 'KB', 'MB', 'GB', 'TB');
			 
			for($i = 0, $c = count($units); $i < $c; $i++)
				{
					if ($size > 1024)
				{
					$size = $size / 1024;
				}
				else
				{
					$unit = $units[$i];
					break;
				}
			}			 
			return round($size, 2).$sep.$unit;
		}
		
		public function Round($amt, $display = TRUE) 
		{
			$amt = round($this->getValue($amt),2);
			if($display == TRUE) $amt = number_format($amt, 2, '.', '');
			return $amt;
		}
		
		public function RemovedHtmlString($str) 
		{
			$var = $this->getValue($str);
			$var = strip_tags($var);
			return $var;
		}
		
		public function getQuery($RemoveArray = array('page')) 
		{
			$get = $_GET;
			foreach($RemoveArray as $val)
			{
				if(array_key_exists($val,$get) === true)
				{
					unset($get[$val]);
				}
			}
			return http_build_query($get);
		}
		
		public function getEditor($width='600', $height='500',$fileBrowser,$linkBrowser,$REPLACE,$group){
			echo "<script type=\"text/javascript\" language=\"javascript\">
			var oEdit1 = new InnovaEditor(\"oEdit1\");
			
			oEdit1.width = ".$width.";
			oEdit1.height = ".$height.";
			
			/*Add Custom Buttons */
			oEdit1.arrCustomButtons.push([\"MyCustomButton\", \"alert('Custom Command here..')\", \"Caption..\", \"btnCustom1.gif\"]);
			
			/*Toolbar Buttons Configuration*/
			
			oEdit1.groups = [
				[\"group1\", \"\", [".$group."]]
			];
			
			/*Define \"InternalLink\" & \"CustomObject\" buttons */
			oEdit1.cmdInternalLink = \"modalDialog('".$linkBrowser."/my_custom_dialog.htm',650,350)\"; //Command to open your custom link browser.
			oEdit1.cmdCustomObject = \"modalDialog('".$linkBrowser."/my_custom_dialog.htm',650,350)\"; //Command to open your custom file browser.
			
			/*Enable Custom File Browser */
			oEdit1.fileBrowser = '".$fileBrowser."';
			
			/*Define \"CustomTag\" dropdown */
			oEdit1.arrCustomTag = [[\"First Name\", \"{%first_name%}\"],
			[\"Last Name\", \"{%last_name%}\"],
			[\"Email\", \"{%email%}\"]]; //Define custom tag selection
			
			oEdit1.returnKeyMode = 2;
			oEdit1.pasteTextOnCtrlV = true;
			oEdit1.enableCssButtons = false;
			
			oEdit1.enableFlickr = false;
			/*Render the editor*/
			oEdit1.css = \"/styles/default.css\";
			oEdit1.REPLACE('".$REPLACE."');
			</script>";	
		}
		
		
		public function getCountry($type)
		{
			$CountryData = array();
			if($type=="user"){
				$CountryData = @file("includes/country.inc");
			}
			elseif($type=="admin"){
				$CountryData = @file("../includes/country.inc");
			}	
			if(isset($CountryData) && count($CountryData)>0){
				$i=0;
				foreach($CountryData as $c){
					$country[$i]['value'] = ucwords(strtolower(trim($c)));
					$i++;
				}
			}
			return $country;
		}
		
		public function post($element) {
			return $this->setValue($_POST[$element]);
		}

		public function SubWord($str_content,$num_of_words_to_show,$symbol_to_more='.',$num_of_symbols=3) {
		   $subword_output ='';
		   $str_arr_num=0;
		   $smbl_to_mr = '';
		   $str_arr = explode(' ',$str_content);
		   for($str_arr_num=0;$str_arr_num<$num_of_words_to_show;$str_arr_num++){
			$subword_output = $subword_output.$str_arr[$str_arr_num].' ';
		   }
		   for($smbl_num=0;$smbl_num<$num_of_symbols;$smbl_num++){
			$smbl_to_mr = $smbl_to_mr.$symbol_to_more;
		   }
		   $subword_output = $subword_output.$smbl_to_mr;
		   return $subword_output; 
		}

		
	}
?>