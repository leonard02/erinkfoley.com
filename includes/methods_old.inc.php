<?php	 
	class Methods extends Functions
	{
		private $db;
		public function __construct($param)
		{
			$this->db = $param;
		}
		
		public function getSettings($fields = "*") 
		{
			$sql = "SELECT ".$fields." FROM `tbl_admin` WHERE `admin_id`=1";
			$res = $this->db->get($sql,__FILE__,__LINE__);
			return $this->db->fetch_array($res);
		}
		
		public function getAdditionalImage($pid)
		{
			$sql = "SELECT * FROM `tbl_product_additional_image` WHERE `product_id`=".$pid;
			$res = $this->db->get($sql,__FILE__,__LINE__);
			if($this->db->num_rows($res) > 0)
			{
				$array = array("result" => $res, "record" => $this->db->num_rows($res));
			}
			else
			{
				$array = array("result" => NULL, "record" => 0);
			}
			return $array;
		}
		
		public function getStaticContent($page_title, $trail)
		{
			$sql = "SELECT `page_content".$trail."` FROM `tbl_static_page_content` WHERE `page_title`='".$page_title."'";
			$res = $this->db->get($sql,__FILE__,__LINE__);
			return $this->db->result($res,0,0);
		}
		
		public function UpdateCart($cid)
		{
			$sql = "UPDATE `tbl_cart` SET `customer_id`=".$cid." WHERE `session_id`='".session_id()."' AND `status`='Inactive'";
			$this->db->get($sql,__FILE__,__LINE__);
		}
		
		public function getPartner()
		{
			$sql = "SELECT * FROM `tbl_partner` ORDER BY RAND()";
			$res = $this->db->get($sql,__FILE__,__LINE__);
			return $res;
			/*$partner = array();
			while($row = $this->db->fetch_array($res))
			{
				$partner[] = array("website" => $row['website'], "image" => $row['image']);
			}
			return json_encode($partner);*/
		}
		
		public function getCustomer($cid)
		{
			$sql = "SELECT * FROM `tbl_customer` WHERE `customer_id`=".$cid;
			$res = $this->db->get($sql,__FILE__,__LINE__);
			return $this->db->fetch_array($res);
		}
		
		public function getProduct($pid)
		{
			$sql = "SELECT * FROM `tbl_product` WHERE `product_id`=".$pid;
			$res = $this->db->get($sql,__FILE__,__LINE__);
			return $this->db->fetch_array($res);
		}
		
		public function getProductPrice($pid, $qty)
		{
			$sql = "SELECT `price` FROM `tbl_product_qty_price` WHERE `product_id`=".$pid." AND `quantity`=".$qty;
			$res = $this->db->get($sql,__FILE__,__LINE__);
			return $this->db->result($res,0,0);
		}
		
		public function getCart()
		{
			$sql = "SELECT COUNT(*) FROM `tbl_cart` WHERE `session_id`='".session_id()."' AND `status`='Inactive'";
			$res = $this->db->get($sql,__FILE__,__LINE__);
			return $this->db->result($res,0,0);
		}
		
		public function getGallery($result, $lang)
		{
			$pid = $result['product_id'];
			$pname = $this->getValue($result['artist'.$lang['lang']]." - ".$result['name_work'.$lang['lang']]);
			$html = '<table width="300" cellspacing="0" cellpadding="0" border="0">
						<tr>
							<td class="galleryboder" style="padding:5px;"><table width="100%" cellspacing="0" cellpadding="0">
								<tr>
									<td width="50%" valign="top"><a title="'.$pname.'" href="product_details.php?product_id='.$pid.'"><img src="makeThumbnail.php?file=uploads/'.$result['product_image'].'&width=142&height=142&sold='.$result['sold'].'" alt="'.$pname.'" width="142" height="142" border="0" /></a></td>
									<td width="4%">&nbsp;</td>
									<td width="46%" valign="top"><table width="100%" cellspacing="0" cellpadding="0">
										<tr>
											<td>'.$lang['artist'].':<br /><span class="footermenulink">'.$this->getValue($result['artist'.$lang['lang']]).'</span></td>
										</tr>
										<tr>
											<td height="10"></td>
										</tr>
										<tr>
											<td>'.$lang['name_work'].':<br /><span class="footermenulink">'.$this->getValue($result['name_work'.$lang['lang']]).'</span></td>
										</tr>
										<tr>
											<td height="10"></td>
										</tr>
										<tr>
											<td>'.$lang['price'].':<br /><span class="footermenulink">&euro;'.$result['product_price'].'</span></td>
										</tr>
									</table></td>
								</tr>
							</table></td>
						</tr>';
			$btn = ($result['sold'] == "Yes") ? '&nbsp;' : '<input id="btn'.$pid.'" type="button" value="'.$lang['add_to_basket'].'" class="btn btnAddCart" />';
			$html.='	<tr>
							<td height="40" align="center" bgcolor="#6fa8d6">'.$btn.'</td>
						</tr>
					</table>';
			return $html;
		}
		
		public function getDisplayProduct($result_array, $lang)
		{
			$pid = $result_array['product_id'];
			$pname = $this->getValue($result_array['artist'.$lang['lang']]." - ".$result_array['name_work'.$lang['lang']]);
			$html = '<table width="170" align="center" cellpadding="0" cellspacing="0">
						<tr>
							<td align="center"><a title="'.$pname.'" href="product_details.php?product_id='.$pid.'"><img src="makeThumbnail.php?file=uploads/'.$result_array['product_image'].'&width=159&height=170&sold='.$result_array['sold'].'" alt="'.$pname.'" width="159" height="171" border="0" /></a></td>
						</tr>
						<tr>
							<td><table width="168" cellspacing="0" cellpadding="0">
								<tr>
									<td height="20" align="center">'.$lang['artist'].': '.$this->getValue($result_array['artist'.$lang['lang']]).'</td>
								</tr>
								<tr>
									<td height="20" align="center"><span class="green">'.$lang['price'].':</span> <span class="orange" style="font-size:18px;">&euro;'.$result_array['product_price'].'</span></td>
								</tr>
							</table></td>
						</tr>
						<tr>
							<td height="10"></td>
						</tr>
					</table>';
			return $html;
		}
	}
?>