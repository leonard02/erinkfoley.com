<?php	 
	/*
		Example
		=================================
		require('your_path/paypal.inc.php');
		$objPaypal = new Paypal();
		$objPaypal->IsAutoSubmit = true;
		$objPaypal->FormName = "frm";
		$objPaypal->LoginEmail = "aa@xyz.com";
		$objPaypal->Description = "Payment For Shopping Cart";
		$objPaypal->PaypalBgColor = "921548";
		$objPaypal->CurrencyCode = "USD";
		$objPaypal->ReturnURL = "Your website url path after successful transaction";
		$objPaypal->Amount = "1000";
		$objPaypal->InvoiceNumber = "QWERWER5454WER54WER54";
		$objPaypal->CancelReturnURL = "Your website url path after non successful transaction or canceled transaction";
		echo $objPaypal->CreateHTML();
		=================================
	*/
	class Paypal {
		var $IsAutoSubmit = true; // Automatic Submit the form or not
		var $FormName; // HTML Form Name
		var $FirstName;
		var $LastName;
		var $Address1;
		var $Address2;
		var $City;
		var $State;
		var $Country;
		var $Zip;
		var $H_PhoneNumber;
		var $Email;
		var $LoginEmail; // Client Paypal Email
		var $Description; // Description of the transaction
		var $PaypalBgColor = ""; // Background color of paypal if you want to display
		var $PaypalHeaderImage = "";
		var $PaypalHeaderBackColor = "";
		var $PaypalHeaderBorderColor = "";
		var $CurrencyCode = ""; // Currency Code. Default is USD
		var $ReturnURL; // Website url path after successful transaction
		var $Amount; // Amount
		var $Tax; // Tax
		var $InvoiceNumber; // Invoice Number what will mail to both. User as well as Site Owner
		var $CancelReturnURL; // Website url path after non successful transaction or canceled transaction
		var $ConfirmationButtonText = ""; // Website url path after non successful transaction or canceled transaction
		
		function CreateHTML() {
			$str = "<form action=\"https://www.paypal.com/cgi-bin/webscr\" method=\"post\" name=\"".$this->FormName."\">\n";
			//$str = "<form action=\"https://www.sandbox.paypal.com\" method=\"post\" name=\"".$this->FormName."\">\n";
			$str.= "<input type=\"hidden\" name=\"cmd\" value=\"_xclick\">\n";
			$str.= "<input type=\"hidden\" name=\"first_name\" value=\"".$this->FirstName."\">\n";
			$str.= "<input type=\"hidden\" name=\"last_name\" value=\"".$this->LastName."\">\n";
			$str.= "<input type=\"hidden\" name=\"address1\" value=\"".$this->Address1."\">\n";
			$str.= "<input type=\"hidden\" name=\"address2\" value=\"".$this->Address2."\">\n";
			$str.= "<input type=\"hidden\" name=\"city\" value=\"".$this->City."\">\n";
			$str.= "<input type=\"hidden\" name=\"state\" value=\"".$this->State."\">\n";
			$str.= "<input type=\"hidden\" name=\"country\" value=\"".$this->Country."\">\n";
			$str.= "<input type=\"hidden\" name=\"zip\" value=\"".$this->Zip."\">\n";
			$str.= "<input type=\"hidden\" name=\"H_PhoneNumber\" value=\"".$this->H_PhoneNumber."\">\n";
			$str.= "<input type=\"hidden\" name=\"email\" value=\"".$this->Email."\">\n";
			$str.= "<input type=\"hidden\" name=\"business\" value=\"".$this->LoginEmail."\">\n";
			$str.= "<input type=\"hidden\" name=\"item_name\" value=\"".$this->Description."\">\n";
			if(empty($this->PaypalBgColor)==false) {
				$str.= "<input type=\"hidden\" name=\"cpp_payflow_color\" value=\"".$this->PaypalBgColor."\">\n";
			}
			if(empty($this->PaypalHeaderImage)==false) {
				$str.= "<input type=\"hidden\" name=\"cpp_header_image\" value=\"".$this->PaypalHeaderImage."\">\n";
			}
			if(empty($this->PaypalHeaderBackColor)==false) {
				$str.= "<input type=\"hidden\" name=\"cpp_headerback_color\" value=\"".$this->PaypalHeaderBackColor."\">\n";
			}
			if(empty($this->PaypalHeaderBorderColor)==false) {
				$str.= "<input type=\"hidden\" name=\"cpp_headerborder_color\" value=\"".$this->PaypalHeaderBorderColor."\">\n";
			}
			if(empty($this->ConfirmationButtonText)==false) {
				$str.= "<input type=\"hidden\" name=\"cbt\" value=\"".$this->ConfirmationButtonText."\">\n";
			}
			$str.= "<input type=\"hidden\" name=\"currency_code\" value=\"".$this->CurrencyCode."\">\n";
			$str.= "<input type=\"hidden\" name=\"rm\" value=\"2\">\n";
			$str.= "<input type=\"hidden\" name=\"return\" value=\"".$this->ReturnURL."\">\n";
			$str.= "<input type=\"hidden\" name=\"amount\" value=\"".$this->Amount."\">\n";
			$str.= "<input type=\"hidden\" name=\"tax\" value=\"".$this->Tax."\">\n";
			$str.= "<input type=\"hidden\" name=\"custom\" value=\"\">\n";
			$str.= "<input type=\"hidden\" name=\"invoice\" value=\"".$this->InvoiceNumber."\">\n";
			$str.= "<input type=\"hidden\" name=\"cancel_return\" value=\"".$this->CancelReturnURL."\">\n";
			$str.= "</form>\n";
			
			if($this->IsAutoSubmit == true) {
				$str.= "<script language=\"javascript\" type=\"text/javascript\">window.document.".$this->FormName.".submit();</script>\n";
			}
			return $str;
		}
	}
?>