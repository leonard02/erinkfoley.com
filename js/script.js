/* 
 * Author: Tibor Kiray
 */

$(function() {
	if(document.location.href.indexOf('checkout') > 0){
		$("#minicart a").attr("href", "javascript:;");
	}
	<!--$("select.ddl").selectbox();-->
	
	$(".tabs .menu li").click(function(){
		
		var tabToOpen = $(this).attr('id');
		var idTokens = tabToOpen.split('-');
		var groupIdx = idTokens[idTokens.length - 1];
		$('.tab-content.group-' + groupIdx).hide();
		$('.tabs .menu li.group-' + groupIdx).removeClass("active");
		$(this).addClass("active");
		$('.tab-content.' + tabToOpen).show();
	});
	
	$(".btn-become-a-vip").click(function(){
		ShowSignupWindow();
	});
	$(".subscribe-email").click(function(){
		ProccessSignup();
	});
	/*GetCartItemsCount();
	$('a.lightbox').lightBox();*/
	$('a.btn-continue-to-checkout').click(function(){
		SubmitCartIfValid();
	});
});

function SubmitCartIfValid(){
	var jqxhr = $.get(
		'/checkout/get-cart-items-count',
		{cart: $.cookie('gcc') == null ? 0 : $.cookie('gcc')}
	);
	jqxhr.success(function(data) {
		if(data.itemsCount > 0){
			window.location.href = '/checkout/samples';
		}else{
			alert('Your cart is empty');
		}
	});
	jqxhr.error(function(data) {
		$("span.cart-items-count").html("E");
		return null;
	});
}

function ValidateCheckout(){
	var message = "Please correct the following errors:\r\n";
	var isValid = true;
	var tab = '        ';
	if($.trim($('#billingFirstName').val()) == ''){
		isValid = false;
		message += tab + "Billing First Name is required\r\n";
	}
	if($.trim($('#billingLastName').val()) == ''){
		isValid = false;
		message += tab + "Billing Last Name is required\r\n";
	}
	if($.trim($('#billingAddressLine1').val()) == ''){
		isValid = false;
		message += tab + "Billing Address Line 1 is required\r\n";
	}
	if($.trim($('#billingCity').val()) == ''){
		isValid = false;
		message += tab + "Billing City is required\r\n";
	}
	if($.trim($('#txtBillingState').val()) == '' && $.trim($('#billingState').val()) == '0'){
		isValid = false;
		message += tab + "Billing State is required\r\n";
	}
	if($.trim($('#billingCountry').val()) == '' && $.trim($('#billingCountry').val()) == '0'){
		isValid = false;
		message += tab + "Billing Country is required\r\n";
	}
	
	if(!EmailIsValid($.trim($('#billingEmail').val()))){
		isValid = false;
		message += tab + "Email address is not valid\r\n";
	}else{
		if($.trim($('#billingEmail').val()) == ''){
			isValid = false;
			message += tab + "Email address is required\r\n";
		}
		
		if($.trim($('#billingConfirmEmail').val()) == ''){
			isValid = false;
			message += tab + "Email confirmation is required\r\n";
		}
		
		if($.trim($('#billingEmail').val()) != $.trim($('#billingConfirmEmail').val())){
			isValid = false;
			message += tab + "Emails don't match\r\n";
		}
	}
	
	if($.trim($('#shippingFirstName').val()) == ''){
		isValid = false;
		message += tab + "Shipping First Name is required\r\n";
	}
	if($.trim($('#shippingLastName').val()) == ''){
		isValid = false;
		message += tab + "Shipping Last Name is required\r\n";
	}
	if($.trim($('#shippingAddressLine1').val()) == ''){
		isValid = false;
		message += tab + "Shipping Address Line 1 is required\r\n";
	}
	if($.trim($('#shippingCity').val()) == ''){
		isValid = false;
		message += tab + "Shipping City is required\r\n";
	}
	if($.trim($('#txtShippingState').val()) == '' && $.trim($('#shippingState').val()) == '0'){
		isValid = false;
		message += tab + "Shipping State is required\r\n";
	}
	if($.trim($('#shippingCountry').val()) == '' && $.trim($('#shippingCountry').val()) == '0'){
		isValid = false;
		message += tab + "Shipping Country is required\r\n";
	}
	
	
	
	if($.trim($('#ccNumber').val()) == ''){
		isValid = false;
		message += tab + "Credit card number is required\r\n";
	}
	if($.trim($('#ccv').val()) == ''){
		isValid = false;
		message += tab + "ccv is required\r\n";
	}
	
	if(!isValid){
		alert(message);
	}
	return isValid;
}

function EmailIsValid(email){
	 var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
	 if(reg.test(email) == false){
		 return false;
	 }
	 return true;
}

function ProccessSignup(){
	var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
	var address = $("#emailToSubscribe").val();
	if(reg.test(address) == false) {
		alert('Invalid Email Address');
		return false;
	}
	var jqxhr = $.get(
		"/index/signup", 
		{ 
			email: $("#emailToSubscribe").val() 
		}
	);
	jqxhr.success(function(data) {
		if(data.code != 0){
			$("#email_signup_form").submit();
		}else{
			alert("Already Subscribed");
		}
	});
}

function GetCartItemsCount(){
	var jqxhr = $.get(
		'/checkout/get-cart-items-count',
		{cart: $.cookie('gcc') == null ? 0 : $.cookie('gcc')}
	);
	jqxhr.success(function(data) {
		$("span.cart-items-count").html(data.itemsCount);
		return data.itemsCount;
	});
	jqxhr.error(function(data) {
		$("span.cart-items-count").html("E");
		return null;
	});
	
}

function ShowSignupWindow(){
	$(".sel-signup").html(
		"<h2 style='margin-bottom: 10px;'>VIP signup</h2>" +
		"<label for='vipEmail'>email:</label><br />" +
		"<input type='text' name='vip-email' id='vipEmail' /><br />" +
		"<label for='vipPassword'>password:</label><br />" +
		"<input type='password' name='vip-password' id='vipPassword' /><br />" +
		"<label for='vipConfirmPassword'>confirm password:</label><br />" +
		"<input type='password' name='vip-confirm-password' id='vipConfirmedPassword' /><br />" +
		"<br />" +
		"<input type='image' src='/img/layout/btn-vip-signup.gif' onclick='VipSignup()' />"
	);
	$(".sel-signup").dialog({modal: true});
}

function VipSignup(){
	var jqxhr = $.get(
		'/my-account/signup',
		{
			email: $("#vipEmail").val(),
			password: $("#vipPassword").val(),
			confirmedPassword: $("#vipConfirmedPassword").val()
		}
	);
	jqxhr.success(function(data) {
		if(data.success){
			//alert(":D");
			$(".sel-signup").html(
					"Thank you!<br />" +
					"Please check your inbox for a confirmation email in order to complete your registration.");
		}else{
			$(".sel-signup").html(
					"Sorry!<br />" + data.message + "<br /><a href='javascript:ShowSignupWindow();'>try again</a>");
		}
	});
}

function ShowResetPasswordWindow(){
	$(".sel-signup").html(
		"<h2 style='margin-bottom: 10px;'>reset VIP password</h2>" +
		"<label for='vipEmail'>email:</label><br />" +
		"<input type='text' name='vip-email' id='vipEmailToReset' /><br />" +
		"<br />" +
		"<input type='image' src='/img/layout/btn-vip-signup.gif' onclick='VipResetPassword()' />"
	);
	$(".sel-signup").dialog({modal: true});
}

function VipResetPassword(){
	var jqxhr = $.get(
		'/my-account/reset-password',
		{
			email: $("#vipEmailToReset").val()
		}
	);
	jqxhr.success(function(data) {
		if(data.success){
			//alert(":D");
			$(".sel-signup").dialog("close");
		}
	});
}

function ReloadCart(){
	var jqxhr = $.get(
		'/checkout/get-cart',
		{
			cart: $.cookie('gcc') == null ? 0 : $.cookie('gcc')
		}
	);
	jqxhr.success(function(data) {
		$('.jquery-selectbox').unselectbox();
		RenderCart(data.cart, data.samples);
		$("select.ddl").selectbox();
	});
	
}

function ShowCart()
{
	$.modal.close();
    var jqxhr = $.get(
		'/checkout/get-cart',
		{
			cart: $.cookie('gcc') == null ? 0 : $.cookie('gcc')
		}
	);
	jqxhr.success(function(data) {
		//$.unblockUI();
		RenderCart(data.cart, data.samples);
		$('.cart-box').modal({
	    	autoPosition: true,
	    	overlayClose: true,
	    	onShow: function(){
				$('select.ddl').selectbox();
				//$.unblockUI();
				$('#simplemodal-container').css("top", "90px");
				//UpdateShipping();
			}
	    });
	});
    /*
     {
        zIndex: 15000,
        closeClass: 'modal-close',
        closeHTML: "<div><img src='//assets.stevemadden.com/layout/icons/ico-close-grey.gif' alt='Close' /> Close</div>",
        overlayClose: true,
        persist: true,
        autoResize: true
    }
     */
}

//<img alt="shade" src="/assets/products/shade-luminous.gif">
var itemTemplate = 
'<div id="cart-item-{cartItemID}" class="cart-item">'
+'	<div class="shade"><span style="background-image: url(/assets/products/{shadeImage})">&nbsp;</span></div>'
+'	<div class="description">'
+'		<div class="name">{itemGroupName}</div>'
+'		<div class="shade-name">shade: {shade}</div>'
+'	</div>'
+'	<div class="price">${price}</div>'
+'	<div class="quantity">'
+'		<input type="text" name="quantity" id="quantity-{cartItemID}" style="width: 45px;" value="{quantity}" />'
+'	</div>'
+'	<div class="subtotal">${subtotal}</div>'
+'	<div class="actions"><a href="javascript:UpdateCartItem({cartItemID})">update</a> | <a href="javascript:RemoveFromCart({cartItemID})">remove</a></div>'
+'	<div class="clearfix"></div>'
+'</div>';

var cartSampleItemTemplate = 
	'<div id="cart-sample-item-{cartSampleID}" class="cart-item">'
	+'	<div class="shade"><span style="background-image: url(/assets/products/{sampleThumbImage})">&nbsp;</span></div>'
	+'	<div class="description">'
	+'		<div class="name">{itemGroupName}</div>'
	+'		<div class="shade-name">shade: {shade} (sample)</div>'
	+'	</div>'
	+'	<div class="price">${price}</div>'
	+'	<div class="quantity" style="line-height: 30px;">1'
	+'	</div>'
	+'	<div class="subtotal">${subtotal}</div>'
	+'	<div class="actions"><a href="javascript:RemoveSampleFromCart({cartSampleID})">remove</a></div>'
	+'	<div class="clearfix"></div>'
	+'</div>';

var sampleTemplate = 
'	<div id="cart-item-{sampleId}" class="cart-item recommendation">'
+'		<div class="shade"><span style="background-image: url(/assets/products/{sampleThumbImage})">&nbsp;</span></div>'
+'		<div class="description">'
+'			<div class="name">{sampleGroupName}</div>'
+'			<div class="shade-name">shade: {sampleItemName}</div>'
+'		</div>'
+'		<div class="price">${price}</div>'
+'		<div class="quantity">'
+'			<input type="text" name="quantity" style="width: 45px;" value="1" />'
+'		</div>'
+'		<div class="recommendation-actions"><a href="javascript:AddItemToCart({itemId}, $(\'#cart-item-{sampleId} input\').val());">add</a> | <!--a href="javascript:AddSampleToCart({sampleId});">try free sample</a> | --> <a href="{url}">learn more</a></div>'
+'		<div class="clearfix"></div>'
+'	</div>';
function RenderCart(cart, samples){
	var items = cart.items;
	var cartSampleItems = cart.sampleItems;
	var cartDiscounts = cart.discounts;
	var itemsHtml = "<h2 style='text-align: center; font-size: 18px;'>Your cart is empty</h2>";
	var merchandiseTotal = 0;
	if(items.length > 0){
		itemsHtml = "";
		for(i = 0; i < items.length; i++){
			//alert(items[i].item.price * items[i].quantity);
			merchandiseTotal += items[i].item.price * items[i].quantity;
			itemsHtml += format(itemTemplate, 
				{
					shadeImage		: items[i].item.item_group.name.replace(/ /gi, '-').toLowerCase() + '_shade_' + items[i].item.name.replace(/ /gi, '-').toLowerCase() + '.png',
					itemGroupName	: items[i].item.item_group.name.toLowerCase(),
					shade			: items[i].item.item_group.url.toLowerCase() != 'cheek-perfection-gel-duo' ? items[i].item.name.toLowerCase() : 'blushed and bronzed',
					price			: CurrencyFormatted(items[i].item.price),
					quantity		: items[i].quantity,
					subtotal		: CurrencyFormatted(items[i].item.price * items[i].quantity),
					cartItemID		: items[i].id
				});
		}
	}
	
	if(cartSampleItems.length > 0){
		for(i = 0; i < cartSampleItems.length; i++){
			itemsHtml += format(cartSampleItemTemplate, 
					{
						sampleThumbImage: cartSampleItems[i].sampleItem.sampleItemGroup.name.replace(/ /gi, '-').toLowerCase() + '_sample-thumb_' + cartSampleItems[i].sampleItem.name.replace(/ /gi, '-').toLowerCase() + '.png',
						itemGroupName	: cartSampleItems[i].sampleItem.sampleItemGroup.name.toLowerCase(),
						shade			: cartSampleItems[i].sampleItem.name.toLowerCase(),
						price			: CurrencyFormatted(0),
						subtotal		: CurrencyFormatted(0),
						cartSampleID	: cartSampleItems[i].id
					});
		}
	}
	
	$(".sel-cart-items").html(itemsHtml);
	
	var samplesHtml = "<div style='height: 2px; width: 605px;'></div>";
	if(samples.length > 0){
		$(".recommended-title").show();
		samplesHtml = "";
		for(i = 0; i < samples.length; i++){
			samplesHtml += format(sampleTemplate, 
					{
						sampleThumbImage: samples[i].sampleItemGroup.name.replace(/ /gi, '-').toLowerCase() + '_sample-thumb_' + samples[i].name.replace(/ /gi, '-').toLowerCase() + '.png',
						sampleGroupName	: samples[i].sampleItemGroup.name.toLowerCase(),
						sampleItemName	: samples[i].name.toLowerCase(),
						price			: CurrencyFormatted(samples[i].item.price),
						url				: samples[i].sampleItemGroup.name.replace(/ /gi, '-').toLowerCase(),
						itemId			: samples[i].item.id,
						sampleId		: samples[i].id
					});
		}
	}else{
		$(".recommended-title").hide();
	}
	
	$(".cart-recommended-items").html(samplesHtml);
	
	var discountAmount = 0;
	if(cartDiscounts.length > 0){
		if(cartDiscounts[0].type == 0){
			discountAmount = merchandiseTotal * cartDiscounts[0].amount;
		}else{
			discountAmount = cartDiscounts[0].amount;
		}
		var discountsHtml = 
				(cartDiscounts[0].type == 0 ? cartDiscounts[0].amount * 100 + "%" : "$" + CurrencyFormatted(cartDiscounts[0].amount)) +
				" " + cartDiscounts[0].description + " discount code: " + cartDiscounts[0].code + "<br />" +
				"You are saving $" + CurrencyFormatted(discountAmount);
		$(".applied-discount").html(discountsHtml);
	}
	if(cart.shipping == null){
		$("#shippingPrice").val(1);
	}else{
//		if(cart.shipping > 4){
//			$("#shippingMethod").val(2);
//		}
		$("#shippingPrice").val(cart.shipping);
	}
	$(".cart-total").html(CurrencyFormatted(merchandiseTotal + GetShippingAmount() - discountAmount));
	$(".merchandise-total").html(CurrencyFormatted(merchandiseTotal));
}


function ShowShadeDetails(id){
	if(id == 0){
		$('.product .presentation .item-details-container .item-details').fadeOut();
		$('.product .presentation img.item-image').fadeIn();
	}else{
		$('.product .presentation img.item-image').fadeOut();
		$('.product .presentation .item-details-container .item-details').fadeOut();
	}
	$('.ddl.sel-item').val(id);
	$('.jquery-selectbox').unselectbox();
	$("select.ddl").selectbox();
	$('.shade').removeClass('current');
	$('#item-' + id).addClass('current');
	var jqxhr = $.get(
			'/product',
			{item: id}
			);
	jqxhr.success(function(data) { 
		var item = data.item;
		var labelImage = '/assets/products/' + item.item_group.name.replace(/ /gi, '-').toLowerCase() + '_label_' + data.item.name.toLowerCase() + '.png';
		var baImage = '/assets/products/' + item.item_group.name.replace(/ /gi, '-').toLowerCase() + '_before-after_' + data.item.name.toLowerCase() + '.png';
		$('.shade-label').css('background-image', 'url(' + labelImage + ')');
		$('.item-details-ba').attr('src', baImage);
		$('.shade-label').html("<h2>" + data.item.name.toLowerCase() + "</h2><h3>" + data.item.description + "</h3>");
		$('.product .presentation .item-details-container .item-details').fadeIn();
	});
}

function ShowCheekDuoShadeDetails(id, uniqueId){
	if(id == 0){
		$('.product .presentation .item-details-container .item-details').fadeOut();
		$('.product .presentation img.item-image').fadeIn();
	}else{
		$('.product .presentation img.item-image').fadeOut();
		$('.product .presentation .item-details-container .item-details').fadeOut();
	}
	$('.ddl.sel-item').val(uniqueId);
	$('.jquery-selectbox').unselectbox();
	$("select.ddl").selectbox();
	$('.shade').removeClass('current');
	$('#item-' + id).addClass('current');
	var jqxhr = $.get(
			'/product',
			{item: id}
			);
	jqxhr.success(function(data) { 
		var item = data.item;
		var labelImage = '/assets/products/' + item.item_group.name.replace(/ /gi, '-').toLowerCase() + '_label_' + data.item.name.toLowerCase() + '.png';
		var baImage = '/assets/products/' + item.item_group.name.replace(/ /gi, '-').toLowerCase() + '_before-after_' + data.item.name.toLowerCase() + '.png';
		$('.shade-label').css('background-image', 'url(' + labelImage + ')');
		$('.item-details-ba').attr('src', baImage);
		$('.shade-label').html("<h2>" + data.item.name.toLowerCase() + "</h2><h3>" + data.item.description + "</h3>");
		$('.product .presentation .item-details-container .item-details').fadeIn();
	});
}


var format = function (str, col) {
    col = typeof col === 'object' ? col : Array.prototype.slice.call(arguments, 1);

    return str.replace(/\{\{|\}\}|\{(\w+)\}/g, function (m, n) {
        if (m == "{{") { return "{"; }
        if (m == "}}") { return "}"; }
        return col[n];
    });
};

//String.prototype.format = function (col) {return format(this,col);}

function CurrencyFormatted(amount)
{
	var i = parseFloat(amount);
	if(isNaN(i)) { i = 0.00; }
	var minus = '';
	if(i < 0) { minus = '-'; }
	i = Math.abs(i);
	i = parseInt((i + .005) * 100);
	i = i / 100;
	s = new String(i);
	if(s.indexOf('.') < 0) { s += '.00'; }
	if(s.indexOf('.') == (s.length - 2)) { s += '0'; }
	s = minus + s;
	return s;
}

(function($) {
	$.extend({
		AddToCart: function(source_id, target_id, item, quantity, callback) {
			if(item == 0 || quantity == 0){
				$(".select-warning").html("Please select a shade and quantity");
				$(".select-warning").dialog({ 
					modal: true,
					resizable: false,
					buttons: [
				    {
				        text: "Ok",
				        click: function() { $(this).dialog("close"); }
				    }
				] });
				return;
			}
			var source = $('#' + source_id );
			var target = $('#' + target_id );
			
			var shadow = $('#' + source_id + '_shadow');
			if( !shadow.attr('id') ) {
				$('body').prepend('<div id="'+source.attr('id')+'_shadow" style="display: none; background-color: #ddd; border: solid 1px darkgray; position: static; top: 0px; z-index: 100000;">&nbsp;</div>');
				var shadow = $('#'+source.attr('id')+'_shadow');
			}

			if( !shadow ) {
				alert('Cannot create the shadow div');
			}

			shadow.width(source.css('width')).height(source.css('height')).css('top', source.offset().top).css('left', source.offset().left).css('opacity', 0.5).show();
			shadow.css('position', 'absolute');

			shadow.animate({
				width: target.innerWidth(),
				height: target.innerHeight(),
				top: target.offset().top,
				left: target.offset().left
			}, {
				duration: 500
			} )
			.animate({
				opacity: 0
			}, {
				duration: 100,
				complete: function(){
					$('#itemSelector_shadow').remove();
					// $.blockUI({ 
            			// message:  '<p>Loading cart...</p>',
            			// overlayCSS:  {
            				// backgroundColor: '#FFFFFF'
            			// }
		            // }); 
        			$('.blockOverlay').attr('title','Click to unblock').click($.unblockUI);
				}
			} );
			
			var jqxhr = $.get(
				'/product/add-to-cart',
				{
					item: item,
					quantity: quantity,
					cart: $.cookie('gcc') == null ? 0 : $.cookie('gcc')
				}
			);
			jqxhr.success(function(data) {
				$.cookie('gcc', data.cart.id, { expires: 14, path: '/' });
				GetCartItemsCount();
				ShowCart();
			});
		}
	});
})(jQuery);

function  RemoveFromCart(id){
	var jqxhr = $.get(
		'/product/remove-from-cart',
		{
			cartItemId: id,
			cart: $.cookie('gcc') == null ? 0 : $.cookie('gcc')
		}
	);
	jqxhr.success(function(data) {
		if(data > 0){
			$('#cart-item-' + id).remove();
			GetCartItemsCount();
			ReloadCart();
		}
	});
}

function AddItemToCart(item, quantity){
	var jqxhr = $.get(
		'/product/add-to-cart',
		{
			item: item,
			quantity: quantity,
			cart: $.cookie('gcc') == null ? 0 : $.cookie('gcc')
		}
	);
	jqxhr.success(function(data) {
		$.cookie('gcc', data.cart.id, { expires: 14, path: '/' });
		GetCartItemsCount();
		ReloadCart();
		//ShowCart();
	});
}

function AddSampleToCart(sampleId){
	var jqxhr = $.get(
		'/product/add-sample-to-cart',
		{
			item: sampleId,
			cart: $.cookie('gcc') == null ? 0 : $.cookie('gcc')
		}
	);
	jqxhr.success(function(data) {
		//$.cookie('gcc', data.cart.id, { expires: 14, path: '/' });
		ReloadCart();
	});
}

function RemoveSampleFromCart(id){
	var jqxhr = $.get(
		'/product/remove-sample-from-cart',
		{
			sampleItemId: id,
			cart: $.cookie('gcc') == null ? 0 : $.cookie('gcc')
		}
	);
	jqxhr.success(function(data) {
		if(data > 0){
			$('#cart-sample-item-' + id).remove();
		}
	});
}

function UpdateCartItem(cartItemID){
	var quantity = $('#quantity-' + cartItemID).val();
	if(parseInt(quantity) != NaN && parseInt(quantity) > 0){
	var jqxhr = $.get(
		'/product/update-cart',
		{
			cartItemId: cartItemID,
			quantity: quantity,
			cart: $.cookie('gcc') == null ? 0 : $.cookie('gcc')
		}
	);
	 jqxhr.success(function(cart) {
		 GetCartItemsCount();
		 ReloadCart();
//		 var items = cart.items;
//		 var merchandiseTotal = 0;
//		 if(items.length > 0){
//			 for(i = 0; i < items.length; i++){
//				 merchandiseTotal += items[i].item.price * items[i].quantity;
//			 }	
//		 }
//		 $(".cart-total").html(CurrencyFormatted(merchandiseTotal + GetShippingAmount()));
//		 $(".merchandise-total").html(CurrencyFormatted(merchandiseTotal));
	 });
	}else{
		alert("Quantity must be an integer greater than 0!");
	}
}

$(function(){
	$("select.ddl").selectbox();
	$('#billingCountry').change(function(){
		ToggleBillingStates();
	});
	
	$('.sel-billing-address-list').change(function(){
		populateBillingAddress();
	});
	$('.sel-shipping-address-list').change(function(){
		populateShippingAddress();
	});
});
function SameAsBilling_Click(){
	if($("#sameAsBilling").is(':checked')){

		$('#shippingFirstName').val($('#billingFirstName').val());
		$('#shippingLastName').val($('#billingLastName').val());
				
		$('#shippingAddressLine1').val($('#billingAddressLine1').val());
		$('#shippingAddressLine2').val($('#billingAddressLine2').val());
		$('#shippingCountry').parents('.jquery-selectbox').unselectbox();
		$('#shippingCountry').val($('#billingCountry').val());
		$('#shippingCountry').selectbox();
		
		$('#shippingState').parents('.jquery-selectbox').unselectbox();
		$('#shippingState').val($('#billingState').val());
		$('#shippingState').selectbox();
		ToggleShippingStates();
		if($('#shippingCountry').val() != 232){
			$('#txtShippingState').val($('#txtBillingState').val());
		}else{
			$('#shippingState').parents('.jquery-selectbox').unselectbox();
			$('#shippingState').val($('#billingState').val());
			$('#shippingState').selectbox();
		}
		$('#shippingCity').val($('#billingCity').val());
		$('#shippingZipCode').val($('#billingZipCode').val());
		
		$('#shippingCountry').change(function(){
			ToggleShippingStates();
		});
	}
}
function populateShippingAddress(){
	var addressId = $('.sel-shipping-address-list').val();
	for(i = 0; i < jsonAddressList.length; i++){
		var address = jsonAddressList[i];
		if(address.id == addressId){
			
			$('#shippingFirstName').val(userFirstName);
			$('#shippingLastName').val(userLastName);
			
			$('#shippingAddressLine1').val(address.address1);
			$('#shippingAddressLine2').val(address.address2);
			$('#shippingCountry').parents('.jquery-selectbox').unselectbox();
			$('#shippingCountry').val(address.country.id);
			$('#shippingCountry').selectbox();
			ToggleShippingStates();
			if($('#shippingCountry').val() != 232){
				$('#txtShippingState').val(address.state);
			}else{
				$('#shippingState').parents('.jquery-selectbox').unselectbox();
				$('#shippingState').val(address.state);
				$('#shippingState').selectbox();
			}
			$('#shippingCity').val(address.city);
			$('#shippingZipCode').val(address.zip);
			
			$('#shippingCountry').change(function(){
				ToggleShippingStates();
			});
		}
	}
}

function populateBillingAddress(){
	var addressId = $('.sel-billing-address-list').val();
	for(i = 0; i < jsonAddressList.length; i++){
		address = jsonAddressList[i];
		if(address.id == addressId){
			
			$('#billingFirstName').val(userFirstName);
			$('#billingLastName').val(userLastName);
			$('#billingEmail').val(userEmail);
			$('#billingConfirmEmail').val(userEmail);
			$('#billingAddressLine1').val(address.address1);
			$('#billingAddressLine2').val(address.address2);
			$('#billingCountry').parents('.jquery-selectbox').unselectbox();
			$('#billingCountry').val(address.country.id);
			$('#billingCountry').selectbox();
			ToggleBillingStates();
			if($('#billingCountry').val() != 232){
				$('#txtBillingState').val(address.state);
			}else{
				$('#billingState').parents('.jquery-selectbox').unselectbox();
				$('#billingState').val(address.state);
				$('#billingState').selectbox();
			}
			$('#billingCity').val(address.city);
			$('#billingZipCode').val(address.zip);
			
			$('#billingCountry').change(function(){
				ToggleBillingStates();
			});
		}
	}
}

function ToggleBillingStates(){
	var countryId = $('#billingCountry').val();
	if(countryId == 232){
		$('.billing-state-wrapper').show();
		$('#billingState').removeAttr('disabled');
		$('#billingState').val(0);
		$('#billingState').selectbox();
		$('#txtBillingState').hide();
		$('#txtBillingState').attr('disabled', 'disabled');
	}else{
		$('#txtBillingState').show();
		$('#billingState').attr('disabled', 'disabled');
		$('.billing-state-wrapper').hide();
		$('#txtBillingState').removeAttr('disabled');
		$('#txtBillingState').val();
	}
}

function ToggleShippingStates(){
	var countryId = $('#shippingCountry').val();
	if(countryId == 232){
		$('.shipping-state-wrapper').show();
		$('#shippingState').removeAttr('disabled');
		$('#shippingState').val(0);
		$('#shippingState').selectbox();
		$('#txtShippingState').hide();
		$('#txtShippingState').attr('disabled', 'disabled');
	}else{
		$('#txtShippingState').show();
		$('#shippingState').attr('disabled', 'disabled');
		$('.shipping-state-wrapper').hide();
		$('#txtShippingState').removeAttr('disabled');
		$('#txtShippingState').val();
	}
}

var editEmailTempalte = 
	'<label>new email</label>' +
	'<input type="text" name="email" id="email" />' +
	'<label>confirm email</label>' +
	'<input type="text" name="confirmEmail" id="confirmEmail" />' +
	'<input type="button" value="save" name="saveEmail" onclick="SaveEmail();" /> ' +
	'<input type="button" value="cancel" name="cancelEditEmail" onclick="CancelEditEmail();" />';
function EditEmail(){
	$(".account-tab-content .basic .email a.trigger").hide();
	$(".account-tab-content .basic .email .editor").html(editEmailTempalte);
}
function SaveEmail(){
	var jqxhr = $.get(
		'/my-account/save-email',
		{
			email: $('#email').val()
		}
	);
	jqxhr.success(function(data) {
		if(data.success == true){
			$(".account-tab-content .basic .message").html("Email updated");
			$(".account-tab-content .basic .email a.trigger").show();
			$(".account-tab-content .basic .email .editor").html("");
		}else{
			$(".account-tab-content .basic .message").html("Email update error");
		}
	});
	jqxhr.error(function(data) {
		$(".account-tab-content .basic .message").html("Email update error");
	});
}
function CancelEditEmail(){
	$(".account-tab-content .basic .email a.trigger").show();
	$(".account-tab-content .basic .email .editor").html("");
}

var editPasswordTempalte = 
	'<label>new password</label>' +
	'<input type="password" name="password" id="password" />' +
	'<label>confirm password</label>' +
	'<input type="password" name="confirmPassword" id="confirmPassword" />' +
	'<input type="button" value="save" name="savePassword" onclick="SavePassword();" /> ' +
	'<input type="button" value="cancel" name="cancelEditPassword" onclick="CancelEditPassword();" />';
function EditPassword(){
	$(".account-tab-content .basic .password a.trigger").hide();
	$(".account-tab-content .basic .password .editor").html(editPasswordTempalte);
}
function SavePassword(){
	var jqxhr = $.get(
		'/my-account/save-password',
		{
			password: $('#password').val()
		}
	);
	jqxhr.success(function(data) {
		if(data.success == true){
			$(".account-tab-content .basic .message").html("Password updated");
			$(".account-tab-content .basic .password a.trigger").show();
			$(".account-tab-content .basic .password .editor").html("");
		}else{
			$(".account-tab-content .basic .message").html("Password update error");
		}
	});
	jqxhr.error(function(data) {
		$(".account-tab-content .basic .message").html("Password update error");
	});
}

function CancelEditPassword(){
	$(".account-tab-content .basic .password a.trigger").show();
	$(".account-tab-content .basic .password .editor").html("");
}

function SetShippingDestination(){
	var method = $("#shippingMethod").val();
	switch(method[0]){
		case "1":
			$("#shippingPrice").html(
				'<option value="1" selected="selected">standard (5 - 7 days) - $7.95</option>' +
				'<option value="2">2nd day (3 - 4 days) - $14.95</option>' +
				'<option value="3">overnight (2 - 3 days) - $17.95</option>');
			break;
		case "2":
			$("#shippingPrice").html(
				'<option value="4" selected="selected">US First Class - $19.95</option>' +
				'<option value="5">US Priority Mail - $34.95</option>');
			break;
	}
	UpdateShipping();
	//ReloadCart();
}

function GetShippingAmount(){
	var method = $("#shippingMethod").val();
	var price = $("#shippingPrice").val();
	if(price != null)
	switch(price[0]){
		case "1": return 7.95;
			break;
		case "2": return 14.95;
			break;
		case "3": return 17.95;
			break;
		case "4": return 19.95;
			break;
		case "5": return 34.95;
			break;
	}
}

function EditAdvancedInfo(){
	$(".advanced .trigger.save-advanced").show();
	$(".advanced .trigger.start-edit").hide();

	$(".editFirstName").html('<input type="text" name="editFirstName" id="editFirstName" value="' + $(".editFirstName").html() +'" />');
	$(".editLastName").html('<input type="text" name="editLastName" id="editLastName" value="' + $(".editLastName").html() +'" />');
	
	var currentTime = new Date();
	var month = currentTime.getMonth() + 1;
	var day = currentTime.getDate();
	var year = currentTime.getFullYear();
	
	var birthDate = $(".editBirthDate").html() != "" ? $(".editBirthDate").html() :  (month + '/' + day + '/' + year) ;
	$(".editBirthDate").html('<input type="text" name="editBirthDate" id="editBirthDate" value="' + birthDate +'" />');
	//$(".editGender").html('<input type="text" name="editGender" id="editGender" value="' + $(".editGender").html() +'" />');
	var gender = $(".editGender").html();
	$(".editGender").html(
			'<select name="editGender" id="editGender">' +
			'	<option value="0">female</option>' +
			'	<option value="1">male</option>' +
			'</select>'
	);
	// value="' + $(".editGender").html() +'" /
//	$( "#editBirthDate" ).datepicker();
//	$( "#editBirthDate" ).datepicker( "option", "altFormat", 'mm-dd-yyyy' );
	$( "#editGender").val(gender == 'female' ? 0 : 1);
	
	//alert($('#editBirthDate').val());
	$( "#editBirthDate" ).DatePicker({
		format:'m/d/Y',
		date: $('#editBirthDate').val(),
		current: $('#editBirthDate').val(),
		starts: 1,
		position: 'right',
		onBeforeShow: function(){
			$('#editBirthDate').DatePickerSetDate($('#editBirthDate').val(), true);
		},
		onChange: function(formated, dates){
			$('#editBirthDate').val(formated);
			//if ($('#closeOnSelect input').attr('checked')) {
				//$('#editBirthDate').DatePickerHide();
			//}
		}
	});
}

function SaveAdvancedInfo(){
	var jqxhr = $.get(
		'/my-account/save-advanced-info',
		{
			firstName:	$("#editFirstName").val(),
			lastName:	$("#editLastName").val(),
			birthDate:	$("#editBirthDate").val(),
			gender:		$("#editGender").val()
		}
	);
	jqxhr.success(function(data) {
		if(data.success == true){
			$(".advanced .trigger.save-advanced").hide();
			$(".advanced .trigger.start-edit").show();
			$(".editFirstName").html($("#editFirstName").val());
			$(".editLastName").html($("#editLastName").val());
			$(".editBirthDate").html($("#editBirthDate").val());
			$(".editGender").html($("#editGender").val() == 0 ? 'female' : 'male');
			$(".account-tab-content .advanced .message").html("Information updated");
		}else{
			$(".account-tab-content .advanced .message").html("Information update error");
		}
	});
	jqxhr.error(function(data) {
		$(".account-tab-content .advanced .message").html("Information update error");
	});
}

function AddDiscount(){
	var discountCode = $("#promoCode").val();
	if(discountCode != 'promo code'){
		var jqxhr = $.get(
			'/checkout/add-discount',
			{
				discountCode:	discountCode,
				cart:			$.cookie('gcc') == null ? 0 : $.cookie('gcc')
			}
		);
		
		jqxhr.success(function(data) {
			if(data.success){
				ReloadCart();
			}else{
				$(".applied-discount").html("<span style='color: #FF3333;'>Discount code is not valid or has expired</span>");
			}
		});
	}
}

function UpdateShipping(){
		var shipping = 	$("#shippingPrice").val();
		var shippingMethod = shipping[0];
		var jqxhr = $.get(
			'/checkout/update-shipping',
			{
				shippingMethod:	shippingMethod,
				cart:			$.cookie('gcc') == null ? 0 : $.cookie('gcc')
			}
		);
		jqxhr.success(function(data) {
			//ReloadCart();
			if(data.success){
				ReloadCart();
			}else{
				//$(".applied-discount").html("<span style='color: #FF3333;'>Discount code is not valid or has expired</span>");
			}
		});
}

function RenderAddressForm(){
	$(".sel-address-form").dialog({
		modal: true,
		open: function(){
			$('#addressId').val("0");
			$('#name').val("");
			$('#addressLine1').val("");
			$('#addressLine2').val("");
			$('#city').val("");
			$('#state').val("");
			$('#zip').val("");
			$('#countryDdl').val('232');
		},
		close: function(){
			$('#addressId').val("0");
			$('#name').val("");
			$('#addressLine1').val("");
			$('#addressLine2').val("");
			$('#city').val("");
			$('#state').val("");
			$('#zip').val("");
			$('#countryDdl').val('232');
		}
	});
	$('#countryDdl').val('232');
	$('#countryDdl').parents('.jquery-selectbox').unselectbox();
	$('#countryDdl').selectbox();
}


var addressItemTemplate = 
	"<div class='address section' id='address-{addressId}'>" +
	"	<h2>{addressName}</h2>" +
	"	<div class='row'>" +
	"		<label>address line 1 </label><span>{address1}</span>" +
	"	</div>" +
	"	<div class='row'>" +
	"		<label>address line 2 </label><span>{address2}</span>" +
	"	</div>" +
	"	<div class='row'>" +
	"		<label>city </label><span>{city}</span>" +
	"	</div>" +
	"	<div class='row'>" +
	"		<label>state </label><span>{state}</span>" +
	"	</div>" +
	"	<div class='row'>" +
	"		<label>zip/postal code </label><span>{zip}</span>" +
	"	</div>" +
	"	<div class='row'>" +
	"		<label>country </label><span>{country}</span>" +
	"		<a href='javascript:EditAddress({addressId});' class='trigger'>edit</a>" +
	"		<a href='javascript:DeleteAddress({addressId});' class='trigger'>delete</a>" +
	"	</div>" +
	"</div>";
function SaveAddress(){
	var jqxhr = $.get(
		'/my-account/save-address',
		{
			addressId: $('#addressId').val(),
			name: $('#name').val(),
			addressLine1: $('#addressLine1').val(),
			addressLine2: $('#addressLine2').val(),
			city: $('#city').val(),
			state: $('#state').val(),
			zip: $('#zip').val(),
			country: $('#countryId').val()
		}
	);
	jqxhr.success(function(data) {
		if($('#addressId').val() == 0){
			addressHtml = format(addressItemTemplate, 
				{
					addressId		: data.address.id,
					addressName		: $('#name').val(),
					address1		: $('#addressLine1').val(),
					address2		: $('#addressLine2').val(),
					city			: $('#city').val(),
					state			: $('#state').val(),
					zip				: $('#zip').val(),
					country			: data.address.country.name
				});
			$(addressHtml).appendTo('.addresses-sel');
		}else{
			addressHtml = format(addressItemTemplate, 
					{
						addressId		: data.address.id,
						addressName		: $('#name').val(),
						address1		: $('#addressLine1').val(),
						address2		: $('#addressLine2').val(),
						city			: $('#city').val(),
						state			: $('#state').val(),
						zip				: $('#zip').val(),
						country			: data.address.country.name
					});
			$("#address-" + $('#addressId').val()).replaceWith(addressHtml)
		}
		$(".sel-address-form").dialog("close");
	});
}

function EditAddress(addressId){
	var jqxhr = $.get(
		'/my-account/get-address',
		{
			addressId: addressId
		}
	);
	jqxhr.success(function(data) {
		$(".sel-address-form").dialog({
			modal: true,
			open: function (){
				//alert(data.result.country.id);
				$('#addressId').val(data.result.id);
				$('#name').val(data.result.name);
				$('#addressLine1').val(data.result.address1);
				$('#addressLine2').val(data.result.address2);
				$('#city').val(data.result.city);
				$('#state').val(data.result.state);
				$('#zip').val(data.result.zip);
				$('#countryDdl').val(data.result.country.id);
				$('#countryDdl').parents('.jquery-selectbox').unselectbox();
				$('#countryDdl').selectbox();
			},
			close: function(){
				$('#addressId').val("0");
				$('#addressLine1').val("");
				$('#addressLine2').val("");
				$('#city').val("");
				$('#state').val("");
				$('#zip').val("");
				$('#countryDdl').val('232');
			}
		});
		//$('#countryDdl').val('232');
		
	});
}

function DeleteAddress(addressId){
	var jqxhr = $.get(
			'/my-account/delete-address',
			{
				addressId: addressId
			}
		);
	jqxhr.success(function(data) {
		$('#address-' + addressId).remove();
	});
}

$(document).ready(function(){
	$("a[rel^='prettyPhoto']").prettyPhoto();
});

























