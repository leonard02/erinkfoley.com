<?php include("includes/config.inc.php");

	$page_id = 3;

	define("TP","tbl_press",true);
	
	//All the press photos are fetched
	$sql_press = "SELECT * FROM `".TP."` ORDER BY `sequence`";
	$res_press = $db->get($sql_press);
	$num_press = $db->num_rows($res_press);

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<?php include("js.css.inc.php");?>
</head>
<body>
<div class="social_media"><?php include("socialmedia.inc.php");?></div>
<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td align="center" valign="top">
<table width="1126" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td height="33" align="center" valign="top">&nbsp;</td>
      </tr>
  <tr>
    <td align="center" valign="top"><?php include("header.inc.php");?></td>
  </tr>
  <tr>
    <td align="center" valign="top"><table width="975" border="0" align="center" cellpadding="0" cellspacing="0">
	<tr>
	<?php
	if($num_press>0){ 
		$i=1;
		while($row_press = $db->fetch_array($res_press)){
			$press_img = "uploads/press/".$row_press['press_image'];
			if($i==1 || $i==4){
				$width = "220";
				$i++;
			}else if($i==2 || $i==3){
				$i++;
				$width = "221";
			}else{
				echo '</tr><tr>
								<td colspan="7" align="center" valign="middle" height="30"></td>
					</tr><tr>';
				$i=2;
				$width = "220";
			}
			
				
				
	?>
        <td height="279" align="center" valign="middle" width="<?php echo $width;?>"><table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td align="center"><a href="press2.php?id=<?php echo $row_press['press_id'];?>"><img src="<?php echo $press_img;?>" width="220" height="277" alt="" border="0" /></a></td>
          </tr>
          <tr>
            <td height="8"></td>
          </tr>
          <tr>
            <td align="center" valign="middle" class="style7"><a href="press2.php?id=<?php echo $row_press['press_id'];?>" style="color:#000; text-decoration:none"><?php echo $f->getValue($row_press['headline']);?></a></td>
          </tr>
		  <tr>
            <td align="center" valign="middle" class="style7"><a href="press2.php?id=<?php echo $row_press['press_id'];?>" style="color:#000; text-decoration:none"><?php echo $f->getValue($row_press['press_date']);?></a></td>
          </tr>
        </table></td>
		<?php if($i<5){?>
        <td width="31" align="center" valign="middle">&nbsp;</td>
		<?php }?>
	 <?php 
	 	}
		if($num_press%4!=0){
			$rem = $num_press%4;
			for($k=1;$k<=$rem;$k++){
				if($i==4){
					$width = "220";
				}
				else{
					$width = "221";
					$i++;
				}
				?>
				<td width="<?php echo $width;?>" align="center" valign="middle">&nbsp;</td>
				<?php if($k!=$rem){?>
				<td width="31" align="center" valign="middle">&nbsp;</td>
				<?php
				}
			}
		}
	 }?>
     </tr>
    </table></td>
  </tr>
  <tr>
    <td align="center" valign="top">&nbsp;</td>
  </tr>
  <tr>
    <td align="center" valign="top">&nbsp;</td>
  </tr>
  <tr>
    <td align="center" valign="top">&nbsp;</td>
  </tr>
    </table></td>
  </tr>
</table>
    </td>
  </tr>
</table>
</body>
</html>
