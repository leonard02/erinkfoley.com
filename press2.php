<?php include("includes/config.inc.php");

	$page_id = 3;

	define("TP","tbl_press",true);
	define("TPAI","tbl_press_addl_images",true);
	
	$press_id = $_GET['id'];
	
	//All the press photos are fetched
	$sql_press = "SELECT * FROM `".TP."` WHERE `press_id`='".$press_id."'";
	$res_press = $db->get($sql_press);
	$row_press = $db->fetch_array($res_press);
	
	//Addl images are fetched
	$sql_press_addl = "SELECT * FROM `".TPAI."` WHERE `press_id`='".$press_id."' ORDER BY `sequence`";
	$res_press_addl = $db->get($sql_press_addl);
	/*$cnt=0;
	while($row_press_addl = $db->fetch_array($res_press_addl)){
		$main_img = "uploads/press/".$row_press_addl['image_path'];
		$image_arr[$cnt]['img_path']=$main_img;
		$cnt++;
	}*/

?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<?php include("js.css.inc.php");?>
<!--<link href="imgSlider/js-image-slider.css" rel="stylesheet" type="text/css" />
<script src="imgSlider/js-image-slider.js" type="text/javascript"></script>
-->
<link rel="stylesheet" href="css/style_press.css" />
</head>
<body>
<ul id="slideshow" style="display:none;">
		<?php 
		while($row_press_addl = $db->fetch_array($res_press_addl)){
			$large_image = "uploads/press/".$row_press_addl['image_path'];
			$thumb_image = "uploads/press/".$row_press_addl['image_path'];
			
		?>
		<li>
			<h3></h3>
			<span><?php echo $large_image;?></span>
			<p></p>
			<a href="#"><img src="<?php echo $thumb_image;?>" height="55" width="55" /></a>
		</li>
		<?php }?>
	</ul>

<div class="social_media"><?php include("socialmedia.inc.php");?></div>
<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td align="center" valign="top">
<table width="1126" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td height="33" align="center" valign="top">&nbsp;</td>
      </tr>
  <tr>
    <td align="center" valign="top"><?php include("header.inc.php");?></td>
  </tr>
  <?php if($db->num_rows($res_press_addl)>0){?>
  <tr>
    <td align="center" valign="top"><table width="100%" border="0" align="center" cellpadding="0" cellspacing="0" class="press_slider">
      <tr>
        <td align="center" valign="top"><table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
          <tr>
           <td width="900" height="512" align="center" valign="middle">
			<div id="wrapper">		
					<div id="fullsize" class="clips_area_press">			
						<div id="imgprev" class="imgnav" title="Previous Image"><img src="images/press2/left.png" alt="" /></div>
						<div id="imglink"></div>
						<div id="imgnext" class="imgnav" title="Next Image"><img src="images/press2/right.png" alt="" /></div>
						<div id="image"></div>
					</div>
					<div id="thumbnails" style="display:none;">
						<div id="slideleft" title="Slide Left"><img src="images/gallery/left_button.jpg" width="23" height="47" alt="" style="padding:0px;" /></div>
						<div id="slidearea">
							<div id="slider"></div>
						</div>
						<div id="slideright" title="Slide Right"><img src="images/gallery/right_button.jpg" width="23" height="47" alt="" style="padding:0px;" /></div>
					</div>
			</div>
		</td>
          </tr>
        </table>
		</td>
      </tr>
    </table></td>
  </tr>
  <?php }?>
  <tr>
    <td align="center" valign="top">&nbsp;</td>
  </tr>
  <tr>
    <td align="center" valign="top"><table width="910" border="0" align="center" cellpadding="0" cellspacing="0">
      <tr>
        <td align="left" valign="top"><?php echo $f->getValue($row_press['press_details']);?></td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td align="center" valign="top">&nbsp;</td>
  </tr>
  <tr>
    <td align="center" valign="top">&nbsp;</td>
  </tr>
  <tr>
    <td align="center" valign="top">&nbsp;</td>
  </tr>
  <tr>
    <td align="center" valign="top">&nbsp;</td>
  </tr>
  <tr>
    <td align="center" valign="top">&nbsp;</td>
  </tr>
    </table></td>
  </tr>
</table>
    </td>
  </tr>
</table>
<script type="text/javascript" src="js/compressed.js"></script>
<script type="text/javascript">
	$('slideshow').style.display='none';
	$('wrapper').style.display='block';
	var slideshow=new TINY.slideshow("slideshow");
	window.onload=function(){
		slideshow.auto=true;
		slideshow.speed=5;
		//slideshow.link="linkhover";
		//slideshow.info="information";
		slideshow.thumbs="slider";
		slideshow.left="slideleft";
		slideshow.right="slideright";
		slideshow.scrollSpeed=5;
		slideshow.spacing=5;
		//slideshow.active="#000";
		slideshow.init("slideshow","image","imgprev","imgnext","imglink");
	}
</script>

</body>
</html>
