<?php include("includes/config.inc.php");

$page_id = 1;

define("TS","tbl_shows",true);
define("TSD","tbl_show_dates",true);
define("TSF","tbl_show_flyers",true);

//Present Shows info are fetched
$sql_shows = "SELECT * FROM `".TS."` ORDER BY `show_type`, `sequence`";

$res_shows = $db->get($sql_shows);
$num_shows = $db->num_rows($res_shows);

if($num_shows>0){
	//Array is populated
	$i=1;
	while($row_shows = $db->fetch_array($res_shows)){
		$show_array[$i]['show_id'] = $f->getValue($row_shows['show_id']);
		$show_array[$i]['headline'] = $f->getValue($row_shows['headline']);
		$show_array[$i]['image_path'] = $f->getValue($row_shows['image_path']);
		$show_array[$i]['location'] = $f->getValue($row_shows['location']);
		$show_array[$i]['city_state'] = $f->getValue($row_shows['city_state']);
		$show_array[$i]['link'] = $f->getWebsiteFullURL($f->getValue($row_shows['link']));
		$show_array[$i]['summarized_dates'] = $f->getValue($row_shows['summarized_dates']);
		$show_array[$i]['list_dates'] = $f->getValue($row_shows['list_dates']);
		$show_array[$i]['from_date'] = $f->getValue($row_shows['from_date']);
		$show_array[$i]['to_date'] = $f->getValue($row_shows['to_date']);
		$show_array[$i]['summarize_time'] = $f->getValue($row_shows['summarize_time']);
		$show_array[$i]['show_type'] = $f->getValue($row_shows['show_type']);
		$i++;
	}
}

//Show flyers are fetched
$sql_flyers = "SELECT * FROM `".TSF."` ORDER BY `flyer_id` ASC";
$res_flyers = $db->get($sql_flyers);


?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<?php include("js.css.inc.php");?>
<script src="js/dw_scroll_c.js" type="text/javascript"></script>
<script type="text/javascript">

function init_dw_Scroll() {
    // arguments: id of scroll area div, id of content div
    var wndo = new dw_scrollObj('wn', 'lyr1');
    // args: id, axis
	//wndo.buildScrollControls('scrollLinks_new', 'v');
    wndo.buildScrollControls('scrollLinks', 'v');
    
    // for mousedown scrolling, add as 3rd argument
    //wndo.buildScrollControls('scrollLinks', 'v', 'mousedown');
    // w/o title
    // args: id, axis, eType, bScrollbar, include title?
    //wndo.buildScrollControls('scrollLinks', 'v', 'mousedown', false, false);
}

// if code supported, link in the style sheet (optional) and call the init function onload
if ( dw_scrollObj.isSupported() ) {
    dw_Util.writeStyleSheet('css/scroll_v.css');
    dw_Event.add( window, 'load', init_dw_Scroll);
}

</script>
</head>
<body>
<div class="social_media"><?php include("socialmedia.inc.php");?></div>
<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td align="center" valign="top">
<table width="1126" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td height="33" align="center" valign="top">&nbsp;</td>
      </tr>
  <tr>
    <td align="center" valign="top"><?php include("header.inc.php");?></td>
  </tr>
  <tr>
    <td align="center" valign="top">
		<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
      <tr>
        <td width="38%" align="left" valign="top"><table width="99%" border="0" align="left" cellpadding="0" cellspacing="0">
		  <?php if($db->num_rows($res_flyers)>0){
		  	while($row_flyers = $db->fetch_array($res_flyers)){
				$image_path = "uploads/flyers/".$row_flyers['flyer_image'];
		  ?>
          <tr>
            <td align="left" valign="top"><img src="<?php echo $image_path;?>" width="415" height="467" alt="" class="style1" /></td>
            </tr>
          <tr>
            <td align="center" valign="middle" class="style2"><?php echo $f->getValue($row_flyers['caption1']);?></td>
            </tr>
          <tr>
            <td align="center" valign="top" class="style2"><?php echo $f->getValue($row_flyers['caption2']);?></td>
            </tr>
          <tr>
            <td height="23" align="left" valign="top">&nbsp;</td>
          </tr>
		  <?php
		  		}
		   }?>
          </table></td>
        <td width="62%" align="right" valign="top">
		<table width="671" border="0" align="right" cellpadding="0" cellspacing="0">
		<tr>
			<td>
		<div id="wn"> <!-- scroll area div -->
    		<div id="lyr1" style="width:671px;"> <!-- layer in scroll area (content div) -->

			<table width="100%" border="0" cellpadding="0" cellspacing="0">

		  <?php if($num_shows>0){
		  	$cnt = 0;
			$show_type_arr = array();
		  	for($i=1;$i<=count($show_array);$i++){
				if($cnt == 0){
					$bgcolor = "#f1f2f1";
					$cnt=1;
				}else{
					$bgcolor = "#e6e6e6";
					$cnt=0;
				}
				if($bgcolor == "#f1f2f1")
					$bgcolor1 = "#e6e6e6";
				else if($bgcolor == "#e6e6e6")
					$bgcolor1 = "#f1f2f1";
				
				//Image path
				$show_image="uploads/shows/".$show_array[$i]['image_path'];
				
				if($show_array[$i]['link']!=""){
					if($show_array[$i]['location']!="Name")
						$location = "<a href='".$show_array[$i]['link']."' target='_blank' style='color:#333333;text-decoration:none;'>".$f->getValue($show_array[$i]['location'])."</a>";
					else
						$location = "";
					if($show_array[$i]['city_state']!="City, State")
						$city_state = "<a href='".$show_array[$i]['link']."' target='_blank' style='color:#333333;text-decoration:none;'>".$f->getValue($show_array[$i]['city_state'])."</a>";
					else
						$city_state = "";
				}else{
					if($show_array[$i]['location']!="Name")
						$location = $f->getValue($show_array[$i]['location']);
					else
						$location = "";
					if($show_array[$i]['city_state']!="City, State")
						$city_state = $f->getValue($show_array[$i]['city_state']);
					else
						$city_state = "";
				}
					
				$headline = $f->getValue($show_array[$i]['headline']);
				
				if(!in_array($show_array[$i]['show_type'],$show_type_arr)){
					$show_type_arr[$i]=$show_array[$i]['show_type'];
				
				if($show_type_arr[$i]=="1")
					$show_type = "LIVE";
				else
					$show_type = "PAST SHOWS";
				
		  ?>
		  <tr>
            <td height="36" align="center" valign="middle" bgcolor="<?php echo $bgcolor1;?>" class="style4"><?php echo $show_type;?></td>
          </tr>
		  <?php }?>
          <tr>
            <td align="center" valign="top"><table width="100%" border="0" align="center" cellpadding="0" cellspacing="0" bgcolor="<?php echo $bgcolor;?>" class="shows">
              <tr>
                <td width="18%" rowspan="9" align="left" valign="top"><img src="<?php echo $show_image;?>" height="97" width="84" alt="" /></td>
                <td colspan="2" align="left" valign="top">&nbsp;</td>
                <td width="47%" colspan="2" align="left" valign="top">&nbsp;</td>
              </tr>
              <tr>
                <td colspan="2" align="left" valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0">
				 <?php if($show_array[$i]['summarized_dates']=="Y"){?>
                  <tr>
                    <td width="40%" align="left"><?php echo date("D, n.j",strtotime($show_array[$i]['from_date']));?><br />
                      -<br /><?php echo date("D, n.j",strtotime($show_array[$i]['to_date']));?></td>
                    <td width="60%" align="left" valign="top"><?php echo strtoupper($show_array[$i]['summarize_time']);?></td>
                  </tr>
				  <?php }else if($show_array[$i]['list_dates']=="Y"){
				  	//Show dates are fetched
					$sql_show_dates = "SELECT * FROM `".TSD."` WHERE `show_id`='".$show_array[$i]['show_id']."' ORDER BY `show_date`";
					$res_show_dates = $db->get($sql_show_dates);
					while($row_show_dates = $db->fetch_array($res_show_dates)){
				  ?>
                  <tr>
                    <td width="40%" align="left"><?php echo date("D, n.j",strtotime($row_show_dates['show_date']));?></td>
                    <td width="60%" align="left"><?php echo strtoupper($row_show_dates['from_time']);?> <?php if($row_show_dates['to_time']!="") echo ",".strtoupper($row_show_dates['to_time']);?></td>
                  </tr>
				  <?php }
				  }
				  ?>
                  <tr>
                    <td width="40%">&nbsp;</td>
                    <td width="60%">&nbsp;</td>
                  </tr>
                </table></td>
                <td colspan="2" align="left" valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                  <tr>
                    <td width="52%" align="left"><?php echo $headline;?></td>
                    <td width="48%" align="left"><?php echo $location;?></td>
                  </tr>
                  <tr>
                    <td>&nbsp;</td>
                    <td align="left"><?php echo $city_state;?></td>
                  </tr>
                </table></td>
                </tr>
            </table></td>
          </tr>
		  <?php 
			}
		  }
		  ?>
			</table>
			</div> <!-- end content div (lyr1) -->
		</div>  <!-- end wn div -->
			</td>
		  </tr>
        </table>
		<?php 
		  //if($tot_count<$num_shows){
		  ?>
		  	<div id="navigation" align="center">
			  <table border="0" cellpadding="0" cellspacing="0" width="100%">
			 <!--<tr>
				<td align="center" valign="top" class="style5" id="scrollLinks_new">SEE MORE</td>
			  </tr>-->
			  <tr>
				<td align="center" valign="top"><div id="scrollLinks"></div></td>
			  </tr>
			  </table>
			 </div>
	  <?php //}?>

		</td>
      </tr>
	  

      <tr>
        <td align="left" valign="middle">&nbsp;</td>
        <td align="left" valign="middle">&nbsp;</td>
      </tr>
      <tr>
        <td align="left" valign="middle">&nbsp;</td>
        <td align="left" valign="middle">&nbsp;</td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td align="center" valign="top"><img src="images/shows/devide-line.jpg" width="1046" height="2" alt="" /></td>
  </tr>
  <tr>
    <td align="center" valign="top">&nbsp;</td>
  </tr>
  <tr>
    <td align="center" valign="top">&nbsp;</td>
  </tr>
  <tr>
    <td align="center" valign="top"><?php include("footer.inc.php");?></td>
  </tr>
  <tr>
    <td align="center" valign="top">&nbsp;</td>
  </tr>
  <tr>
    <td align="center" valign="top">&nbsp;</td>
  </tr>
  <tr>
    <td align="center" valign="top">&nbsp;</td>
  </tr>
    </table></td>
  </tr>
</table>
    </td>
  </tr>
</table>
<input type="hidden" name="hidoffset" id="hidoffset" value="<?php echo $tot_count;?>" />
<input type="hidden" name="cnt" id="cnt" value="0" />
</body>
</html>
